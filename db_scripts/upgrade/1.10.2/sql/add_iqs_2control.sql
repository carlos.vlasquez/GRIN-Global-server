/*
   Wednesday, January 31, 20189:56:45 AM
   User: 
   Server: localhost\MSSQLSERVER01
   Database: gringlobal
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.inventory_quality_status
	DROP CONSTRAINT fk_iqs_me
GO
ALTER TABLE dbo.method SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.method', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.method', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.method', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.inventory_quality_status
	DROP CONSTRAINT fk_iqs_i
GO
ALTER TABLE dbo.inventory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.inventory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.inventory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.inventory', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.inventory_quality_status
	DROP CONSTRAINT fk_iqs_created
GO
ALTER TABLE dbo.inventory_quality_status
	DROP CONSTRAINT fk_iqs_cur
GO
ALTER TABLE dbo.inventory_quality_status
	DROP CONSTRAINT fk_iqs_modified
GO
ALTER TABLE dbo.inventory_quality_status
	DROP CONSTRAINT fk_iqs_owned
GO
ALTER TABLE dbo.cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_inventory_quality_status
	(
	inventory_quality_status_id int NOT NULL IDENTITY (1, 1),
	inventory_id int NOT NULL,
	test_type_code nvarchar(20) NOT NULL,
	contaminant_code nvarchar(20) NOT NULL,
	test_result_code nvarchar(20) NULL,
	plant_part_tested_code nvarchar(20) NULL,
	test_results_score nvarchar(40) NULL,
	test_results_score_type_code nvarchar(20) NULL,
	negative_control decimal(10, 5) NULL,
	positive_control decimal(10, 5) NULL,
	replicate int NULL,
	started_date datetime2(7) NULL,
	started_date_code nvarchar(20) NULL,
	completed_date datetime2(7) NULL,
	completed_date_code nvarchar(20) NULL,
	required_replication_count int NULL,
	started_count int NULL,
	completed_count int NULL,
	plate_or_assay_number nvarchar(40) NULL,
	method_id int NULL,
	tester_cooperator_id int NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_inventory_quality_status SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_inventory_quality_status TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_inventory_quality_status TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_inventory_quality_status TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_inventory_quality_status TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_inventory_quality_status TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_inventory_quality_status ON
GO
IF EXISTS(SELECT * FROM dbo.inventory_quality_status)
	 EXEC('INSERT INTO dbo.Tmp_inventory_quality_status (inventory_quality_status_id, inventory_id, test_type_code, contaminant_code, test_result_code, plant_part_tested_code, test_results_score, test_results_score_type_code, replicate, started_date, started_date_code, completed_date, completed_date_code, required_replication_count, started_count, completed_count, plate_or_assay_number, method_id, tester_cooperator_id, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT inventory_quality_status_id, inventory_id, test_type_code, contaminant_code, test_result_code, plant_part_tested_code, test_results_score, test_results_score_type_code, replicate, started_date, started_date_code, completed_date, completed_date_code, required_replication_count, started_count, completed_count, plate_or_assay_number, method_id, tester_cooperator_id, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.inventory_quality_status WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_inventory_quality_status OFF
GO
DROP TABLE dbo.inventory_quality_status
GO
EXECUTE sp_rename N'dbo.Tmp_inventory_quality_status', N'inventory_quality_status', 'OBJECT' 
GO
ALTER TABLE dbo.inventory_quality_status ADD CONSTRAINT
	PK_inventory_quality_status PRIMARY KEY CLUSTERED 
	(
	inventory_quality_status_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_iqs_created ON dbo.inventory_quality_status
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_iqs_cur ON dbo.inventory_quality_status
	(
	tester_cooperator_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_iqs_i ON dbo.inventory_quality_status
	(
	inventory_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_iqs_me ON dbo.inventory_quality_status
	(
	method_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_iqs_modified ON dbo.inventory_quality_status
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_iqs_owned ON dbo.inventory_quality_status
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_iqs_test ON dbo.inventory_quality_status
	(
	test_type_code,
	contaminant_code
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_iqs ON dbo.inventory_quality_status
	(
	inventory_id,
	test_type_code,
	contaminant_code,
	started_date,
	plant_part_tested_code,
	test_results_score_type_code,
	replicate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.inventory_quality_status ADD CONSTRAINT
	fk_iqs_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_quality_status ADD CONSTRAINT
	fk_iqs_cur FOREIGN KEY
	(
	tester_cooperator_id
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_quality_status ADD CONSTRAINT
	fk_iqs_i FOREIGN KEY
	(
	inventory_id
	) REFERENCES dbo.inventory
	(
	inventory_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_quality_status ADD CONSTRAINT
	fk_iqs_me FOREIGN KEY
	(
	method_id
	) REFERENCES dbo.method
	(
	method_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_quality_status ADD CONSTRAINT
	fk_iqs_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_quality_status ADD CONSTRAINT
	fk_iqs_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.inventory_quality_status', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.inventory_quality_status', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.inventory_quality_status', 'Object', 'CONTROL') as Contr_Per 