-- Update table field mappings for a new field
USE [gringlobal]

DECLARE @table nvarchar(50);
DECLARE @field nvarchar(50);
DECLARE @ordinal int;
DECLARE @tableID int;
DECLARE @fieldID int;

SET @table = 'inventory_quality_status';
SET @field = 'negative_control';
SET @ordinal = 8;

SET @tableID = (SELECT sys_table_id FROM sys_table WHERE table_name = @table);

-- Make roome for new field entry 
UPDATE sys_table_field
SET field_ordinal = field_ordinal + 1
WHERE sys_table_id = @tableID AND field_ordinal >= @ordinal

-- Add the new field
INSERT INTO [dbo].[sys_table_field]
           ([sys_table_id],[field_name],[field_ordinal]
           ,[field_purpose],[field_type],[default_value],[is_primary_key],[is_foreign_key]
           ,[foreign_key_table_field_id],[foreign_key_dataview_name]
           ,[is_nullable],[gui_hint],[is_readonly]
           ,[min_length],[max_length],[numeric_precision],[numeric_scale],[is_autoincrement],[group_name]
           ,[created_date],[created_by],[modified_date],[modified_by],[owned_date],[owned_by])
     VALUES
           (@tableID, @field, @ordinal
           ,'DATA','DECIMAL','{DBNull.Value}','N','N'
           ,NULL,NULL
           ,'Y','DECIMAL_CONTROL','N'
           ,0,0,0,0,'N',NULL
           ,GETUTCDATE(),48,NULL,NULL,GETUTCDATE(),48)


----------------------------------------------------------------------------------------------
SET @table = 'inventory_quality_status';
SET @field = 'positive_control';
SET @ordinal = 9;

SET @tableID = (SELECT sys_table_id FROM sys_table WHERE table_name = @table);

-- Make roome for new field entry 
UPDATE sys_table_field
SET field_ordinal = field_ordinal + 1
WHERE sys_table_id = @tableID AND field_ordinal >= @ordinal

-- Add the new field
INSERT INTO [dbo].[sys_table_field]
           ([sys_table_id],[field_name],[field_ordinal]
           ,[field_purpose],[field_type],[default_value],[is_primary_key],[is_foreign_key]
           ,[foreign_key_table_field_id],[foreign_key_dataview_name]
           ,[is_nullable],[gui_hint],[is_readonly]
           ,[min_length],[max_length],[numeric_precision],[numeric_scale],[is_autoincrement],[group_name]
           ,[created_date],[created_by],[modified_date],[modified_by],[owned_date],[owned_by])
     VALUES
           (@tableID, @field, @ordinal
           ,'DATA','DECIMAL','{DBNull.Value}','N','N'
           ,NULL,NULL
           ,'Y','DECIMAL_CONTROL','N'
           ,0,0,0,0,'N',NULL
           ,GETUTCDATE(),48,NULL,NULL,GETUTCDATE(),48)

