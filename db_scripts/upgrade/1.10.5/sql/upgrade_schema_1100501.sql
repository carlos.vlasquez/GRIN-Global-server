/*
   Schema change to add alternate_transcription column to taxonomy_common_name table
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_common_name
	DROP CONSTRAINT fk_tcn_cit
GO
ALTER TABLE dbo.citation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.citation', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_common_name
	DROP CONSTRAINT fk_tcn_tg
GO
ALTER TABLE dbo.taxonomy_genus SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_genus', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_genus', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_genus', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_common_name
	DROP CONSTRAINT fk_tcn_created
GO
ALTER TABLE dbo.taxonomy_common_name
	DROP CONSTRAINT fk_tcn_modified
GO
ALTER TABLE dbo.taxonomy_common_name
	DROP CONSTRAINT fk_tcn_owned
GO
ALTER TABLE dbo.cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_common_name
	DROP CONSTRAINT fk_tcn_ts
GO
ALTER TABLE dbo.taxonomy_species SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_taxonomy_common_name
	(
	taxonomy_common_name_id int NOT NULL IDENTITY (1, 1),
	taxonomy_genus_id int NULL,
	taxonomy_species_id int NULL,
	language_description nvarchar(100) NULL,
	name nvarchar(100) NOT NULL,
	simplified_name nvarchar(100) NULL,
	alternate_transcription nvarchar(100) NULL,
	citation_id int NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_taxonomy_common_name SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_taxonomy_common_name TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_taxonomy_common_name TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_taxonomy_common_name TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_taxonomy_common_name TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_taxonomy_common_name TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_taxonomy_common_name ON
GO
IF EXISTS(SELECT * FROM dbo.taxonomy_common_name)
	 EXEC('INSERT INTO dbo.Tmp_taxonomy_common_name (taxonomy_common_name_id, taxonomy_genus_id, taxonomy_species_id, language_description, name, simplified_name, citation_id, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT taxonomy_common_name_id, taxonomy_genus_id, taxonomy_species_id, language_description, name, simplified_name, citation_id, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.taxonomy_common_name WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_taxonomy_common_name OFF
GO
DROP TABLE dbo.taxonomy_common_name
GO
EXECUTE sp_rename N'dbo.Tmp_taxonomy_common_name', N'taxonomy_common_name', 'OBJECT' 
GO
ALTER TABLE dbo.taxonomy_common_name ADD CONSTRAINT
	PK_taxonomy_common_name PRIMARY KEY CLUSTERED 
	(
	taxonomy_common_name_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_cn_name ON dbo.taxonomy_common_name
	(
	name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_cn_simplified_name ON dbo.taxonomy_common_name
	(
	simplified_name
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_tcn_created ON dbo.taxonomy_common_name
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_tcn_modified ON dbo.taxonomy_common_name
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_tcn_owned ON dbo.taxonomy_common_name
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_tcn_tg ON dbo.taxonomy_common_name
	(
	taxonomy_genus_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_tcn_ts ON dbo.taxonomy_common_name
	(
	taxonomy_species_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_tcn ON dbo.taxonomy_common_name
	(
	taxonomy_genus_id,
	taxonomy_species_id,
	language_description,
	name,
	citation_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.taxonomy_common_name ADD CONSTRAINT
	fk_tcn_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_common_name ADD CONSTRAINT
	fk_tcn_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_common_name ADD CONSTRAINT
	fk_tcn_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_common_name ADD CONSTRAINT
	fk_tcn_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_common_name ADD CONSTRAINT
	fk_tcn_tg FOREIGN KEY
	(
	taxonomy_genus_id
	) REFERENCES dbo.taxonomy_genus
	(
	taxonomy_genus_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_common_name ADD CONSTRAINT
	fk_tcn_cit FOREIGN KEY
	(
	citation_id
	) REFERENCES dbo.citation
	(
	citation_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_common_name', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_common_name', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_common_name', 'Object', 'CONTROL') as Contr_Per EXECUTE sp_fulltext_database N'enable'
GO
IF EXISTS(SELECT 1 FROM sys.fulltext_catalogs WHERE name = 'gringlobalftsc') BEGIN
	CREATE FULLTEXT INDEX ON dbo.taxonomy_common_name (name LANGUAGE 1033, simplified_name LANGUAGE 1033) KEY INDEX PK_taxonomy_common_name ON gringlobalftsc WITH CHANGE_TRACKING AUTO;
	ALTER FULLTEXT INDEX ON dbo.taxonomy_common_name ENABLE;
END
GO
EXECUTE sp_fulltext_database N'disable'
GO
