-- Adjusts some sys_table_field values on new taxonomy_cwr table after it is table mapped

UPDATE sys_table_field SET foreign_key_dataview_name = 'taxonomy_species_lookup'
WHERE field_name = 'taxonomy_species_id' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')
UPDATE sys_table_field SET foreign_key_dataview_name = 'taxonomy_species_citation_lookup'
WHERE field_name = 'citation_id' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')
UPDATE sys_table_field SET foreign_key_dataview_name = 'cooperator_lookup'
WHERE field_name = 'created_by' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')
UPDATE sys_table_field SET foreign_key_dataview_name = 'cooperator_lookup'
WHERE field_name = 'modified_by' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')
UPDATE sys_table_field SET foreign_key_dataview_name = 'cooperator_lookup'
WHERE field_name = 'owned_by' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')

UPDATE sys_table_field SET group_name = 'CWR_GENEPOOL'
WHERE field_name = 'genepool_code' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')
UPDATE sys_table_field SET group_name = 'CWR_TRAIT_CLASS'
WHERE field_name = 'trait_class_code' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')
UPDATE sys_table_field SET group_name = 'CWR_BREEDING_TYPE'
WHERE field_name = 'breeding_type_code' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')

UPDATE sys_table_field SET default_value = 'N'
WHERE field_name = 'is_crop' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')
UPDATE sys_table_field SET default_value = 'N'
WHERE field_name = 'is_potential' AND sys_table_id = (SELECT sys_table_id from sys_table where table_name = 'taxonomy_cwr')
