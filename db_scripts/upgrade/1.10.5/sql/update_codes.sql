DROP PROCEDURE #update_cvl;
GO
CREATE PROCEDURE #update_cvl  
(  
	@group NVARCHAR(100)  
	, @value NVARCHAR(20)
	, @lang NVARCHAR(50)
	, @title NVARCHAR(500)
	, @description NVARCHAR(500)
)  
AS  
BEGIN  
	DECLARE @cvid int;

	IF NOT EXISTS (SELECT 1 FROM code_value WHERE group_name = @group AND value = @value)
		INSERT INTO code_value
			VALUES (@group, @value, GETUTCDATE(), 48, NULL, NULL, GETUTCDATE(), 48);
	   
	SELECT @cvid = code_value_id FROM code_value WHERE group_name = @group AND value = @value

	DECLARE @slid int;
	SELECT @slid = sys_lang_id FROM sys_lang WHERE title = @lang;

	IF EXISTS (SELECT 1 FROM code_value_lang WHERE code_value_id = @cvid AND sys_lang_id = @slid)
		UPDATE code_value_lang
		SET title = @title, description = @description
			,modified_by = 48, modified_date = GETUTCDATE()
		WHERE code_value_id = @cvid AND sys_lang_id = @slid;
	ELSE
		INSERT INTO code_value_lang
		VALUES (@cvid, @slid, @title, @description, GETUTCDATE(), 48, NULL, NULL, GETUTCDATE(), 48);
END
GO


/*
SELECT 
	'EXEC #update_cvl ''' + cv.group_name
	+ ''', ''' + cv.value
	+ ''', ''' + sl.title
	+ ''', ''' + cvl.title
	+ ''', ''' + COALESCE(cvl.description, '')
	+ ''''
FROM code_value cv
INNER JOIN code_value_lang cvl ON cvl.code_value_id = cv.code_value_id
INNER JOIN sys_lang sl ON sl.sys_lang_id = cvl.sys_lang_id
WHERE COALESCE(cvl.modified_date, cvl.created_date) > '2019'
ORDER BY cv.group_name, cvl.description, cvl.sys_lang_id
*/

EXEC #update_cvl 'CWR_BREEDING_TYPE', 'CT', 'English', 'Cold Tolerance', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'CQ', 'English', 'Crop Quality', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'CMS', 'English', 'Cytoplasmic Male Sterility', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'DR', 'English', 'Disease Resistance', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'DT', 'English', 'Drought Resistance', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'D', 'English', 'Dwarfing', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'EM', 'English', 'Early Maturity', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'GH', 'English', 'Growth Habit', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'HT', 'English', 'Heat Tolerance', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'HY', 'English', 'High Yield', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'LM', 'English', 'Late Maturity', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'MT', 'English', 'Metal Tolerance', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'PR', 'English', 'Pest Resistance', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'ST', 'English', 'Salt Tolerance', ''
EXEC #update_cvl 'CWR_BREEDING_TYPE', 'WT', 'English', 'Waterlogging Tolerance', ''
EXEC #update_cvl 'CWR_GENEPOOL', 'Primary', 'English', 'Primary', ''
EXEC #update_cvl 'CWR_GENEPOOL', 'Secondary', 'English', 'Secondary', ''
EXEC #update_cvl 'CWR_GENEPOOL', 'Tertiary', 'English', 'Tertiary', ''
EXEC #update_cvl 'CWR_GENEPOOL', 'Quaternary', 'English', 'Quaternary', ''
EXEC #update_cvl 'CWR_GENEPOOL', 'Progenitor', 'English', 'Progenitor', ''
EXEC #update_cvl 'CWR_GENEPOOL', 'ProbProg', 'English', 'Probable Progenitor', ''
EXEC #update_cvl 'CWR_TRAIT_CLASS', 'ABIO', 'English', 'Abiotic', ''
EXEC #update_cvl 'CWR_TRAIT_CLASS', 'AGRO', 'English', 'Agronomic', ''
EXEC #update_cvl 'CWR_TRAIT_CLASS', 'BIOT', 'English', 'Biotic', ''
EXEC #update_cvl 'CWR_TRAIT_CLASS', 'FERT', 'English', 'Fertility', ''
EXEC #update_cvl 'CWR_TRAIT_CLASS', 'PHEN', 'English', 'Phenological', ''