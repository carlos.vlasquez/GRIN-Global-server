/*
   Thursday, April 12, 20181:00:57 PM
   User: 
   Server: localhost\MSSQLSERVER01
   Database: gringlobal
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_tg
GO
ALTER TABLE dbo.taxonomy_genus SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_genus', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_genus', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_genus', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_tf
GO
ALTER TABLE dbo.taxonomy_family SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_family', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_family', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_family', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_m
GO
ALTER TABLE dbo.method SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.method', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.method', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.method', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_gm
GO
ALTER TABLE dbo.genetic_marker SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.genetic_marker', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.genetic_marker', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.genetic_marker', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_created
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_modified
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_owned
GO
ALTER TABLE dbo.cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_ap
GO
ALTER TABLE dbo.accession_pedigree SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_pedigree', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_pedigree', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_pedigree', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_aipr
GO
ALTER TABLE dbo.accession_ipr SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_ipr', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_ipr', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_ipr', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_a
GO
ALTER TABLE dbo.accession SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_ts
GO
ALTER TABLE dbo.taxonomy_species SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation
	DROP CONSTRAINT fk_ci_l
GO
ALTER TABLE dbo.literature SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.literature', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.literature', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.literature', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_citation
	(
	citation_id int NOT NULL IDENTITY (1, 1),
	literature_id int NULL,
	citation_title nvarchar(400) NULL,
	author_name nvarchar(2000) NULL,
	citation_year int NULL,
	reference nvarchar(200) NULL,
	doi_reference nvarchar(500) NULL,
	url nvarchar(500) NULL,
	title nvarchar(500) NULL,
	description nvarchar(500) NULL,
	accession_id int NULL,
	method_id int NULL,
	taxonomy_species_id int NULL,
	taxonomy_genus_id int NULL,
	taxonomy_family_id int NULL,
	accession_ipr_id int NULL,
	accession_pedigree_id int NULL,
	genetic_marker_id int NULL,
	type_code nvarchar(20) NULL,
	unique_key int NULL,
	is_accepted_name nvarchar(1) NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_citation SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_citation TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_citation TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_citation TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_citation TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_citation TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_citation ON
GO
IF EXISTS(SELECT * FROM dbo.citation)
	 EXEC('INSERT INTO dbo.Tmp_citation (citation_id, literature_id, citation_title, author_name, citation_year, reference, doi_reference, url, title, description, accession_id, method_id, taxonomy_species_id, taxonomy_genus_id, taxonomy_family_id, accession_ipr_id, accession_pedigree_id, genetic_marker_id, type_code, unique_key, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT citation_id, literature_id, citation_title, author_name, citation_year, reference, doi_reference, url, title, description, accession_id, method_id, taxonomy_species_id, taxonomy_genus_id, taxonomy_family_id, accession_ipr_id, accession_pedigree_id, genetic_marker_id, type_code, unique_key, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.citation WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_citation OFF
GO
ALTER TABLE dbo.taxonomy_common_name
	DROP CONSTRAINT fk_tcn_cit
GO
ALTER TABLE dbo.taxonomy_geography_map
	DROP CONSTRAINT fk_tgm_cit
GO
ALTER TABLE dbo.taxonomy_use
	DROP CONSTRAINT fk_tu_cit
GO
DROP TABLE dbo.citation
GO
EXECUTE sp_rename N'dbo.Tmp_citation', N'citation', 'OBJECT' 
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	PK_citation PRIMARY KEY CLUSTERED 
	(
	citation_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_a ON dbo.citation
	(
	accession_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_aipr ON dbo.citation
	(
	accession_ipr_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_ap ON dbo.citation
	(
	accession_pedigree_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_created ON dbo.citation
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_gm ON dbo.citation
	(
	genetic_marker_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_l ON dbo.citation
	(
	literature_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_m ON dbo.citation
	(
	method_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_modified ON dbo.citation
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_owned ON dbo.citation
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_tf ON dbo.citation
	(
	taxonomy_family_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_tg ON dbo.citation
	(
	taxonomy_genus_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ci_ts ON dbo.citation
	(
	taxonomy_species_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_cit ON dbo.citation
	(
	literature_id,
	citation_title,
	accession_id,
	method_id,
	taxonomy_species_id,
	taxonomy_genus_id,
	taxonomy_family_id,
	accession_ipr_id,
	accession_pedigree_id,
	genetic_marker_id,
	type_code,
	unique_key
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_l FOREIGN KEY
	(
	literature_id
	) REFERENCES dbo.literature
	(
	literature_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_aipr FOREIGN KEY
	(
	accession_ipr_id
	) REFERENCES dbo.accession_ipr
	(
	accession_ipr_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_ap FOREIGN KEY
	(
	accession_pedigree_id
	) REFERENCES dbo.accession_pedigree
	(
	accession_pedigree_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_gm FOREIGN KEY
	(
	genetic_marker_id
	) REFERENCES dbo.genetic_marker
	(
	genetic_marker_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_m FOREIGN KEY
	(
	method_id
	) REFERENCES dbo.method
	(
	method_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_tf FOREIGN KEY
	(
	taxonomy_family_id
	) REFERENCES dbo.taxonomy_family
	(
	taxonomy_family_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_tg FOREIGN KEY
	(
	taxonomy_genus_id
	) REFERENCES dbo.taxonomy_genus
	(
	taxonomy_genus_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.citation', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_use ADD CONSTRAINT
	fk_tu_cit FOREIGN KEY
	(
	citation_id
	) REFERENCES dbo.citation
	(
	citation_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_use SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_use', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_use', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_use', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_geography_map ADD CONSTRAINT
	fk_tgm_cit FOREIGN KEY
	(
	citation_id
	) REFERENCES dbo.citation
	(
	citation_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_geography_map SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_geography_map', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_geography_map', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_geography_map', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_common_name ADD CONSTRAINT
	fk_tcn_cit FOREIGN KEY
	(
	citation_id
	) REFERENCES dbo.citation
	(
	citation_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_common_name SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_common_name', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_common_name', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_common_name', 'Object', 'CONTROL') as Contr_Per 

update citation set is_accepted_name = 'N'

ALTER TABLE citation ALTER COLUMN is_accepted_name  nvarchar(1) NOT NULL