/****** Object:  Table [dbo].[method_attach]    Script Date: 7/19/2018 3:53:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[method_attach](
	[method_attach_id] [int] IDENTITY(1,1) NOT NULL,
	[method_id] [int] NOT NULL,
	[virtual_path] [nvarchar](255) NOT NULL,
	[thumbnail_virtual_path] [nvarchar](255) NULL,
	[sort_order] [int] NULL,
	[title] [nvarchar](500) NULL,
	[description] [nvarchar](500) NULL,
	[content_type] [nvarchar](100) NULL,
	[category_code] [nvarchar](20) NULL,
	[is_web_visible] [nvarchar](1) NOT NULL,
	[copyright_information] [nvarchar](100) NULL,
	[attach_cooperator_id] [int] NULL,
	[attach_date] [datetime2](7) NULL,
	[attach_date_code] [nvarchar](20) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_method_attach] PRIMARY KEY CLUSTERED 
(
	[method_attach_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_mat_c]    Script Date: 7/19/2018 3:53:25 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_c] ON [dbo].[method_attach]
(
	[attach_cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_mat_created]    Script Date: 7/19/2018 3:53:25 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_created] ON [dbo].[method_attach]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_mat_m]    Script Date: 7/19/2018 3:53:25 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_m] ON [dbo].[method_attach]
(
	[method_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_mat_modified]    Script Date: 7/19/2018 3:53:25 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_modified] ON [dbo].[method_attach]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_mat_owned]    Script Date: 7/19/2018 3:53:25 PM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_mat_owned] ON [dbo].[method_attach]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [ndx_uniq_mat]    Script Date: 7/19/2018 3:53:25 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_mat] ON [dbo].[method_attach]
(
	[method_id] ASC,
	[virtual_path] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_c] FOREIGN KEY([attach_cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_c]
GO

ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_created]
GO

ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_m] FOREIGN KEY([method_id])
REFERENCES [dbo].[method] ([method_id])
GO

ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_m]
GO

ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_modified]
GO

ALTER TABLE [dbo].[method_attach]  WITH CHECK ADD  CONSTRAINT [fk_mat_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[method_attach] CHECK CONSTRAINT [fk_mat_owned]
GO


