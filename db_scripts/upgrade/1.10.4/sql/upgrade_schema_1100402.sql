/*
   Schema change to add DOI column to accession table and index it
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

BEGIN TRANSACTION
GO

DECLARE @drop_sql NVARCHAR(MAX) = N'';
DECLARE @table_name nvarchar(255) = 'accession';

-- drop default constraints on table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name)
	+ ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + '; '
FROM sys.default_constraints AS dc
INNER JOIN sys.tables AS ct ON dc.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE dc.parent_object_id = OBJECT_ID(@table_name)

-- drop foreign key constraints involving the table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + '; '
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE fk.parent_object_id = OBJECT_ID(@table_name) OR fk.referenced_object_id = OBJECT_ID(@table_name)

-- execute the drops
EXECUTE sp_executesql @drop_sql
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_species SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.site SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.site', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.site', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.site', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_accession
	(
	accession_id int NOT NULL IDENTITY (1, 1),
	doi nvarchar(20) NULL,
	accession_number_part1 nvarchar(50) NOT NULL,
	accession_number_part2 int NULL,
	accession_number_part3 nvarchar(50) NULL,
	is_core nvarchar(1) NOT NULL,
	is_backed_up nvarchar(1) NOT NULL,
	backup_location1_site_id int NULL,
	backup_location2_site_id int NULL,
	status_code nvarchar(20) NOT NULL,
	life_form_code nvarchar(20) NULL,
	improvement_status_code nvarchar(20) NULL,
	reproductive_uniformity_code nvarchar(20) NULL,
	initial_received_form_code nvarchar(20) NULL,
	initial_received_date datetime2(7) NULL,
	initial_received_date_code nvarchar(20) NULL,
	taxonomy_species_id int NOT NULL,
	is_web_visible nvarchar(1) NOT NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_accession SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_accession TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_accession TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_accession TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_accession TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_accession TO gg_user  AS dbo
GO
ALTER TABLE dbo.Tmp_accession ADD CONSTRAINT
	DF__accession__is_we__7F60ED59 DEFAULT ('Y') FOR is_web_visible
GO
SET IDENTITY_INSERT dbo.Tmp_accession ON
GO
IF EXISTS(SELECT * FROM dbo.accession)
	 EXEC('INSERT INTO dbo.Tmp_accession (accession_id, accession_number_part1, accession_number_part2, accession_number_part3, is_core, is_backed_up, backup_location1_site_id, backup_location2_site_id, status_code, life_form_code, improvement_status_code, reproductive_uniformity_code, initial_received_form_code, initial_received_date, initial_received_date_code, taxonomy_species_id, is_web_visible, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT accession_id, accession_number_part1, accession_number_part2, accession_number_part3, is_core, is_backed_up, backup_location1_site_id, backup_location2_site_id, status_code, life_form_code, improvement_status_code, reproductive_uniformity_code, initial_received_form_code, initial_received_date, initial_received_date_code, taxonomy_species_id, is_web_visible, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.accession WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_accession OFF
GO
DROP TABLE dbo.accession
GO
EXECUTE sp_rename N'dbo.Tmp_accession', N'accession', 'OBJECT' 
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	PK_accession PRIMARY KEY CLUSTERED 
	(
	accession_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_a_part1 ON dbo.accession
	(
	accession_number_part1
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_a_part2 ON dbo.accession
	(
	accession_number_part2
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_a_part3 ON dbo.accession
	(
	accession_number_part3
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_a_created ON dbo.accession
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_a_modified ON dbo.accession
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_a_owned ON dbo.accession
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_a_t ON dbo.accession
	(
	taxonomy_species_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_ac ON dbo.accession
	(
	accession_number_part1,
	accession_number_part2,
	accession_number_part3
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	fk_a_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	fk_a_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	fk_a_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	fk_a_s1 FOREIGN KEY
	(
	backup_location1_site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	fk_a_s2 FOREIGN KEY
	(
	backup_location2_site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	fk_a_t FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_source ADD CONSTRAINT
	fk_as_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_source SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_source', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_source', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_source', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_quarantine ADD CONSTRAINT
	fk_aq_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_quarantine SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_quarantine', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_quarantine', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_quarantine', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_pedigree ADD CONSTRAINT
	fk_ap_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_pedigree ADD CONSTRAINT
	fk_ap_a_female FOREIGN KEY
	(
	female_accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_pedigree ADD CONSTRAINT
	fk_ap_a_male FOREIGN KEY
	(
	male_accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_pedigree SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_pedigree', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_pedigree', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_pedigree', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_ipr ADD CONSTRAINT
	fk_ar_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_ipr SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_ipr', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_ipr', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_ipr', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.citation', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.inventory ADD CONSTRAINT
	fk_i_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.inventory', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.inventory', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.inventory', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.web_user_cart_item ADD CONSTRAINT
	fk_wuci_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.web_user_cart_item SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.web_user_cart_item', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.web_user_cart_item', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.web_user_cart_item', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_action ADD CONSTRAINT
	fk_aa_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_action SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_action', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_action', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_action', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.web_order_request_item ADD CONSTRAINT
	fk_wori_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.web_order_request_item SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.web_order_request_item', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.web_order_request_item', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.web_order_request_item', 'Object', 'CONTROL') as Contr_Per EXECUTE sp_fulltext_database N'enable'
GO
IF EXISTS(SELECT 1 FROM sys.fulltext_catalogs WHERE name = 'gringlobalftsc') BEGIN
	CREATE FULLTEXT INDEX ON dbo.accession (note LANGUAGE 1033) KEY INDEX PK_accession ON gringlobalftsc WITH CHANGE_TRACKING AUTO;
	ALTER FULLTEXT INDEX ON dbo.accession ENABLE; 
END
EXECUTE sp_fulltext_database N'disable'
GO

-- need to use filter to allow multiple null rows
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_doi
ON dbo.accession(doi)
WHERE doi  IS NOT NULL;
GO

