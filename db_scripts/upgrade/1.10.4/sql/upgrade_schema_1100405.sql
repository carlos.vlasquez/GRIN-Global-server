/*
   Schema change to add order_request_item_action table
*/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[order_request_item_action](
	[order_request_item_action_id] [int] IDENTITY(1,1) NOT NULL,
	[order_request_item_id] [int] NOT NULL,
	[action_name_code] [nvarchar](20) NOT NULL,
	[started_date] [datetime2](7) NOT NULL,
	[started_date_code] [nvarchar](20) NULL,
	[completed_date] [datetime2](7) NULL,
	[completed_date_code] [nvarchar](20) NULL,
	[action_information] [nvarchar](max) NULL,
	[action_cost] [decimal](18, 5) NULL,
	[cooperator_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_order_request_item_action] PRIMARY KEY CLUSTERED 
(
	[order_request_item_action_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

-- constraints

ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_c] FOREIGN KEY([cooperator_id])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_c]
GO

ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_created]
GO

ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_modified]
GO

ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_ori] FOREIGN KEY([order_request_item_id])
REFERENCES [dbo].[order_request_item] ([order_request_item_id])
GO

ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_ori]
GO

ALTER TABLE [dbo].[order_request_item_action]  WITH CHECK ADD  CONSTRAINT [fk_oria_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[order_request_item_action] CHECK CONSTRAINT [fk_oria_owned]
GO

-- indexes
/****** Object:  Index [ndx_fk_oria_c]    Script Date: 12/3/2018 10:37:02 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_c] ON [dbo].[order_request_item_action]
(
	[cooperator_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_oria_created]    Script Date: 12/3/2018 10:37:02 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_created] ON [dbo].[order_request_item_action]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_oria_modified]    Script Date: 12/3/2018 10:37:02 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_modified] ON [dbo].[order_request_item_action]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_oria_ori]    Script Date: 12/3/2018 10:37:02 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_ori] ON [dbo].[order_request_item_action]
(
	[order_request_item_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Index [ndx_fk_oria_owned]    Script Date: 12/3/2018 10:37:02 AM ******/
CREATE NONCLUSTERED INDEX [ndx_fk_oria_owned] ON [dbo].[order_request_item_action]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

SET ANSI_PADDING ON
GO

/****** Object:  Index [ndx_uniq_ora]    Script Date: 12/3/2018 10:37:02 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_oria] ON [dbo].[order_request_item_action]
(
	[order_request_item_id] ASC,
	[action_name_code] ASC,
	[started_date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

