/*
   Schema change to create taxonomy_cwr_crop, taxonomy_cwr_map, taxonomy_cwr_trait tables
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[taxonomy_cwr_crop](
	[taxonomy_cwr_crop_id] [int] IDENTITY(1,1) NOT NULL,
	[crop_name] [nvarchar](150) NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_cwr_crop] PRIMARY KEY CLUSTERED ([taxonomy_cwr_crop_id] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcc_created] ON [dbo].[taxonomy_cwr_crop] ([created_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcc_modified] ON [dbo].[taxonomy_cwr_crop] ([modified_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcc_owned] ON [dbo].[taxonomy_cwr_crop] ([owned_by] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcc] ON [dbo].[taxonomy_cwr_crop] ([crop_name] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[taxonomy_cwr_crop]  WITH CHECK ADD  CONSTRAINT [fk_tcc_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_crop] CHECK CONSTRAINT [fk_tcc_created]
GO

ALTER TABLE [dbo].[taxonomy_cwr_crop]  WITH CHECK ADD  CONSTRAINT [fk_tcc_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_crop] CHECK CONSTRAINT [fk_tcc_modified]
GO

ALTER TABLE [dbo].[taxonomy_cwr_crop]  WITH CHECK ADD  CONSTRAINT [fk_tcc_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_crop] CHECK CONSTRAINT [fk_tcc_owned]
GO


/*
   Schema change to create taxonomy_cwr_map table
*/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[taxonomy_cwr_map](
	[taxonomy_cwr_map_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[taxonomy_cwr_crop_id] [int] NOT NULL,
	[crop_common_name] [nvarchar](100) NULL,
	[is_crop] [nvarchar](1) NOT NULL,
	[genepool_code] [nvarchar](20) NULL,
	[is_graftstock] [nvarchar](1) NOT NULL,
	[is_potential] [nvarchar](1) NOT NULL,
	[citation_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_cwr_map] PRIMARY KEY CLUSTERED ([taxonomy_cwr_map_id] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrm_ts] ON [dbo].[taxonomy_cwr_map] ([taxonomy_species_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrm_tc] ON [dbo].[taxonomy_cwr_map] ([taxonomy_cwr_crop_id] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrm_cit] ON [dbo].[taxonomy_cwr_map] ([citation_id] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrm_created] ON [dbo].[taxonomy_cwr_map] ([created_by] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrm_modified] ON [dbo].[taxonomy_cwr_map] ([modified_by] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrm_owned] ON [dbo].[taxonomy_cwr_map] ([owned_by] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcwrm] ON [dbo].[taxonomy_cwr_map]
(
	[taxonomy_species_id] ASC,
	[taxonomy_cwr_crop_id] ASC,
	[citation_id] ASC,
	[genepool_code] ASC,
	[is_graftstock] ASC,
	[is_potential] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[taxonomy_cwr_map]  WITH CHECK ADD  CONSTRAINT [fk_tcwrm_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_map] CHECK CONSTRAINT [fk_tcwrm_ts]
GO

ALTER TABLE [dbo].[taxonomy_cwr_map]  WITH CHECK ADD  CONSTRAINT [fk_tcwrm_tc] FOREIGN KEY([taxonomy_cwr_crop_id])
REFERENCES [dbo].[taxonomy_cwr_crop] ([taxonomy_cwr_crop_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_map] CHECK CONSTRAINT [fk_tcwrm_tc]
GO

ALTER TABLE [dbo].[taxonomy_cwr_map]  WITH CHECK ADD  CONSTRAINT [fk_tcwrm_cit] FOREIGN KEY([citation_id])
REFERENCES [dbo].[citation] ([citation_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_map] CHECK CONSTRAINT [fk_tcwrm_cit]
GO

ALTER TABLE [dbo].[taxonomy_cwr_map]  WITH CHECK ADD  CONSTRAINT [fk_tcwrm_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_map] CHECK CONSTRAINT [fk_tcwrm_created]
GO

ALTER TABLE [dbo].[taxonomy_cwr_map]  WITH CHECK ADD  CONSTRAINT [fk_tcwrm_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_map] CHECK CONSTRAINT [fk_tcwrm_modified]
GO

ALTER TABLE [dbo].[taxonomy_cwr_map]  WITH CHECK ADD  CONSTRAINT [fk_tcwrm_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_map] CHECK CONSTRAINT [fk_tcwrm_owned]
GO


/*
   Schema change to create taxonomy_cwr_trait table
*/

CREATE TABLE [dbo].[taxonomy_cwr_trait](
	[taxonomy_cwr_trait_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_cwr_map_id] [int] NOT NULL,
	[trait_class_code] [nvarchar](20) NULL,
	[is_potential] [nvarchar](1) NOT NULL,
	[breeding_type_code] [nvarchar](20) NULL,
	[breeding_usage_note] [nvarchar](1000) NULL,
	[ontology_trait_identifier] [nvarchar](20) NULL,
	[citation_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,

 CONSTRAINT [PK_taxonomy_cwr_trait] PRIMARY KEY CLUSTERED ([taxonomy_cwr_trait_id] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrt_tcc] ON [dbo].[taxonomy_cwr_trait] ([taxonomy_cwr_map_id] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrt_cit] ON [dbo].[taxonomy_cwr_trait] ([citation_id] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrt_created] ON [dbo].[taxonomy_cwr_trait] ([created_by] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrt_modified] ON [dbo].[taxonomy_cwr_trait] ([modified_by] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwrt_owned] ON [dbo].[taxonomy_cwr_trait] ([owned_by] ASC)
  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcwrt] ON [dbo].[taxonomy_cwr_trait]
(
	[taxonomy_cwr_map_id] ASC,
	[citation_id] ASC,
	[ontology_trait_identifier] ASC,
	[breeding_type_code] ASC,
	[is_potential] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[taxonomy_cwr_trait]  WITH CHECK ADD  CONSTRAINT [fk_tcwrt_tc] FOREIGN KEY([taxonomy_cwr_map_id])
REFERENCES [dbo].[taxonomy_cwr_map] ([taxonomy_cwr_map_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_trait] CHECK CONSTRAINT [fk_tcwrt_tc]
GO

ALTER TABLE [dbo].[taxonomy_cwr_trait]  WITH CHECK ADD  CONSTRAINT [fk_tcwrt_cit] FOREIGN KEY([citation_id])
REFERENCES [dbo].[citation] ([citation_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_trait] CHECK CONSTRAINT [fk_tcwrt_cit]
GO

ALTER TABLE [dbo].[taxonomy_cwr_trait]  WITH CHECK ADD  CONSTRAINT [fk_tcwrt_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_trait] CHECK CONSTRAINT [fk_tcwrt_created]
GO

ALTER TABLE [dbo].[taxonomy_cwr_trait]  WITH CHECK ADD  CONSTRAINT [fk_tcwrt_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_trait] CHECK CONSTRAINT [fk_tcwrt_modified]
GO

ALTER TABLE [dbo].[taxonomy_cwr_trait]  WITH CHECK ADD  CONSTRAINT [fk_tcwrt_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO
ALTER TABLE [dbo].[taxonomy_cwr_trait] CHECK CONSTRAINT [fk_tcwrt_owned]
GO
