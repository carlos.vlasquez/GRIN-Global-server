<SecureDataDataSet>
  <sys_dataview>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <is_enabled>Y</is_enabled>
    <is_readonly>Y</is_readonly>
    <category_code>Lookups</category_code>
    <database_area_code>Lookup Table</database_area_code>
    <database_area_code_sort_order>0</database_area_code_sort_order>
    <is_transform>N</is_transform>
    <created_date>2019-11-05T18:20:39-05:00</created_date>
    <modified_date>2019-11-06T21:12:47.6388943-05:00</modified_date>
  </sys_dataview>
  <sys_dataview_lang>
    <sys_lang_id>1</sys_lang_id>
    <title>Taxonomy CWR Map Lookup</title>
    <description>Taxonomy CWR Map Lookup</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_lang>
    <sys_lang_id>9</sys_lang_id>
    <title>Taxonomy CWR Map Lookup</title>
    <description>Taxonomy CWR Map Lookup</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>x-en-CODE</ietf_tag>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_sql>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <database_engine_tag>sqlserver</database_engine_tag>
    <sql_statement>SELECT
  tcm.taxonomy_cwr_map_id AS value_member
	, CONCAT(tcc.crop_name
		, '; ', ts.name
		, CASE WHEN (SELECT count(*) FROM taxonomy_cwr_map WHERE taxonomy_cwr_crop_id = tcm.taxonomy_cwr_crop_id AND taxonomy_species_id = tcm.taxonomy_species_id) = 1
			THEN '' ELSE CONCAT(
				CASE WHEN is_potential = 'Y' THEN ' Potential' ELSE '' END
				, CASE WHEN is_crop = 'Y' THEN ' Crop' ELSE '' END
				, CASE WHEN genepool_code IS NOT NULL THEN CONCAT(' ', genepool_code) ELSE '' END
				, CASE WHEN is_graftstock = 'Y' THEN ' Graftstock' ELSE '' END
				) END ) AS display_member
  , tcm.taxonomy_cwr_crop_id
  , tcm.taxonomy_species_id
FROM
    taxonomy_cwr_map tcm
    LEFT JOIN taxonomy_species ts ON  tcm.taxonomy_species_id = ts.taxonomy_species_id 
    LEFT JOIN taxonomy_cwr_crop tcc ON  tcc.taxonomy_cwr_crop_id = tcm.taxonomy_cwr_crop_id 
WHERE
  ((tcm.created_date &gt; COALESCE(:createddate, '1753-01-01'))
   OR (tcm.modified_date &gt; COALESCE(:modifieddate, '1753-01-01'))
   OR (tcm.taxonomy_cwr_map_id IN (:valuemember))
   OR (tcm.taxonomy_cwr_map_id BETWEEN :startpkey AND :stoppkey)
 )

</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_param>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <param_name>:createddate</param_name>
    <param_type>DATETIME</param_type>
    <sort_order>0</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <param_name>:modifieddate</param_name>
    <param_type>DATETIME</param_type>
    <sort_order>1</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <param_name>:valuemember</param_name>
    <param_type>INTEGERCOLLECTION</param_type>
    <sort_order>2</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <param_name>:startpkey</param_name>
    <param_type>INTEGER</param_type>
    <sort_order>3</sort_order>
  </sys_dataview_param>
  <sys_dataview_param>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <param_name>:stoppkey</param_name>
    <param_type>INTEGER</param_type>
    <sort_order>4</sort_order>
  </sys_dataview_param>
  <sys_dataview_field>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <field_name>value_member</field_name>
    <table_name>taxonomy_cwr_map</table_name>
    <table_field_name>taxonomy_cwr_map_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>Y</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>0</sort_order>
    <table_alias_name>tcm</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <field_name>display_member</field_name>
    <table_name>taxonomy_cwr_crop</table_name>
    <table_field_name>crop_name</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>1</sort_order>
    <table_alias_name>tcc</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=tcm.taxonomy_cwr_crop_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <field_name>taxonomy_cwr_crop_id</field_name>
    <table_name>taxonomy_cwr_map</table_name>
    <table_field_name>taxonomy_cwr_crop_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>2</sort_order>
    <foreign_key_dataview_name>taxonomy_cwr_crop_lookup</foreign_key_dataview_name>
    <table_alias_name>tcm</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>taxonomy_cwr_map_lookup</dataview_name>
    <field_name>taxonomy_species_id</field_name>
    <table_name>taxonomy_cwr_map</table_name>
    <table_field_name>taxonomy_species_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>3</sort_order>
    <foreign_key_dataview_name>taxonomy_species_lookup</foreign_key_dataview_name>
    <table_alias_name>tcm</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
</SecureDataDataSet>