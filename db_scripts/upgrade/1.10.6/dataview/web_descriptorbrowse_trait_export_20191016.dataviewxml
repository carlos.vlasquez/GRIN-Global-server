<SecureDataDataSet>
  <sys_dataview>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <is_enabled>Y</is_enabled>
    <is_readonly>Y</is_readonly>
    <category_code>Web Application</category_code>
    <database_area_code_sort_order>0</database_area_code_sort_order>
    <is_transform>N</is_transform>
    <transform_field_for_names />
    <transform_field_for_captions />
    <transform_field_for_values />
    <configuration_options />
    <created_date>2013-09-16T23:03:39-04:00</created_date>
    <modified_date>2019-10-16T16:41:15.0930075-04:00</modified_date>
  </sys_dataview>
  <sys_dataview_lang>
    <sys_lang_id>1</sys_lang_id>
    <title>Web Descriptor Result Export</title>
    <description>Web Descriptor Result Export</description>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
  </sys_dataview_lang>
  <sys_dataview_sql>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <database_engine_tag>sqlserver</database_engine_tag>
    <sql_statement>select distinct
	a.accession_id,
	a.accession_number_part1 as accession_prefix,
	a.accession_number_part2 as accession_number,
	case 
		when ct.is_coded = 'Y' then ctc.code
    		when cto.numeric_value is not null then convert(varchar, cast(cto.numeric_value as float))    	
    	else 
        	cto.string_value 
    	end as observation_value,
	coalesce(coalesce(ctcl.title, ctc.code, convert(nvarchar, cto.crop_trait_code_id)), string_value, convert(nvarchar, cast(numeric_value as float))) as observation_definition,
	ct.coded_name as descriptor_name,
	m.name as method_name,
	a.accession_number_part3 as accession_suffix,
   	(select TOP 1 plant_name from accession_inv_name an join inventory i on an.inventory_id = i.inventory_id where a.accession_id = i.accession_id and plant_name_rank = (select MIN(plant_name_rank) from accession_inv_name an2 join inventory i2 on an2.inventory_id = i2.inventory_id where a.accession_id = i2.accession_id)) as plant_name,
	t.name as taxon,
	(select cvl.title
   		from code_value cv left join code_value_lang cvl on cv.code_value_id = cvl.code_value_id 
   		where cv.group_name = 'GEOGRAPHY_COUNTRY_CODE' and cvl.sys_lang_id = __LANGUAGEID__ 
   		and cv.value = (select top 1 g.country_code from geography g join accession_source asr on g.geography_id = asr.geography_id
   		and asr.is_origin = 'Y' and asr.accession_id = a.accession_id)) as origin,
	cto.original_value,	
	cto.frequency,    	
	cto.minimum_value as low,
    	cto.maximum_value as hign,
    	cto.mean_value as mean,
    	cto.standard_deviation as sdev,
    	cto.sample_size as ssize,
	i.inventory_number_part1 as inventory_prefix,
	i.inventory_number_part2 as inventory_number,
	i.inventory_number_part3 as inventory_suffix,
	a.note as accession_comment
from 
	accession a 
	inner join inventory i 
        	on a.accession_id = i.accession_id 
    	join crop_trait_observation cto 
        	on cto.inventory_id = i.inventory_id 
    	join crop_trait ct 
        	on cto.crop_trait_id = ct.crop_trait_id 
    	left join crop_trait_code ctc 
        	on cto.crop_trait_code_id = ctc.crop_trait_code_id 
    	left join method m 
		on cto.method_id = m.method_id 
	join taxonomy_species t
		on a.taxonomy_species_id = t.taxonomy_species_id
	left join crop_trait_code_lang ctcl
        on ctc.crop_trait_code_id = ctcl.crop_trait_code_id
        and ctcl.sys_lang_id = __LANGUAGEID__ 
where 
	a.accession_id in (select distinct i.accession_id from inventory i 
		join crop_trait_observation cto on i.inventory_id = cto.inventory_id 
	where :where )
order by a.accession_id, observation_value</sql_statement>
  </sys_dataview_sql>
  <sys_dataview_param>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <param_name>:where</param_name>
    <param_type>STRINGREPLACEMENT</param_type>
    <sort_order>0</sort_order>
  </sys_dataview_param>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>accession_id</field_name>
    <table_name>accession</table_name>
    <table_field_name>accession_id</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>0</sort_order>
    <table_alias_name>a</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=i.accession_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>accession_prefix</field_name>
    <table_name>accession</table_name>
    <table_field_name>accession_number_part1</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>1</sort_order>
    <table_alias_name>a</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=i.accession_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>accession_number</field_name>
    <table_name>accession</table_name>
    <table_field_name>accession_number_part2</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>2</sort_order>
    <table_alias_name>a</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=i.accession_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>observation_value</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>3</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>observation_definition</field_name>
    <table_name>crop_trait_code_lang</table_name>
    <table_field_name>title</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>4</sort_order>
    <table_alias_name>ctcl</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>descriptor_name</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>5</sort_order>
    <gui_hint>TEXT_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>method_name</field_name>
    <table_name>method</table_name>
    <table_field_name>name</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>6</sort_order>
    <table_alias_name>m</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>accession_suffix</field_name>
    <table_name>accession</table_name>
    <table_field_name>accession_number_part3</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>7</sort_order>
    <table_alias_name>a</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=i.accession_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>plant_name</field_name>
    <table_name>accession_inv_name</table_name>
    <table_field_name>plant_name</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>8</sort_order>
    <table_alias_name>i</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>taxon</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>9</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>origin</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>10</sort_order>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>original_value</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>11</sort_order>
    <gui_hint>TEXT_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>frequency</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>12</sort_order>
    <gui_hint>DECIMAL_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>low</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>13</sort_order>
    <gui_hint>DECIMAL_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>hign</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>14</sort_order>
    <gui_hint>DECIMAL_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>mean</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>15</sort_order>
    <gui_hint>DECIMAL_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>sdev</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>16</sort_order>
    <gui_hint>DECIMAL_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>ssize</field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>17</sort_order>
    <gui_hint>DECIMAL_CONTROL</gui_hint>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>inventory_prefix</field_name>
    <table_name>inventory</table_name>
    <table_field_name>inventory_number_part1</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>18</sort_order>
    <table_alias_name>i</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>inventory_number</field_name>
    <table_name>inventory</table_name>
    <table_field_name>inventory_number_part2</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>19</sort_order>
    <table_alias_name>i</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>inventory_suffix</field_name>
    <table_name>inventory</table_name>
    <table_field_name>inventory_number_part3</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>20</sort_order>
    <table_alias_name>i</table_alias_name>
    <is_visible>Y</is_visible>
  </sys_dataview_field>
  <sys_dataview_field>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>accession_comment</field_name>
    <table_name>accession</table_name>
    <table_field_name>note</table_field_name>
    <is_readonly>Y</is_readonly>
    <is_primary_key>N</is_primary_key>
    <is_transform>N</is_transform>
    <sort_order>21</sort_order>
    <table_alias_name>a</table_alias_name>
    <is_visible>Y</is_visible>
    <configuration_options>join_children=i.accession_id;</configuration_options>
  </sys_dataview_field>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>descriptor_name</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>الاسم المشفر للصفة</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>descriptor_name</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>coded_name</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>descriptor_name</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Trait Name</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>descriptor_name</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Nombre del Descriptor Codificado</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>descriptor_name</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Nom court</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>descriptor_name</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>Сокращенное название</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>frequency</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>تردد</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>frequency</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>frequency</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>frequency</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Frequency</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>frequency</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Frecuencia</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>frequency</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Fréquence</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>frequency</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Frequência</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>frequency</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>Частота</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>hign</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>القيمة العظمى </title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>hign</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>maximum_value</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>hign</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Maximum Value</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>hign</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Valor máximo</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>hign</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Valeur maximale</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>hign</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Valor máximo</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>hign</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>максимальное значение</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>low</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>القيمة الدنيا </title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>low</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>minimum_value</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>low</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Minimum Value</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>low</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Valor mínimo</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>low</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Valeur minimale</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>low</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Valor mínimo</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>low</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>Минимальное значение</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>mean</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>القيمة المتوسطة</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>mean</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>mean_value</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>mean</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Mean Value</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>mean</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Valor promedio</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>mean</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Valeur moyenne</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>mean</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Valor significativo</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>mean</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>среднее значение</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>original_value</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>القيمة الأصلية</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>original_value</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>original_value</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>original_value</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Original Value</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>original_value</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Valor original</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>original_value</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Valeur initiale</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>original_value</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Valor original</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>original_value</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>исходное значение</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>sdev</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>الانحراف المعياري</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>sdev</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>standard_deviation</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>sdev</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Standard Deviation</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>sdev</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Desviación estándar</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>sdev</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Ecart type</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>sdev</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Desvio padrão</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>sdev</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>стандартное отклонение</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>ssize</field_name>
    <iso_639_3_tag>ARA</iso_639_3_tag>
    <ietf_tag>ar</ietf_tag>
    <title>حجم العينة</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>ssize</field_name>
    <iso_639_3_tag>SYS</iso_639_3_tag>
    <ietf_tag>en-SYS</ietf_tag>
    <title>sample_size</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>ssize</field_name>
    <iso_639_3_tag>ENG</iso_639_3_tag>
    <ietf_tag>en-US</ietf_tag>
    <title>Sample Size</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>ssize</field_name>
    <iso_639_3_tag>SPA</iso_639_3_tag>
    <ietf_tag>es-419</ietf_tag>
    <title>Tamaño de la muestra</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>ssize</field_name>
    <iso_639_3_tag>FRA</iso_639_3_tag>
    <ietf_tag>fr-FR</ietf_tag>
    <title>Dimention de l'échantillon</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>ssize</field_name>
    <iso_639_3_tag>POR</iso_639_3_tag>
    <ietf_tag>pt-BR</ietf_tag>
    <title>Tamanho de amostra</title>
    <description />
  </sys_dataview_field_lang>
  <sys_dataview_field_lang>
    <dataview_name>web_descriptorbrowse_trait_export</dataview_name>
    <field_name>ssize</field_name>
    <iso_639_3_tag>RUS</iso_639_3_tag>
    <ietf_tag>ru-RU</ietf_tag>
    <title>размер образца</title>
    <description />
  </sys_dataview_field_lang>
</SecureDataDataSet>