REM *** copy_installer_files.bat
@ECHO OFF

set GG=%~dp0\..
set TEMPINST=%GG%\scratch\webinstaller
set LOCALIISDIR=C:\inetpub\wwwroot\gringlobal
set APPVERSION=latest

REM ***************************************************************************************************************************
REM Copy setup.exe files to scrach directory since post compile can't
REM ***************************************************************************************************************************
echo %date% %time% Copying setup.exe files 
copy "%GG%\GrinGlobal.Admin.Setup\bin\Debug\setup.exe" "%TEMPINST%\GrinGlobal_Admin_Setup.exe"
copy "%GG%\GrinGlobal.Database.Setup\bin\Debug\setup.exe" "%TEMPINST%\GrinGlobal_Database_Setup.exe"
copy "%GG%\GrinGlobal.Updater.Setup\bin\Debug\setup.exe" "%TEMPINST%\GrinGlobal_Updater_Setup.exe"

REM ***************************************************************************************************************************
REM Copy msi / exe files to version-specific installer path on local webserver
REM (so Updater can locate them when a client requests them from the local website)
REM ***************************************************************************************************************************
echo %date% %time% Copying setup files and data cab files to local web server...
mkdir "%LOCALIISDIR%\uploads\installers\%APPVERSION%\"
copy "%TEMPINST%\*.*" "%LOCALIISDIR%\uploads\installers\%APPVERSION%\"

REM ***************************************************************************************************************************
REM Copy Updater msi / exe files to installer path on local webserver
REM (so Updater can locate them after a client downloads them)
REM ***************************************************************************************************************************
echo %date% %time% Copying Updater to download area on local web server...
copy "%TEMPINST%\GrinGlobal_Updater_Setup.*" "%LOCALIISDIR%\uploads\installers\"

echo Check for errors above!
pause
