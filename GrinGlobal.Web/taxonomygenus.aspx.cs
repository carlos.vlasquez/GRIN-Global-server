﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Data;
using System.Text.RegularExpressions;
using System.Text;

namespace GrinGlobal.Web
{
    public partial class TaxonomyGenus : System.Web.UI.Page
    {
        string _type;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (this.Request.QueryString["type"] != null)
                {
                    _type = this.Request.QueryString["type"].ToString();
                    ViewState["type"] = this.Request.QueryString["type"].ToString();
                }
                else _type = "genus";
                bindData(Toolkit.ToInt32(Request.QueryString["id"], 0));
            }
        }

        private void bindData(int taxonomygenusID)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                bindTaxonomy(sd, taxonomygenusID);
                bindReferences(sd, taxonomygenusID);
                bindOtherReferences();
                bindSynonyms(sd, taxonomygenusID);
                //if (_type == "genus") bindCheckOther(sd, taxonomygenusID);
                bindSubdivisions(sd, taxonomygenusID);
                bindImages(sd, taxonomygenusID);
                bindCitation();
            }
        }

        private void bindTaxonomy(SecureData sd, int taxonomygenusID)
        {
        
            string value = "";
            string altfam = "";
            var dt = sd.GetData("web_taxonomygenus_summary", ":taxonomygenusid=" + taxonomygenusID, 0, 0).Tables["web_taxonomygenus_summary"];
            int currentid;
            //KMK 05/30/2018  Removed alt_family from taxonomygenus_summary to allow for multiple alt families,  then added the sql below
            //to get it to add to the dt from the DV
            if (dt != null)
            {
               
                currentid = Convert.ToInt32(dt.Rows[0]["current_taxonomy_genus_id"]);
                dt.Columns.Add("altfamily");
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    DataTable dt2 = dm.Read(@"
    select  tf2.family_name from taxonomy_genus tg  left join taxonomy_alt_family_map tafm
	on tg.taxonomy_genus_id = tafm.taxonomy_genus_id
    left join taxonomy_family tf2
	on tafm.taxonomy_family_id = tf2.taxonomy_family_id  where tg.taxonomy_genus_id = :taxonomygenusid
    and tf2.family_name is not null",
                                new DataParameters(":taxonomygenusid", taxonomygenusID));

                    if (dt2.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt2.Rows.Count; i++)
                        {
                            altfam += " " + dt2.Rows[i]["family_name"].ToString() + ",";
                        }
                        altfam = altfam.TrimEnd(',').Trim();
                        dt.Rows[0]["altfamily"] = altfam;
                    }
                    else
                        dt.Rows[0]["altfamily"] = "";

                    //07/05/18 KMK find synonym and create link to it.  Currently only gets the genus, not the sections.
              
                    if (currentid != taxonomygenusID)
                    {
                    DataTable d1 = dm.Read(@"
                         Select qualifying_code
                         from taxonomy_genus where taxonomy_genus_id = :taxonomygenusid",
                    new DataParameters(":taxonomygenusid",taxonomygenusID));
//KMK Mar 2019 Changed code to have hybrid code show in synonyms
                        DataTable dtsyn = dm.Read(@"
    Select qualifying_code,
    hybrid_code,
    genus_name,
    genus_authority,
	subgenus_name,
	section_name,
	subsection_name
    from taxonomy_genus where taxonomy_genus_id = :taxonomygenusid",
                    new DataParameters(":taxonomygenusid", currentid));
                        StringBuilder strName = new StringBuilder();
                        if (dtsyn.Rows.Count > 0)
                        {
                            StringBuilder strLink = new StringBuilder();
                            strLink.Append("<a href='taxonomygenus.aspx?");
                            if (dtsyn.Rows[0]["hybrid_code"].ToString() == "X")
                                strName.Append("×");
                            else if (dtsyn.Rows[0]["hybrid_code"].ToString() == "+")
                                strName.Append("+");
                            strName.Append("<i>").Append(dtsyn.Rows[0]["genus_name"].ToString()).Append("</i>");
                            strName.Append(" ").Append(dtsyn.Rows[0]["genus_authority"].ToString());
                            switch (_type)
                            {
                                case "subgenus":
                                    if (dtsyn.Rows[0]["subgenus_name"].ToString() != "")
                                    {
                                        strName.Append(" subg.<i> ").Append(dtsyn.Rows[0]["subgenus_name"].ToString()).Append("</i> ");
                                        strLink.Append("type=subgenus&id=").Append(currentid).Append("'>");
                                    }
                                    break;
                                case "section":
                                    if (dtsyn.Rows[0]["section_name"].ToString() != "")
                                    {
                                        strName.Append(" sect.<i> ").Append(dtsyn.Rows[0]["section_name"].ToString()).Append("</i> ");
                                        strLink.Append("type=section&id=").Append(currentid).Append("'>");
                                    }
                                    break;
                                case "subsection":
                                    if (dtsyn.Rows[0]["subsection_name"].ToString() != "")
                                    {
                                        strName.Append(" subs.<i> ").Append(dtsyn.Rows[0]["subsection_name"].ToString()).Append("</i> ");
                                        strLink.Append("type=subsection&id=").Append(currentid).Append("'>");
                                    }
                                    break;
                                default:
                                    strLink.Append("id=").Append(currentid).Append("'>");
                                    break;

                            }
                            strName.Append("</a>");
                           
                            if (d1.Rows[0]["qualifying_code"].ToString() == "~")
                                lblSynonym.Text = "Possible synonym of: " + strLink.ToString() + strName.ToString();
                            else if (d1.Rows[0]["qualifying_code"].ToString() == "=~")
                                lblSynonym.Text = "Usually considered a synonym of: " + strLink.ToString() + strName.ToString();
                            else
                            lblSynonym.Text = "Synonym of: " + strLink.ToString() + strName.ToString();
                            strName.Clear();
                        }
                    }
                }
                dvGenus.DataSource = dt;
                dvGenus.DataBind();
                ViewState["genus_name"] = dt.Rows[0]["genus_name"];
                ViewState["genus_short_name"] = dt.Rows[0]["genus_short_name"];

                if (ViewState["type"] != null)
                {
                    _type = ViewState["type"].ToString();
                    switch (ViewState["type"].ToString())
                    {
                        case "subgenus":
                            ViewState["subgenus_name"] = dt.Rows[0]["subgenus_name"];
                            _type = "subgenus";
                            value = ViewState["genus_short_name"].ToString() + " subg. " + dt.Rows[0]["subgenus_name"];
                            lblGenus.Text = @"<a href='taxonomygenus.aspx?id=" + dt.Rows[0]["genus_id"].ToString() +
                             "' title='Link to GRIN-Global report for genus " + dt.Rows[0]["genus_link_name"].ToString() + "'>" +
                               dt.Rows[0]["genus_name"].ToString() + "</a>";
                            lblSubGenus.Text = dt.Rows[0]["subgenus_name"].ToString();
                            pnlSubGenus.Visible = true;
                            break;
                        case "section":
                            ViewState["section_name"] = dt.Rows[0]["section_name"];
                            _type = "section";
                            value = ViewState["genus_short_name"].ToString() + " sect. " + dt.Rows[0]["section_name"];
                            lblGenus.Text = @"<a href='taxonomygenus.aspx?id=" + dt.Rows[0]["genus_id"].ToString() +
                           "' title='Link to GRIN-Global report for genus " + dt.Rows[0]["genus_link_name"].ToString() + "'>" +
                             dt.Rows[0]["genus_name"].ToString() + "</a>";
                            if (dt.Rows[0]["subgenus_name"] != DBNull.Value)
                            {
                                lblSubGenus.Text = @"<a href='taxonomygenus.aspx?type=subgenus&id=" + dt.Rows[0]["subgenus_id"].ToString() +
                           "' title='Link to GRIN-Global report for subgenus " + dt.Rows[0]["subgenus_name"].ToString() + "'>" +
                             dt.Rows[0]["subgenus_name"].ToString() + "</a>";
                                pnlSubGenus.Visible = true;
                            }
                            lblSect.Text = dt.Rows[0]["section_name"].ToString();
                            pnlSect.Visible = true;
                            break;
                        case "subsection":
                            ViewState["subsection_name"] = dt.Rows[0]["subsection_name"];
                            _type = "subsection";
                            value = ViewState["genus_short_name"].ToString() + " subsect. " + dt.Rows[0]["subsection_name"];
                            lblGenus.Text = @"<a href='taxonomygenus.aspx?id=" + dt.Rows[0]["genus_id"].ToString() +
                                   "' title='Link to GRIN-Global report for genus " + dt.Rows[0]["genus_link_name"].ToString() + "'>" +
                                     dt.Rows[0]["genus_name"].ToString() + "</a>";
                            if (dt.Rows[0]["subgenus_name"] != DBNull.Value)
                            {
                                lblSubGenus.Text = @"<a href='taxonomygenus.aspx?type=subgenus&id=" + dt.Rows[0]["subgenus_id"].ToString() +
                           "' title='Link to GRIN-Global report for subgenus " + dt.Rows[0]["subgenus_name"].ToString() + "'>" +
                             dt.Rows[0]["subgenus_name"].ToString() + "</a>";
                                pnlSubGenus.Visible = true;
                            }

                            if (dt.Rows[0]["section_name"] != DBNull.Value)
                            {
                                lblSect.Text = @"<a href='taxonomygenus.aspx?type=section&id=" + dt.Rows[0]["section_id"].ToString() +
                           "' title='Link to GRIN-Global report for section " + dt.Rows[0]["section_name"].ToString() + "'>" +
                             dt.Rows[0]["section_name"].ToString() + "</a>";
                                pnlSect.Visible = true;
                            }
                            lblSubSect.Text = dt.Rows[0]["subsection_name"].ToString();
                            pnlSubSect.Visible = true;
                            break;
                        case "series":
                            ViewState["series_name"] = dt.Rows[0]["series_name"];
                            _type = "series";
                            value = ViewState["genus_short_name"].ToString() + " seri. " + dt.Rows[0]["series_name"];
                            lblGenus.Text = @"<a href='taxonomygenus.aspx?id=" + dt.Rows[0]["genus_id"].ToString() +
            "' title='Link to GRIN-Global report for genus " + dt.Rows[0]["genus_link_name"].ToString() + "'>" +
              dt.Rows[0]["genus_name"].ToString() + "</a>";
                            if (dt.Rows[0]["subgenus_name"] != DBNull.Value)
                            {
                                lblSubGenus.Text = @"<a href='taxonomygenus.aspx?type=subgenus&id=" + dt.Rows[0]["subgenus_id"].ToString() +
                           "' title='Link to GRIN-Global report for subgenus " + dt.Rows[0]["subgenus_name"].ToString() + "'>" +
                             dt.Rows[0]["subgenus_name"].ToString() + "</a>";
                                pnlSubGenus.Visible = true;
                            }

                            if (dt.Rows[0]["section_name"] != DBNull.Value)
                            {
                                lblSect.Text = @"<a href='taxonomygenus.aspx?type=section&id=" + dt.Rows[0]["section_id"].ToString() +
                           "' title='Link to GRIN-Global report for section " + dt.Rows[0]["section_name"].ToString() + "'>" +
                             dt.Rows[0]["section_name"].ToString() + "</a>";
                                pnlSect.Visible = true;
                            }
                            if (dt.Rows[0]["subsection_name"] != DBNull.Value)
                            {
                                lblSubSect.Text = @"<a href='taxonomygenus.aspx?type=subsection&id=" + dt.Rows[0]["subsection_id"].ToString() +
                                                      "' title='Link to GRIN-Global report for subsection " + dt.Rows[0]["subsection_name"].ToString() + "'>" +
                                                        dt.Rows[0]["subsection_name"].ToString() + "</a>";
                                pnlSubSect.Visible = true;
                            }
                            lblSeries.Text = dt.Rows[0]["series_name"].ToString();
                            pnlSeries.Visible = true;

                            break;
                        case "subseries":
                            ViewState["subseries_name"] = dt.Rows[0]["subseries_name"];
                            _type = "subseries";
                            value = ViewState["genus_short_name"].ToString() + " subseri. " + dt.Rows[0]["subseries_name"];
                            lblGenus.Text = @"<a href='taxonomygenus.aspx?id=" + dt.Rows[0]["genus_id"].ToString() +
             "' title='Link to GRIN-Global report for genus " + dt.Rows[0]["genus_link_name"].ToString() + "'>" +
               dt.Rows[0]["genus_name"].ToString() + "</a>";
                            if (dt.Rows[0]["subgenus_name"] != DBNull.Value)
                            {
                                lblSubGenus.Text = @"<a href='taxonomygenus.aspx?type=subgenus&id=" + dt.Rows[0]["subgenus_id"].ToString() +
                           "' title='Link to GRIN-Global report for subgenus " + dt.Rows[0]["subgenus_name"].ToString() + "'>" +
                             dt.Rows[0]["subgenus_name"].ToString() + "</a>";
                                pnlSubGenus.Visible = true;
                            }

                            if (dt.Rows[0]["section_name"] != DBNull.Value)
                            {
                                lblSubGenus.Text = @"<a href='taxonomygenus.aspx?type=section&id=" + dt.Rows[0]["section_id"].ToString() +
                           "' title='Link to GRIN-Global report for section " + dt.Rows[0]["section_name"].ToString() + "'>" +
                             dt.Rows[0]["section_name"].ToString() + "</a>";
                                pnlSect.Visible = true;
                            }
                            if (dt.Rows[0]["subsection_name"] != DBNull.Value)
                            {
                                lblSubSect.Text = @"<a href='taxonomygenus.aspx?type=subsection&id=" + dt.Rows[0]["subsection_id"].ToString() +
                                                      "' title='Link to GRIN-Global report for subsection " + dt.Rows[0]["subsection_name"].ToString() + "'>" +
                                                        dt.Rows[0]["subsection_name"].ToString() + "</a>";
                                pnlSubSect.Visible = true;
                            }
                            if (dt.Rows[0]["series_name"] != DBNull.Value)
                            {
                                lblSubSect.Text = @"<a href='taxonomygenus.aspx?type=series&id=" + dt.Rows[0]["series_id"].ToString() +
                                                      "' title='Link to GRIN-Global report for series " + dt.Rows[0]["series_name"].ToString() + "'>" +
                                                        dt.Rows[0]["series_name"].ToString() + "</a>";
                                pnlSubSect.Visible = true;
                            }
                            lblSubSeries.Text = dt.Rows[0]["subseries_name"].ToString();
                            pnlSubSeries.Visible = true;

                            break;
                    }
                }
                else
                {
                    ViewState["genus_name"] = dt.Rows[0]["genus_name"];
                    _type = "genus";
                    value = ViewState["genus_short_name"].ToString();
                    lblGenus.Text = dt.Rows[0]["genus_name"].ToString();

                }

                if (dt.Rows[0]["subfamily"] == DBNull.Value)
                    dvGenus.FindControl("tr_subfamily").Visible = false;
                if (dt.Rows[0]["tribe"] == DBNull.Value)
                    dvGenus.FindControl("tr_tribe").Visible = false;
                if (dt.Rows[0]["subtribe"] == DBNull.Value)
                    dvGenus.FindControl("tr_subtribe").Visible = false;
                if (dt.Rows[0]["altfamily"].ToString() == "")
                    dvGenus.FindControl("tr_altfamily").Visible = false;
                if (dt.Rows[0]["common_name"] == DBNull.Value)
                    dvGenus.FindControl("tr_common_name").Visible = false;
                if (dt.Rows[0]["note"] == DBNull.Value)
                    dvGenus.FindControl("tr_comments").Visible = false;

                hlRecordlist.NavigateUrl = "taxonomylist.aspx?category=species&type=" + _type + "&value=" + value + "&id=" + taxonomygenusID;

            }
        }

        private void bindReferences(SecureData sd, int taxonomygenusid)
        {
            var dt = sd.GetData("web_taxonomygenus_references", ":taxonomyid=" + taxonomygenusid, 0, 0).Tables["web_taxonomygenus_references"];
            if (dt.Rows.Count > 0)
            {
                DataTable dtRef = new DataTable();
                dtRef = Utils.FormatCitations(dt);
                if (dtRef.Rows.Count > 0)
                {
                    if (dtRef.Rows.Count > 1)
                        lblReference.Text = "References: ";
                    else
                        lblReference.Text = "Reference: ";
                    lblReference.Visible = true;
                    pnlReferences.Visible = true;
                    rptReferences.DataSource = dtRef;
                    rptReferences.DataBind();
                }
            }

        }

        private void bindOtherReferences()
        {
            if (ViewState["type"] == null)
            {
                pnlMore.Visible = true;
                string gName = ViewState["genus_short_name"].ToString();
                //KMK 04/18/19 Kew Bibliographic database is no longer being maintained.
                //hlKBD.NavigateUrl = "http://www.kew.org/kbd/searchpage.do?general=" + gName;
                txtGoogle.Text = gName;
                btnGoogle.OnClientClick = "javascript:window.open(" + "\"" + "http://scholar.google.com/scholar?q=" + gName + "\"" + ")";
            }
        }

        private void bindSynonyms(SecureData sd, int taxonomygenusid)
        {
            var dt = sd.GetData("web_taxonomygenus_synonyms", ":taxonomygenusid=" + taxonomygenusid, 0, 0).Tables["web_taxonomygenus_synonyms"];
           
            string lb = "";
            if (dt.Rows.Count > 0)
            {
                string strLink;
                StringBuilder strName = new StringBuilder(); 
                pnlSynonyms.Visible = true;     
                switch (_type)
                {
                    
                    case "subgenus":
                        for(int i = 0; i<dt.Rows.Count; i++) {
                            strName.Append(dt.Rows[i]["qualifying_code"].ToString()).Append(" ");
                            if (dt.Rows[i]["hybrid_code"].ToString() == "X")
                                strName.Append("×");
                                else if (dt.Rows[i]["hybrid_code"].ToString() == "+")
                                strName.Append("+");
                            strName.Append("<i>").Append(dt.Rows[i]["genus_name"].ToString()).Append("</i>");
                            strName.Append(" ").Append(dt.Rows[i]["genus_authority"].ToString());
                            strName.Append(" subg.<i> ").Append(dt.Rows[i]["subgenus_name"].ToString()).Append("</i> ");
                            strLink = "<a href='taxonomygenus.aspx?type=subgenus&id=" + dt.Rows[i]["taxonomy_genus_id"].ToString() + "'>";
                            lb += strLink + strName.ToString() + "</a><br />";
                            strName.Clear();
                        }                    
                        break;
                    case "section":
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strName.Append(dt.Rows[i]["qualifying_code"].ToString()).Append(" ");
                            if (dt.Rows[i]["hybrid_code"].ToString() == "X")
                                strName.Append("×");
                            else if (dt.Rows[i]["hybrid_code"].ToString() == "+")
                                strName.Append("+");
                            strName.Append("<i>").Append(dt.Rows[i]["genus_name"].ToString()).Append("</i>");
                            strName.Append(" ").Append(dt.Rows[i]["genus_authority"].ToString());
                            strName.Append(" subg.<i> ").Append(dt.Rows[i]["subgenus_name"].ToString()).Append("</i> ");
                            strName.Append(" sect.<i> ").Append(dt.Rows[i]["section_name"].ToString()).Append("</i> ");
                            strLink = "<a href='taxonomygenus.aspx?type=section&id=" + dt.Rows[i]["taxonomy_genus_id"].ToString() + "'>";
                            lb += strLink + strName.ToString() + "</a><br />";
                            strName.Clear();
                        }
                        break;
                    case "subsection":
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strName.Append(dt.Rows[i]["qualifying_code"].ToString()).Append(" ");
                            if (dt.Rows[i]["hybrid_code"].ToString() == "X")
                                strName.Append("×");
                            else if (dt.Rows[i]["hybrid_code"].ToString() == "+")
                                strName.Append("+");
                            strName.Append("<i>").Append(dt.Rows[i]["genus_name"].ToString()).Append("</i>");
                            strName.Append(" ").Append(dt.Rows[i]["genus_authority"].ToString());
                            strName.Append(" subg.<i> ").Append(dt.Rows[i]["subgenus_name"].ToString()).Append("</i> ");
                            strName.Append(" sect.<i> ").Append(dt.Rows[i]["section_name"].ToString()).Append("</i> ");
                            strName.Append(" subs.<i> ").Append(dt.Rows[i]["subsection_name"].ToString()).Append("</i> ");
                            strLink = "<a href='taxonomygenus.aspx?type=subsection&id=" + dt.Rows[i]["taxonomy_genus_id"].ToString() + "'>";
                            lb += strLink + strName.ToString() + "</a><br />";
                            strName.Clear();
                        }
                        break;
                    default:
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strName.Append(dt.Rows[i]["qualifying_code"].ToString()).Append(" ");
                            if (dt.Rows[i]["hybrid_code"].ToString() == "X")
                                strName.Append("×");
                            else if (dt.Rows[i]["hybrid_code"].ToString() == "+")
                                strName.Append("+");
                            strName.Append("<i>").Append(dt.Rows[i]["genus_name"].ToString()).Append("</i>");
                            strName.Append(" ").Append(dt.Rows[i]["genus_authority"].ToString());
                            strLink = "<a href='taxonomygenus.aspx?id=" + dt.Rows[i]["taxonomy_genus_id"].ToString() + "'>";
                            lb += strLink + strName.ToString() + "</a><br />";
                            strName.Clear();
                        }
                        break;
                }
                lblSynonyms.Text = lb;
              
            }
        }
        #region more junk
        //KMK 06/07/2018 - This code is SO unnecessary for what it's trying to do.
        //private void bindCheckOther(SecureData sd, int taxonomygenusID) //temp query now
        //{
        //    var dt = sd.GetData("web_taxonomygenus_checkother", ":taxonomygenusid=" + taxonomygenusID, 0, 0).Tables["web_taxonomygenus_checkother"];
        //    if (dt.Rows.Count > 0)
        //    {
        //        //pnlCheckOther.Visible = true;
        //        rptCheckOther.DataSource = dt;
        //        rptCheckOther.DataBind();
        //    }
        //}
        #endregion
        private void bindSubdivisions(SecureData sd, int taxonomygenusID)
        {
            string name = ViewState["genus_name"].ToString();
            #region crapcode
            //using (DataManager dm = sd.BeginProcessing(true, true))
            //{

            //This code is SO unnecessary as we already know what the column type is from the QueryString.
            //KMK 06/06/2018
            //DataTable dt1 = dm.Read(@"
            //select genus_name, subgenus_name, section_name, subsection_name, series_name, subseries_name 
            //from taxonomy_genus 
            //where taxonomy_genus_id = :taxonomyid",
            //new DataParameters(":taxonomyid", taxonomygenusID));

            //if (dt1.Rows.Count > 0)
            //{
            //    string genus_name = dt1.Rows[0][0].ToString().Trim();
            //    string subgenus_name = dt1.Rows[0][1].ToString().Trim();
            //    string section_name = dt1.Rows[0][2].ToString().Trim();
            //    string subsection_name = dt1.Rows[0][3].ToString().Trim();
            //    string series_name = dt1.Rows[0][4].ToString().Trim();
            //    string subseries_name = dt1.Rows[0][5].ToString().Trim();


            //    if (!String.IsNullOrEmpty(subseries_name))
            //    {
            //        columntype = "subseries_name"; 
            //    }
            //    else if (!String.IsNullOrEmpty(series_name))
            //    {
            //        columntype = "series_name"; 
            //    }
            //    else if (!String.IsNullOrEmpty(subsection_name))
            //    {
            //        columntype = "subsection_name"; 
            //    }
            //    else if (!String.IsNullOrEmpty(section_name))
            //    {
            //        columntype = "section_name"; 
            //    }
            //    else if (!String.IsNullOrEmpty(subgenus_name))
            //    {
            //        columntype = "subgenus_name"; 
            //    }
            //    else
            //        columntype = "genus_name"; 

            //}

            //switch (_type)
            //    {
            //        case "genus":
            //            columntype = "genus_name";
            //            break;
            //        case "subgenus":
            //            columntype = "subgenus_name";
            //            break;
            //        case "section":
            //            columntype = "section_name";
            //            break;
            //        case "subsection":
            //            columntype = "subsection_name";
            //            break;
            //        case "series":
            //            columntype = "series_name";
            //            break;
            //        case "subseries":
            //            columntype = "subseries_name";
            //           break;
            //    }
            #endregion
            DataTable dt = null;
            dt = sd.GetData(
                "web_taxonomygenus_subdivisions",
                ":columntype=" + _type + "_name" +
                ";:taxonomygenusid=" + taxonomygenusID
                , 0, 0).Tables["web_taxonomygenus_subdivisions"];

            //KMK 06/06/2018 Had to change the DV to remove taxonomy_genus_id <> to the one being passed
            //because it was dropping a subdivision if it was passing a subdivision.  
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Subgenus"].ToString() == "" && dt.Rows[0]["Subgenus"].ToString() == ""
                   && dt.Rows[0]["Subgenus"].ToString() == "" && dt.Rows[0]["Subgenus"].ToString() == ""
                   && dt.Rows[0]["Subgenus"].ToString() == "")

                    dt.Rows[0].Delete();
                dt.AcceptChanges();
            }
            bool sg = false;
            bool sec = false;
            bool ssec = false;
            bool ser = false;
            bool sser = false;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow r in dt.Rows)
                {
                    //KMK only show subdivisions if there is information (later check for subdivisions below current one)
                    if (r["Subgenus"].ToString() != "")
                        sg = true;
                    if (r["Section"].ToString() != "")
                        sec = true;
                    if (r["Subsection"].ToString() != "")
                        ssec = true;
                    if (r["Series"].ToString() != "")
                        ser = true;
                    if (r["Subseries"].ToString() != "")
                        sser = true;
                }

                if (ViewState["subgenus_name"] != null)
                {
                    name += " subg. <i>" + ViewState["subgenus_name"].ToString() + "</i>";
                    sg = false;
                }
                if (ViewState["section_name"] != null)
                {
                    name += " sect. <i>" + ViewState["section_name"].ToString() + "</i>";
                    sg = sec = false;
                }
                if (ViewState["subsection_name"] != null)
                {
                    name += " subsect. <i>" + ViewState["subsection_name"].ToString() + "</i>";
                    sg = sec = ssec = false;
                }
                if (ViewState["series_name"] != null)
                {
                    name += " ser. <i>" + ViewState["series_name"].ToString() + "</i>";
                    sg = sec = ssec = ser = false;
                }
                if (ViewState["subseries_name"] != null)
                    name += " subser. <i>" + ViewState["subseries_name"].ToString() + "</i>";

                //KMK 3/20/18  Can return a datatable with 1 row of "NULL", so if 1 row and everything is NOT null, then make visible
                if (!(dt.Rows.Count == 1 && dt.Rows[0]["Subgenus"].ToString() == "" && dt.Rows[0]["Section"].ToString() == ""
                    && dt.Rows[0]["Subsection"].ToString() == "" && dt.Rows[0]["Series"].ToString() == "" && dt.Rows[0]["Subseries"].ToString() == ""))
                {
                    //get name for label
                    lblSub.Text = "Subdivisions of " + name + ":";
                    if (sg)
                    {
                        gvSubdivisions.Columns[0].Visible = true;
                        pnlSubdivisons.Visible = true;
                    }
                    if (sec)
                    {
                        gvSubdivisions.Columns[1].Visible = true;
                        pnlSubdivisons.Visible = true;
                    }
                    if (ssec)
                    {
                        gvSubdivisions.Columns[2].Visible = true;
                        pnlSubdivisons.Visible = true;
                    }
                    if (ser)
                    {
                        gvSubdivisions.Columns[3].Visible = true;
                        pnlSubdivisons.Visible = true;
                    }
                    if (sser)
                    {
                        gvSubdivisions.Columns[4].Visible = true;
                        pnlSubdivisons.Visible = true;
                    }
                    gvSubdivisions.DataSource = dt;
                    gvSubdivisions.DataBind();
                }
            }
        }
        protected void bindCitation()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string cite = "Cite as: USDA, Agricultural Research Service, National Plant Germplasm System. ";
            cite += DateTime.Today.Year + ". ";
            cite += "Germplasm Resources Information Network (GRIN-Taxonomy).<br /> National Germplasm Resources Laboratory, Beltsville, Maryland. URL: ";
            cite += url + ". Accessed ";
            cite += DateTime.Today.ToString("d MMMM yyyy") + ".";
            lblCite.Text = cite;
            pnlcite.Visible = true;
            string genusname = ViewState["genus_short_name"].ToString();

            //KMK 06/07/2018  Added making link for checking ING database
            lblING.Text = @"<a href='http://botany.si.edu/ing/INGsearch.cfm?searchword=" + genusname + "' target='blank'>" +
                "Check Index Nominum Genericorum for " + genusname + "</a>";

        }
        private void bindImages(SecureData sd, int taxonomygenusID) //temp query now
        {
            var dt = sd.GetData("web_taxonomygenus_images", ":taxonomygenusid=" + taxonomygenusID, 0, 0).Tables["web_taxonomygenus_images"];
            if (dt.Rows.Count > 0)
            {
                pnlImages.Visible = true;
                rptImages.DataSource = dt;
                rptImages.DataBind();
            }
        }



        protected string getNameTitle()
        {
            return _type;
        }

        protected string DisplayNote(object note)
        {
            if (!String.IsNullOrEmpty(note.ToString()))
            {
                string note1 = note as string;

                //int i = note1.IndexOf("\\;");
                note1 = System.Text.RegularExpressions.Regex.Split(note1, "\\;")[0];
                if (note1.Length > 1)
                {
                    if (note1.Substring(note1.Length - 1, 1) == "\\")
                        note1 = note1.Substring(0, note1.Length - 1);
                }

                return note1;

            }
            return "";
        }
        protected string DisplayComment(object note)
        {
            if (!String.IsNullOrEmpty(note.ToString()))
            {
                string note1 = note as string;

                string[] comments = Regex.Split(note1, @"\\\;");

                if (comments.Length > 1)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    foreach (string comment in comments)
                    {
                        sb.Append("<li>&#149 ").Append(comment).Append("</li>");
                    }
                    sb.Append("</ul>");
                    return sb.ToString();
                }
                else
                    return note1;
            }
            return "";
        }

    }
}
