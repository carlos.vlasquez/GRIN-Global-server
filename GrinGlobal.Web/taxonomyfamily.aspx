﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site1.Master" CodeBehind="taxonomyfamily.aspx.cs" Inherits="GrinGlobal.Web.TaxonomyFamily" %>

<%@ Import Namespace="GrinGlobal.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <asp:Panel ID="pnlFamily" runat="server">
        <h2><a href='taxon/abouttaxonomy.aspx?chapter=scient' target='_blank'>Family</a>&nbsp;
       <asp:Label ID="lblFamily" runat="server"></asp:Label>
        <asp:HiddenField ID="hdfamilyid" runat="server"  />
                    </h2>
        <div style="float: right;">
            <asp:Button ID="btnNewFamily" runat="server" Text="New Family Search" OnClick="btnNewFamily_Click" /></div>

    </asp:Panel>
    <asp:Panel ID="pnlSubFamily" runat="server" Visible="false">
        <h2>subfamily <i>
            <asp:Label ID="lblSubfamily" runat="server" Text=""></asp:Label></i> <%--<i><%# Eval("subfamily") %></i>--%></h2>
    </asp:Panel>
    <asp:Panel ID="pnlTribe" runat="server" Visible="false">
        <h2>tribe  <i><asp:Label ID="lblTribe" runat="server" Text=""></asp:Label></i> <%--<i><%# Eval("tribe") %></i>--%></h2>
    </asp:Panel>
    <asp:Panel ID="pnlSubTribe" runat="server" Visible="false">
        <h2>subtribe <i> <asp:Label ID="lblSubtribe" runat="server" Text=""></asp:Label></i><%--<i><%# Eval("subtribe") %></i>--%></h2>
    </asp:Panel>


    <asp:DetailsView ID="dvFamily" runat="server" AutoGenerateRows="false" DefaultMode="ReadOnly" CssClass='detail' GridLines="None" Width="100%">
        <FieldHeaderStyle CssClass="" />

        <HeaderTemplate>
            <%#Eval("synonym_for_family")%>
            <%-- <asp:Panel ID="pnlSubFamily1" runat="server" Visible="false">
        <h2>subfamily <i><%# Eval("subfamily") %></i></h2></asp:Panel>
        <asp:Panel ID="pnlTribe1" runat="server" Visible="false">
        <h2>tribe <i><%# Eval("tribe") %></i></h2></asp:Panel>
         <asp:Panel ID="pnlSubTribe1" runat="server" Visible="false">
        <h2>subtribe <i><%# Eval("subtribe") %></i></h2></asp:Panel>--%>
        </HeaderTemplate>
        <EmptyDataTemplate>
            No family data found
        </EmptyDataTemplate>
        <Fields>
            <asp:TemplateField>
                <ItemTemplate>
                    <table id="Table1" runat="server" cellpadding='1' cellspacing='1' border='0' class='grid horiz2' style='width: 600px; border: 1px solid black'>
                        <tr>
                            <th>Family number:</th>
                            <td><%# Eval("family_number") %> &nbsp;&nbsp;
                <b>Last Updated:</b> <%# Toolkit.Coalesce(Eval("modified_date", "{0:dd-MMM-yyyy}"), Eval("created_date", "{0:dd-MMM-yyyy}")) %></td>
                        </tr>
                        <tr id="tr_alternatename">
                            <th>Alternate name:</th>
                            <td><%# Eval("altfamily") %></td>
                        </tr>
                        <tr>
                            <th>Number of accepted genera:</th>
                            <td><%# Eval("genera_count")%></td>
                        </tr>
                        <tr id="tr_typegenus">
                            <th>Type genus:</th>
                            <td><%# Eval("genus_type")%></td>
                        </tr>
                        <tr id="tr_comments">
                            <th>Comments:</th>
                            <td><%# DisplayComment(Eval("note")) %> </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>

    <br />
   
        <b>
        <asp:HyperLink ID="hlRecordlist" runat="server"> <b>Complete list of genera</b></asp:HyperLink>
    </b>
   
    <br />

    <!--
<asp:Panel ID="pnlCheckOther" runat="server" Visible="false">
<asp:Repeater ID="rptCheckOther" runat="server">
    <HeaderTemplate>
        <h1><%= Page.DisplayText("htmlCheck", "Check other databases for ")%> <i><%# getName() %></i>:</h1>
        <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li><%# Eval("otherDBlink") %>: &nbsp <%# Eval("otherDB") %> </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>
</asp:Panel>
-->

 <asp:Panel ID="pnlReferences" runat="server" Visible="false">
         <a name="References"></a>
        <h1><a href='taxon/abouttaxonomy.aspx?chapter=liter' target='_blank'>
            <asp:Label ID="lblReference" runat="server" Visible="false"></asp:Label></a></h1>
           <asp:Repeater ID="rptReferences" runat="server">
           <HeaderTemplate>
            <ol>
        </HeaderTemplate>
        <ItemTemplate>
            <li><%# Eval("reference") %></li>
        </ItemTemplate>
        <FooterTemplate>
            </ol>
        </FooterTemplate>
        </asp:Repeater>
</asp:Panel>

     <asp:Panel ID="pnlMore" runat="server" Visible="false">
         <%--<li>    KMK 04/18/19 Kew Bibliographic database is no longer being maintained.
                <asp:HyperLink ID="hlKBD" runat="server" Target="_blank"> 
		<b>KBD:</b></asp:HyperLink> Kew Bibliographic Databases of Royal
			Botanic Gardens, Kew 
                <br />
                <font size="-2">Note: Log on to KBD for better access.
                </font></li>--%>
         <br />   <a href="http://scholar.google.com/">
                <img src="images/Google_Scholar_logo_2015.png"
                    alt="Google Scholar" width="105" 
                    border="0" /></a>&nbsp; <font size="-2">
                        <asp:TextBox ID="txtGoogle" runat="server"></asp:TextBox>
                        &nbsp;<asp:Button ID="btnGoogle" runat="server" Text="Search" />
                    </font>
            
    </asp:Panel>

    <asp:Panel ID="pnlSynonyms" runat="server" Visible="false">
        <asp:Repeater ID="rptSynonyms" runat="server">
            <HeaderTemplate>
                <h1>Synonyms for family:</h1>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <li><a href='taxonomyfamily.aspx?id=<%# Eval("taxonomy_family_id") %>'><i><%# Eval("family_name")%></i></a> <%# Eval("author_name") %></li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>

    <asp:Panel ID="pnlSubdivisons" runat="server" Visible="false">
        <h1>Subfamilies and tribes for <%# getName() %>:</h1>
        <asp:GridView ID="gvSubdivisions" runat="server" GridLines="None" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Subfamily" visible="false">
                    <ItemTemplate><i><%# Eval("Subfamily")%></i><span style="padding-right:25px"></span></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Tribe" visible="false">
                    <ItemTemplate><i><%# Eval("Tribe")%></i><span style="padding-right:25px"></span></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Subtribe" visible="false">
                    <ItemTemplate><i><%# Eval("SubTribe")%></i><span style="padding-right:25px"></span></ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>

    <asp:Panel ID="pnlImages" runat="server" Visible="false">
        <asp:Repeater ID="rptImages" runat="server">
            <HeaderTemplate>
                <h1><%= Page.DisplayText("htmlImages", "Images:")%></h1>
                <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <li><b><%# Eval("title") %>: </b><%# DisplayNote(Eval("note")) %></li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <hr />
    <asp:Panel id="pnlcite" runat="server" Visible="false">
        <asp:Label ID="lblCite" runat="server"></asp:Label>
    </asp:Panel>
    <br />
</asp:Content>
