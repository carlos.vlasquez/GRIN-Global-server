﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site1.Master" CodeBehind="taxonomygenus.aspx.cs" Inherits="GrinGlobal.Web.TaxonomyGenus" %>
<%@ Import Namespace="GrinGlobal.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
   
        <h2><a href='taxon/abouttaxonomy.aspx?chapter=scient' target='_blank'>Genus:</a>
         <asp:Label ID="lblGenus" runat="server"></asp:Label></h2>
        <asp:Panel ID="pnlSubGenus" runat="server" Visible="false">
        <h2>subg. <i><asp:Label ID="lblSubGenus" runat="server"></asp:Label></i></h2></asp:Panel>
        <asp:Panel ID="pnlSect" runat="server" Visible="false">
        <h2>sect. <i><asp:Label ID="lblSect" runat="server"></asp:Label></i></h2></asp:Panel>
        <asp:Panel ID="pnlSubSect" runat="server" Visible="false">
        <h2>subsect. <i><asp:Label ID="lblSubSect" runat="server"></asp:Label></i></h2></asp:Panel>
         <asp:Panel ID="pnlSeries" runat="server" Visible="false">
        <h2>ser. <i><asp:Label ID="lblSeries" runat="server"></asp:Label></i></h2></asp:Panel>
        <asp:Panel ID="pnlSubSeries" runat="server" Visible="false">
        <h2>subser. <i><asp:Label ID="lblSubSeries" runat="server"></asp:Label></i></h2></asp:Panel>
        <h2><asp:Label ID="lblSynonym" runat="server"></asp:Label></h2>
      <asp:DetailsView ID="dvGenus" runat="server" AutoGenerateRows="false" DefaultMode="ReadOnly" CssClass='detail' GridLines="None">

    <EmptyDataTemplate>
        No genus data found
    </EmptyDataTemplate>
    <Fields>
        <asp:TemplateField>
            <ItemTemplate>
            <table runat="server" cellpadding='1' cellspacing='1' border='0' class='grid horiz' style='width:505px; border:1px solid black'>
            <tr>
                <th>Family:</th>
                <td><i><%# Eval("family_name") %></i></td>
            </tr>
            <tr id="tr_subfamily">
                <th>Subfamily:</th>
                <td><i><%# Eval("subfamily") %></i></td>
            </tr>
            <tr id="tr_tribe">
                <th>Tribe:</th>
                <td><i><%# Eval("tribe") %></i></td>
            </tr>
            <tr id="tr_subtribe">
                <th>Subtribe:</th>
                <td><i><%# Eval("subtribe") %> </i></td>
            </tr>
            <tr id="tr_altfamily">
                <th>Alternate family(ies):</th>
                <td><i><%# Eval("altfamily") %></i></td>
            </tr>
            <tr id="tr_common_name">
                <th>Common names:</th>
                <td><%# Eval("common_name") %></td>
            </tr>
            <tr>
                <th>Genus number:</th>
                <td><%# Eval("genus_number") %></td>
            </tr>
            <tr>
                <th>Last updated:</th>
                <td><%# Toolkit.Coalesce(Eval("modified_date", "{0:dd-MMM-yyyy}"), Eval("created_date", "{0:dd-MMM-yyyy}")) %></td>
            </tr>
            <tr  id="tr_count">
                <th>Accession Count:</th>
                <td><a href='view2.aspx?dv=web_taxonomygenus_view_accessionlist&params=:taxonomygenusid=<%# Eval("genus_number")%>&hdv=web_taxonomygenus_header' title="Active: Living material present in NPGS; Available: Can be ordered."><%# Eval("accession_count") %> (<%# Eval("active_count") %> active, <%# Eval("available_count") %> available)</a> in National Plant Germplasm System</td>
            </tr>
            <tr  id="tr_comments">
                <th>Comments:</th>
                <td><%# DisplayComment(Eval("note")) %> </td>
            </tr>
        </table>
            </ItemTemplate>
        </asp:TemplateField>
    </Fields>
</asp:DetailsView>

<br/>
<b>
    <asp:HyperLink ID="hlRecordlist" runat="server"> <b>List of Species Records in GRIN</b></asp:HyperLink>
</b>
<br />
<br />
 <asp:Panel ID="pnlReferences" runat="server" Visible="false">
        <a name="References"></a>
        <h1><a href='taxon/abouttaxonomy.aspx?chapter=liter' target='_blank'>
            <asp:Label ID="lblReference" runat="server" Visible="false"></asp:Label></a></h1>
        <asp:Repeater ID="rptReferences" runat="server">
           <HeaderTemplate>
            <ol>
        </HeaderTemplate>
        <ItemTemplate>
            <li><%# Eval("reference") %></li>
        </ItemTemplate>
        <FooterTemplate>
            </ol>
        </FooterTemplate>
        </asp:Repeater>
 </asp:Panel>


 
<asp:Panel ID="pnlMore" runat="server" Visible="false">

<%--<li> KMK 04/18/19 Kew Bibliographic database is no longer being maintained.
    <asp:HyperLink ID="hlKBD" runat="server" Target="_blank"> 
		<b>KBD:</b></asp:HyperLink> Kew Bibliographic Databases of Royal
			Botanic Gardens, Kew 
	<br />Note: Log on to KBD for better access.
		</li> --%><br />
<a href="https://scholar.google.com/">
        <img src="images/Google_Scholar_logo_2015.png"
                alt="Google Scholar" width="105" 
                border="0"/></a>&nbsp; <font size="-2">
                        <asp:TextBox ID="txtGoogle" runat="server" ></asp:TextBox>
                        &nbsp;<asp:Button ID="btnGoogle" runat="server" Text="Search" />
		</font>

</asp:Panel>

<asp:Panel ID="pnlSynonyms" runat="server" Visible="false">
  
        <h1>Synonyms (=), probable synonyms (=~), and possible synonyms (~):</h1>
        <ul>   
        <li><asp:Label ID="lblSynonyms" runat="server"></asp:Label></li>  
        </ul>    
</asp:Panel> 


<asp:Panel ID="pnlSubdivisons" runat="server" Visible="false">
<h1><asp:Label ID="lblSub" runat="server"></asp:Label></h1>
    <asp:GridView ID="gvSubdivisions" runat="server" GridLines="None" AutoGenerateColumns="False">
    <Columns>
    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Subgenus" visible="false"><ItemTemplate><i><%# Eval("Subgenus")%></i><span style="padding-right:25px"></span></ItemTemplate></asp:TemplateField> 
    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Section" visible="false"><ItemTemplate><i><%# Eval("Section")%></i><span style="padding-right:25px"></ItemTemplate></asp:TemplateField> 
    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Subsection" visible="false"><ItemTemplate><i><%# Eval("Subsection")%></i><span style="padding-right:25px"></span></ItemTemplate></asp:TemplateField> 
    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Series" visible="false"><ItemTemplate><i><%# Eval("Series")%></i><span style="padding-right:25px"></span></ItemTemplate></asp:TemplateField> 
    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Subseries" visible="false"><ItemTemplate><i><%# Eval("Subseries")%></i><span style="padding-right:25px"></span></ItemTemplate></asp:TemplateField> 
    </Columns>
     </asp:GridView></asp:Panel>

<asp:Panel ID="pnlImages" runat="server" Visible="false">
<asp:Repeater ID="rptImages" runat="server">
    <HeaderTemplate>
        <h1><%= Page.DisplayText("htmlImages", "Images:")%></h1>
        <ul>
    </HeaderTemplate>
    <ItemTemplate>
        <li><b><%# Eval("title") %>: </b><%# DisplayNote(Eval("note")) %></li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    </FooterTemplate>
</asp:Repeater>  
</asp:Panel>
<br />
<asp:Label ID="lblING" runat="server"></asp:Label>

<hr />
    
<asp:Panel id="pnlcite" runat="server" Visible="false">
        <asp:Label ID="lblCite" runat="server"></asp:Label>
    </asp:Panel>
    <br />
</asp:Content>