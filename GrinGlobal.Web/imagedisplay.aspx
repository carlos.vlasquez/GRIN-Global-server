﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="imagedisplay.aspx.cs" Inherits="GrinGlobal.Web.ImagaDisplay" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Display Image</title>
    <link runat='server' id='lnkTheme' href="styles/default.css" rel='stylesheet' />

    <%--KMK 06/07/2018  Added the links below to fix image orientation--%>
    <%--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.1.0/exif.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-load-image/2.12.2/load-image.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-load-image/2.12.2/load-image-scale.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/blueimp-load-image/2.12.2/load-image-orientation.min.js"></script>--%>
<script type="text/javascript" src="scripts/exif.min.js"></script>
    <script type="text/javascript"
            src="scripts/load-image.min.js"></script>
    <script type="text/javascript"
            src="scripts/load-image-scale.min.js"></script>
    <script type="text/javascript"
            src="scripts/load-image-orientation.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:Label ID="lblInformation" runat="server" Text="The Image requested can't be found." Visible="false" Font-Bold="True"></asp:Label>
    <%-- KMK So original image does not show --%>
        <img id="image1" runat="server"  alt="Image" class="displayImage" style="width:0px;height:0px" visible="false"/>
    </div>
         <%-- KMK New div to show image --%>
    <div id="container">
    </div>
    </form>
    <%-- KMK Added script to show image --%>
    <script type="text/javascript">
    
    window.loadImage(<%=_src%>, function (img) {
        if (img.type === "error") {
            console.log("couldn't load image:", img);
        } else {
            window.EXIF.getData(img, function () {
                console.log("done!");
                var orientation = window.EXIF.getTag(this, "Orientation");
                var canvas = window.loadImage.scale(img, { orientation: orientation || 0, canvas: true, maxWidth: 700 });
                document.getElementById("container").appendChild(canvas);
               
                });
        }
    });
</script>
</body>
</html>
