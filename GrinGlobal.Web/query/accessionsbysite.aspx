﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="accessionsbysite.aspx.cs" Inherits="GrinGlobal.Web.query.accesssionsbysite" MasterPageFile="~/Site1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Sites and their Accessions Numbers</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
<center>

<h3><asp:Label ID="lbl" runat="server" Text="Accessions by Site" /></h3>
<asp:GridView ID="gvAccession" runat="server" 
        AutoGenerateColumns="False" CssClass="grid" ShowFooter="True" FooterStyle-BackColor="Silver">
<EmptyDataTemplate>
    No data found
</EmptyDataTemplate>
<Columns>
   <asp:BoundField DataField="sitename" HeaderText="Site"  FooterText = "Total" />
     <asp:BoundField DataField="acount" HeaderText="count"   />
     <asp:TemplateField HeaderText="Percent" FooterText = "100">
    <ItemTemplate>
    <asp:Label ID="Label1" runat="server" 
      Text='<%# CalculatePercent(Eval("acount"), Eval("atotal")) %>'></asp:Label>
  </ItemTemplate>
 </asp:TemplateField>
</Columns>
</asp:GridView>
<br />
<hr />
<sup>1</sup> The count represents accessions held only at the NLGRP. In addition the NLGRP serves as the primary backup repository for the National Plant Germplasm System. Over 400,000 accessions are backed up in long term storage at the NLGRP.
 <hr />
 </center>

</asp:Content>