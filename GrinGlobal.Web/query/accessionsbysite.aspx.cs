﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using System.Data;
using GrinGlobal.Business;


namespace GrinGlobal.Web.query
{
    public partial class accesssionsbysite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (var sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                DataTable dt = sd.GetData("web_summary_accessions_bysite", "", 0, 0).Tables["web_summary_accessions_bysite"];

                gvAccession.Columns[1].FooterText = dt.Rows[0][2].ToString(); 
                gvAccession.DataSource = dt;
                gvAccession.DataBind();
                gvAccession.ShowFooter = true;

                addNote();
            }
        }

        //protected void gvAccession_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.DataItemIndex > -1) {

        //        var acount = Toolkit.ToInt32(((DataRowView)e.Row.DataItem)["acount"], -1)/(DataRowView)e.Row.DataItem)["acount"];
        //}

        public string CalculatePercent(object val1, object val2)
        {
            var per = 100.00 * Toolkit.ToInt32(val1.ToString()) / Toolkit.ToInt32(val2.ToString()) ;
            string percent = "";
            if (per < 1)
            {
                percent = "< 1";
            }
            else
                percent = System.Math.Round(per, 0, MidpointRounding.AwayFromZero).ToString();


            return percent;
        }

        private void addNote()
        {
            foreach(GridViewRow gridRow in gvAccession.Rows)
            {
                //for (int i = 0; i < gvAccession.Columns.Count; i++)
                //{
                    if (gridRow.Cells[0].Text.Equals("National Laboratory for Genetic Resources Preservation"))  // need code improvement here
                    {
                        gridRow.Cells[0].Text = gridRow.Cells[0].Text + "<sup> 1 </sup>";
                    }
                //}
            }
        }

    }
    
}
