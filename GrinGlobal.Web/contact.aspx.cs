﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using System.Text;
using GrinGlobal.Business;
using GrinGlobal.Core;
using System.Data;

namespace GrinGlobal.Web
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                btnSendFeedback.Text = Site1.DisplayText("btnSendFeedback", "Send Request"); 
                bindSubject();
            }
            
        }

        protected void btnSendFeedback_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            string CRLF = "\r\n";

            sb.Append("At " + DateTime.Now + " request was sent from the Contact Us Page" + CRLF + CRLF);
            sb.Append("Name:  " + this.txtName.Text + CRLF + CRLF);
            sb.Append("Email Address: " + this.txtEmail.Text + CRLF + CRLF);
            sb.Append("Subject: " + this.ddlSubject.SelectedItem.Text.Trim() + CRLF + CRLF);
            sb.Append("Message:  " + this.txtMessage.Text.Trim());

            string appendSubject = Toolkit.GetSetting("AppendContactSubject", "").ToLower();
            if (appendSubject == "name")
                appendSubject = " (From " + this.txtName.Text + ")";
            else if (appendSubject == "date")
                appendSubject = " (" + DateTime.Now.ToString("yyyyMMddHHmmssf") + ")";

            try
            {
                EmailQueue.SendEmail(Toolkit.GetSetting("EmailHelpTo", ""),
                            Toolkit.GetSetting("EmailFrom", ""),
                            this.txtEmail.Text.Trim(),
                            "",
                            "GRIN-GLOBAL - " + ddlSubject.SelectedItem.Text.Trim() + appendSubject ,
                            sb.ToString());
            }
            catch (Exception ex)
            {
                // debug, nothing we can/need to do if mail failed to send.
                string s = ex.Message;
                Logger.LogTextForcefully("Application error: Sending email failed for contact us from " + this.txtEmail.Text + ". ", ex);
            }
            finally
            {
            }

            this.pnlSendEmail.Visible = false;
            this.pnlMailSent.Visible = true;
        }

        private void bindSubject()
        {
            DataTable dt = Utils.GetCodeValue("CONTACT_SUBJECT", "first");

            if (dt.Rows.Count > 1)
            {
                //dt.Rows[0].Delete();
                //dt.AcceptChanges(); 

                ddlSubject.DataValueField = "Value";
                ddlSubject.DataTextField = "Text";
                ddlSubject.DataSource = dt;
                ddlSubject.DataBind();

                ListItem itemToRemove = ddlSubject.Items.FindByValue("first");
                if (itemToRemove != null)
                {
                    ddlSubject.Items.Remove(itemToRemove);
                }
            }
        }
    }
}
