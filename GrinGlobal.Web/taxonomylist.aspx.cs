﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Data;
using GrinGlobal.Web.taxon;

namespace GrinGlobal.Web
{
    public partial class TaxonomyList : System.Web.UI.Page
    {
       //Page for further expansion, list of names, subitems under lists of species, genera...
        string _category;
        string _type;
        string _value;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
               if (this.Request.QueryString["category"] != null)
                    _category = HttpUtility.HtmlEncode(this.Request.QueryString["category"].ToString());

                if (this.Request.QueryString["type"] != null)
                    _type = HttpUtility.HtmlEncode(this.Request.QueryString["type"].ToString());

                if (this.Request.QueryString["value"] != null)
                    _value = HttpUtility.HtmlEncode(this.Request.QueryString["value"].ToString());

                

                bindData(Toolkit.ToInt32(Request.QueryString["id"], 0));
            }
        }

        private void bindData(int id)
        {       
            DataTable dt = null;
            DataTable dtName = new DataTable();
            string columntype = "";

            if ( _type != "")
            {
                using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
                {
                    if (_category == "species")
                    {
                       lblTitle.Text = "Species of <i>" + taxon.TaxonUtil.ItalicTaxon(_value) + "</i>";

                        switch (_type)
                        {
                            case "genus":
                                columntype = "genus_name";
                                break;
                            case "subgenus":
                                columntype = "subgenus_name";
                                break;
                            case "section":
                                columntype = "section_name";
                                break;
                            case "subsection":
                                columntype = "subsection_name";
                                break;
                            case "series":
                                columntype = "series_name";
                                break;
                            case "subseries":
                                columntype = "subseries_name";
                               break;
                        }

                        dt = sd.GetData(
                            "web_taxonomygenus_view_specieslist",
                            ":columntype=" + columntype +
                            ";:taxonomygenusid=" + id
                            , 0, 0).Tables["web_taxonomygenus_view_specieslist"];
                    }
                    else if(_category == "genera")
                    {

                        switch (_type)
                        {
                            case "family":
                                columntype = "family_name";
                                break;
                            case "subfamily":
                                columntype = "subfamily_name";
                                break;
                            case "tribe":
                                columntype = "tribe_name";
                                break;
                            case "subtribe":
                                columntype = "subtribe_name";
                                break;
                        }

                        dt = sd.GetData(
                            "web_taxonomyfamily_view_generalist",
                            ":columntype=" + columntype +
                            ";:taxonomyfamilyid=" + id
                            , 0, 0).Tables["web_taxonomyfamily_view_generalist"];
                        if (dt.Rows.Count > 0)
                            lblTitle.Text = "Genera and generic subdivisions of " + dt.Rows[0]["family_name"].ToString();
                        else
                            lblTitle.Text = "GRIN does not accept any genera in this family.";
                    }
         
                }
            }
            rptRecordlist.DataSource = dt;
            rptRecordlist.DataBind();
            //if (dt.Rows.Count > 0)
            //{
            //    dt.Columns["name"].ReadOnly = false;
            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        dr["name"] = TaxonUtil.ItalicTaxon(dr["name"].ToString());
            //    }
            //    rptRecordlist.DataSource = dt;
            //    rptRecordlist.DataBind();
            //}
        }
    }
}
