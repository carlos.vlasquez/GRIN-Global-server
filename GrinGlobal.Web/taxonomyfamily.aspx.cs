﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Data;
using System.Text.RegularExpressions;
using System.Text;

namespace GrinGlobal.Web
{
    public partial class TaxonomyFamily : System.Web.UI.Page
    {
        string _type;


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (this.Request.QueryString["type"] != null)
                    ViewState["type"] = this.Request.QueryString["type"].ToString();
                bindData(Toolkit.ToInt32(Request.QueryString["id"], 0));

            }
        }
        private void bindData(int taxonomyfamilyID)
        {
            using (SecureData sd = new SecureData(false, UserManager.GetLoginToken()))
            {
                bindTaxonomy(sd, taxonomyfamilyID);
                bindReferences(sd, taxonomyfamilyID);
                bindOtherReferences();
                if (_type == "family")
                {
                    bindCheckOther(sd, taxonomyfamilyID);
                    bindSynonyms(sd, taxonomyfamilyID);
                }
                bindSubdivisions(sd, taxonomyfamilyID);
                bindImages(sd, taxonomyfamilyID);
                bindCitation();
            }
        }

        private void bindTaxonomy(SecureData sd, int taxonomyfamilyID)
        {
            string value = "";
            var dt = sd.GetData("web_taxonomyfamily_summary", ":taxonomyfamilyid=" + taxonomyfamilyID, 0, 0).Tables["web_taxonomyfamily_summary"];
            dvFamily.DataSource = dt;
            dvFamily.DataBind();
            if (ViewState["type"] != null)
            {
                _type = ViewState["type"].ToString();

                string strfamily = "";
                string strsubfam, strtribe;
                //if (ViewState["type"] != null)
                //{
                switch (ViewState["type"].ToString())
                {
                    case "subfamily":
                        ViewState["name"] = dt.Rows[0]["subfamily"];
                        //   _type = "subfamily";
                        value = dt.Rows[0]["family_short_name"] + " subfam. " + dt.Rows[0]["subfamily"];
                        strfamily = "<a href=taxonomyfamily.aspx?id=" + dt.Rows[0]["family_id"].ToString();
                        strfamily += " title='Link to GRIN report for family " + dt.Rows[0]["family_short_name"].ToString() + "' >";
                        strfamily += dt.Rows[0]["family_name"].ToString() + "</a>";
                        lblFamily.Text = strfamily;
                        ViewState["fullname"] = strfamily;
                        lblSubfamily.Text = dt.Rows[0]["subfamily"].ToString();
                        Page.DataBind(); //do bind before setting visible property
                        pnlSubFamily.Visible = true;
                        break;
                    case "tribe":
                        ViewState["name"] = dt.Rows[0]["tribe"];
                        //     _type = "tribe";
                        value = dt.Rows[0]["family_short_name"] + " tribe " + dt.Rows[0]["tribe"];
                        strfamily = "<a href=taxonomyfamily.aspx?id=" + dt.Rows[0]["family_id"].ToString();
                        strfamily += " title='Link to GRIN report for family " + dt.Rows[0]["family_short_name"].ToString() + "' >";
                        strfamily += dt.Rows[0]["family_name"].ToString() + "</a>";
                        lblFamily.Text = strfamily;
                        ViewState["fullname"] = strfamily;
                        strsubfam = "<a href=taxonomyfamily.aspx?type=subfamily&id=" + dt.Rows[0]["subfamily_id"].ToString();
                        strsubfam += " title='Link to GRIN report for subfamily " + dt.Rows[0]["subfamily"].ToString() + "' >";
                        strsubfam += dt.Rows[0]["subfamily"].ToString() + "</a>";
                        lblSubfamily.Text = strsubfam;
                        lblTribe.Text = dt.Rows[0]["tribe"].ToString();
                        Page.DataBind();
                        if (dt.Rows[0]["subfamily"] != DBNull.Value)
                            pnlSubFamily.Visible = true;
                        pnlTribe.Visible = true;
                        break;
                    case "subtribe":
                        ViewState["name"] = dt.Rows[0]["subtribe"];
                        //       _type = "subtribe";
                        value = dt.Rows[0]["family_short_name"] + " subtribe " + dt.Rows[0]["subtribe"];
                        strfamily = "<a href=taxonomyfamily.aspx?id=" + dt.Rows[0]["family_id"].ToString();
                        strfamily += " title='Link to GRIN report for family " + dt.Rows[0]["family_short_name"].ToString() + "' >";
                        strfamily += dt.Rows[0]["family_name"].ToString() + "</a>";
                        lblFamily.Text = strfamily;
                        ViewState["fullname"] = strfamily;
                        strsubfam = "<a href=taxonomyfamily.aspx?type=subfamily&id=" + dt.Rows[0]["subfamily_id"].ToString();
                        strsubfam += " title='Link to GRIN report for subfamily " + dt.Rows[0]["subfamily"].ToString() + "' >";
                        strsubfam += dt.Rows[0]["subfamily"].ToString() + "</a>";
                        lblSubfamily.Text = strsubfam;
                        strtribe = "<a href=taxonomyfamily.aspx?type=tribe&id=" + dt.Rows[0]["tribe_id"].ToString();
                        strtribe += " title='Link to GRIN report for tribe " + dt.Rows[0]["tribe"].ToString() + "' >";
                        strtribe += dt.Rows[0]["tribe"].ToString() + "</a>";
                        lblTribe.Text = strtribe;
                        lblSubtribe.Text = dt.Rows[0]["subtribe"].ToString();
                        Page.DataBind();
                        if (dt.Rows[0]["subfamily"] != DBNull.Value)
                            pnlSubFamily.Visible = true;
                        if (dt.Rows[0]["tribe"] != DBNull.Value)
                            pnlTribe.Visible = true;
                        pnlSubTribe.Visible = true;
                        break;
                }

            }
            else
            {
                ViewState["name"] = dt.Rows[0]["family_short_name"];
                _type = "family";
                value = dt.Rows[0]["family_short_name"].ToString();
                lblFamily.Text = dt.Rows[0]["family_name"].ToString();
                ViewState["fullname"] = dt.Rows[0]["family_name"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["subfamily"].ToString()))
                    pnlSubFamily.Visible = true;
                if (!String.IsNullOrEmpty(dt.Rows[0]["tribe"].ToString()))
                    pnlTribe.Visible = true;
                if (!String.IsNullOrEmpty(dt.Rows[0]["subtribe"].ToString()))
                    pnlSubTribe.Visible = true;
                Page.DataBind();

            }

            if (dt.Rows[0]["altfamily"] == DBNull.Value)
                dvFamily.FindControl("tr_alternatename").Visible = false;
            if (dt.Rows[0]["genus_type"] == DBNull.Value)
                dvFamily.FindControl("tr_typegenus").Visible = false;
            if (dt.Rows[0]["note"] == DBNull.Value)
                dvFamily.FindControl("tr_comments").Visible = false;

            #region nolongerused
            //KMK 11/13/17  This code didn't make sense since it was looking for the follow in the above case/if statements
            //if (_type == "family")
            //{
            //    lblFamily.Text = dt.Rows[0]["family_name"].ToString();
            //    if (!String.IsNullOrEmpty(dt.Rows[0]["subfamily"].ToString()))
            //        pnlSubFamily.Visible = true;
            //    if (!String.IsNullOrEmpty(dt.Rows[0]["tribe"].ToString()))
            //        pnlTribe.Visible = true;
            //    if (!String.IsNullOrEmpty(dt.Rows[0]["subtribe"].ToString()))
            //        pnlSubTribe.Visible = true;
            //}

            //if (_type == "subfamily")
            //{
            //    string strfamily = "<a href=taxonomyfamily.aspx?id=" + dt.Rows[0]["family_id"].ToString();
            //    strfamily += " title='Link to GRIN report for family " + dt.Rows[0]["family_short_name"].ToString() + "' >";
            //    strfamily += dt.Rows[0]["family_name"].ToString() + "</a>";
            //    lblFamily.Text = strfamily; 
            //    lblSubfamily.Text = dt.Rows[0]["subfamily"].ToString();
            //    if (!String.IsNullOrEmpty(dt.Rows[0]["tribe"].ToString()))
            //        pnlTribe.Visible = true;
            //    if (!String.IsNullOrEmpty(dt.Rows[0]["subtribe"].ToString()))
            //       pnlSubTribe.Visible = true;
            //}
            //if (_type == "tribe")
            //{
            //    if (!String.IsNullOrEmpty(dt.Rows[0]["subtribe"].ToString()))
            //        pnlSubTribe.Visible = true;
            //}
            #endregion

            hlRecordlist.NavigateUrl = "taxonomylist.aspx?category=genera&type=" + _type + "&value=" + value + "&id=" + taxonomyfamilyID;


        }

        private void bindReferences(SecureData sd, int taxonomyfamilyID)
        {
            var dt = sd.GetData("web_taxonomyfamily_references", ":taxonomyid=" + taxonomyfamilyID, 0, 0).Tables["web_taxonomyfamily_references"];
            if (dt.Rows.Count > 0)
            {
                DataTable dtRef = new DataTable();
                dtRef = Utils.FormatCitations(dt);
                if (dtRef.Rows.Count > 0)
                {
                    if (dtRef.Rows.Count > 1)
                        lblReference.Text = "References: ";
                    else
                        lblReference.Text = "Reference: ";
                    lblReference.Visible = true;
                    pnlReferences.Visible = true;
                    rptReferences.DataSource = dtRef;
                    rptReferences.DataBind();
                }
            }
        }

        private void bindOtherReferences()
        {
            if (ViewState["type"] == null)
            {
                pnlMore.Visible = true;
                string gName = ViewState["name"].ToString();
                //KMK 04/18/19 Kew Bibliographic database is no longer being maintained.
                //hlKBD.NavigateUrl = "http://www.kew.org/kbd/searchpage.do?general=" + gName;
                txtGoogle.Text = gName;
                btnGoogle.OnClientClick = "javascript:window.open(" + "\"" + "https://scholar.google.com/scholar?q=" + gName + "\"" + ")";
            }
        }

        private void bindSynonyms(SecureData sd, int taxonomyfamilyID)
        {
            var dt = sd.GetData("web_taxonomyfamily_synonyms", ":taxonomyfamilyid=" + taxonomyfamilyID, 0, 0).Tables["web_taxonomyfamily_synonyms"];
            if (dt.Rows.Count > 0)
            {
                pnlSynonyms.Visible = true;
                rptSynonyms.DataSource = dt;
                rptSynonyms.DataBind();
            }
        }


        private void bindCheckOther(SecureData sd, int taxonomyfamilyID)//temp query now
        {
            var dt = sd.GetData("web_taxonomyfamily_checkother", ":taxonomyfamilyid=" + taxonomyfamilyID, 0, 0).Tables["web_taxonomyfamily_checkother"];
            if (dt.Rows.Count > 0)
            {
                pnlCheckOther.Visible = true;
                rptCheckOther.DataSource = dt;
                rptCheckOther.DataBind();
            }
        }

        private void bindSubdivisions(SecureData sd, int taxonomyfamilyID)
        {
            DataTable dt = null;

            if (ViewState["type"] != null)
            {
                switch (ViewState["type"].ToString())
                {
                    case "subfamily":
                        dt = sd.GetData(
                            "web_taxonomyfamily_subdivisions",
                            ":columntype=" + "subfamily_name" +
                            ";:taxonomyfamilyid=" + taxonomyfamilyID
                            , 0, 0).Tables["web_taxonomyfamily_subdivisions"];
                        break;
                    case "tribe":
                        dt = sd.GetData(
                            "web_taxonomyfamily_subdivisions",
                            ":columntype=" + "tribe_name" +
                            ";:taxonomyfamilyid=" + taxonomyfamilyID
                            , 0, 0).Tables["web_taxonomyfamily_subdivisions"];
                        break;
                    case "subtribe":
                        dt = sd.GetData(
                            "web_taxonomyfamily_subdivisions",
                            ":columntype=" + "subtribe_name" +
                            ";:taxonomyfamilyid=" + taxonomyfamilyID
                            , 0, 0).Tables["web_taxonomyfamily_subdivisions"];
                        break;
                }
            }
            else
                dt = sd.GetData(
                    "web_taxonomyfamily_subdivisions",
                    ":columntype=" + "family_name" +
                    ";:taxonomyfamilyid=" + taxonomyfamilyID
                    , 0, 0).Tables["web_taxonomyfamily_subdivisions"];

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow r in dt.Rows)
                {
                    if (r["Subfamily"].ToString() != "")
                        gvSubdivisions.Columns[0].Visible = true;
                    if (r["Tribe"].ToString() != "")
                        gvSubdivisions.Columns[1].Visible = true;
                    if (r["Subtribe"].ToString() != "")
                        gvSubdivisions.Columns[2].Visible = true;
                }
                pnlSubdivisons.Visible = true;
                gvSubdivisions.DataSource = dt;
                gvSubdivisions.DataBind();
            }
        }

        private void bindImages(SecureData sd, int taxonomyfamilyID) //temp query now
        {
            var dt = sd.GetData("web_taxonomyfamily_images", ":taxonomyfamilyid=" + taxonomyfamilyID, 0, 0).Tables["web_taxonomyfamily_images"];
            if (dt.Rows.Count > 0)
            {
                pnlImages.Visible = true;
                rptImages.DataSource = dt;
                rptImages.DataBind();
            }
        }

        protected string getName()
        {
            return ViewState["fullname"].ToString();
        }

        protected string getNameTitle()
        {
            return _type;
        }

        protected string DisplayNote(object note)
        {
            if (!String.IsNullOrEmpty(note.ToString()))
            {
                string note1 = note as string;

                //int i = note1.IndexOf("\\;");
                note1 = System.Text.RegularExpressions.Regex.Split(note1, "\\;")[0];
                if (note1.Length > 1)
                {
                    if (note1.Substring(note1.Length - 1, 1) == "\\")
                        note1 = note1.Substring(0, note1.Length - 1);
                }

                return note1;

            }
            return "";
        }
        protected void bindCitation()
        {
            string url = HttpContext.Current.Request.Url.AbsoluteUri;
            string cite = "Cite as: USDA, Agricultural Research Service, National Plant Germplasm System. ";
            cite += DateTime.Today.Year + ". ";
            cite += "Germplasm Resources Information Network (GRIN-Taxonomy).<br /> National Germplasm Resources Laboratory, Beltsville, Maryland. URL: ";
            cite += url + ". </br>Accessed ";
            cite += DateTime.Today.ToString("d MMMM yyyy") + ".";
            lblCite.Text = cite;
            pnlcite.Visible = true;

        }
        protected void btnNewFamily_Click(object sender, EventArgs e)
        {
            Response.Redirect("taxon/famgensearch.aspx");
        }
        protected string DisplayComment(object note)
        {
            if (!String.IsNullOrEmpty(note.ToString()))
            {
                string note1 = note as string;

                string[] comments = Regex.Split(note1, @"\\\;");

                if (comments.Length > 1)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ul>");
                    foreach (string comment in comments)
                    {
                        sb.Append("<li>&#149 ").Append(comment).Append("</li>");
                    }
                    sb.Append("</ul>");
                    return sb.ToString();
                }
                else
                    return note1;
            }
            return "";
        }
        protected string DisplayCommentHead(object note)
        {
            if (!String.IsNullOrEmpty(note.ToString()))
            {
                string note1 = note as string;

                string[] comments = Regex.Split(note1, @"\\\;");

                if (comments.Length > 1)
                    return "Comments:";
                else
                    return "Comment:";
            }
            return "";
        }
    }
}
