﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site1.Master" CodeBehind="taxonomylist.aspx.cs" Inherits="GrinGlobal.Web.TaxonomyList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
 <style>
        .fixed {position:fixed;
                top:300px;
                left: 950px;
        }
    </style>
<h1><asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></h1>
<br />
    <a name="top"></a>
<a href="#top"><asp:Button ID="btnTop" runat="server" class="fixed" Text="Top of page" /></a>
<asp:Repeater ID="rptRecordlist" runat="server">
    <HeaderTemplate>
        <ol>
    </HeaderTemplate>
    <ItemTemplate>
        <li><%# Eval("name")%></li>
    </ItemTemplate>
    <FooterTemplate>
        </ol>
    </FooterTemplate>
</asp:Repeater> 
    
</asp:Content>