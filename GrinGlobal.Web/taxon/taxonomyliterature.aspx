﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="taxonomyliterature.aspx.cs" Inherits="GrinGlobal.Web.taxon.taxonomyliterature" Title="Literature citations for crop relative gene pool assignment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<link href="~/styles/default.css" rel="stylesheet" type="text/css" />
<title>Literature citations for crop relative gene pool assignment</title></head>
<body>
    <form id="form1" runat="server">
    <div style="margin-left:20px">
<%--        KMK 10/30/17 Formatted the references to display per taxonomist request --%>
        <asp:Panel ID="pnlReference" runat="server">
             <center><h1><%= Page.DisplayText("htmlTaxonLiterature", "Literature References for GRIN Taxonomy Crop and Crop Wild Relative Breeding Data")%></h1></center> 
        <strong> <asp:Label ID="lblTaxon" runat="server" Text=""></asp:Label></strong><br /><br />
            <asp:Repeater ID="rptReference" runat="server">   
            
        <HeaderTemplate>
      <%--    <asp:Label ID="lblHeader" runat="server"><%# Eval("citation_year")%></asp:Label>--%>
        </HeaderTemplate>
        <ItemTemplate>
                <li>
                <%# Eval("author").ToString().Last()=='.'? Eval("author") : Eval("author") + "."%><%# Eval("citation_year").ToString() == "" ? "" : " " + Eval("citation_year")+"." %> <%# Eval("title").ToString().Last() == '.' ? Eval("title") : Eval("title") + "."%> <%# Eval("abbrev")%> <%# Eval("reference").ToString().Replace("-","–")%> <%# Eval("cit_note").ToString() == "" ? "" : "["+Eval("cit_note")+"]" %>
                </li>
         </ItemTemplate>
        </asp:Repeater>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
