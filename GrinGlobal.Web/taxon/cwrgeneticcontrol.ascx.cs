﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrinGlobal.Core;
using GrinGlobal.Business;
using System.Data;



namespace GrinGlobal.Web.taxon
{
    public partial class cwrgeneticcontrol : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindData();
        }

        private string cropid;
        public string Crop
        {
            get { return cropid; }
            set { cropid = value; }
        }

        private int cropcount;
        public int CropCount
        {
            get { return cropcount; }
            set { cropcount = value; }
        }

        private bool primary;
        public bool Primary
        {
            get { return primary; }
            set { primary = value; }
        }

        private bool secondary;
        public bool Secondary
        {
            get { return secondary; }
            set { secondary = value; }
        }

        private bool tertiary;
        public bool Tertiary
        {
            get { return tertiary; }
            set { tertiary = value; }
        }

        private bool graftstock;
        public bool Graftstock
        {
            get { return graftstock; }
            set { graftstock = value; }
        }

        private bool hasdata;
        public bool HasData
        {
            get { return hasdata; }
            set { hasdata = value; }
        }

        private string sqlbase;
        public string SqlBase
        {
            set { sqlbase = value; }
        }

        private string altcropname;
        public string AltCropName
        {
            get { return altcropname; }
            set { altcropname = value; }
        }


        private void bindData()
        {
            using (var sd = new SecureData(false, UserManager.GetLoginToken(true)))
            {
                using (DataManager dm = sd.BeginProcessing(true, true))
                {
                    try
                    {
                        string sql = "";
                        DataTable dt = null;

//                        // crop
//                        var dt = dm.Read(@"
//                                            select alternate_crop_name, crop_genepool_reviewers, crop_id from taxonomy_crop_map where crop_id = :cropid and crop_genepool_reviewers is not null",
//                        new DataParameters(":cropid", cropid));

//                        if (dt.Rows.Count > 0)
//                        {
//                            DataRow dr = dt.Rows[0];
//                            lblCrop.Text = "Crop:  " + dr["alternate_crop_name"].ToString();

//                            lblCompiled.Text = "(" + dr["crop_genepool_reviewers"].ToString() + ")";
//                        }

//                        // crop taxon
//                        if (cropcount > 1)
//                            lblCropTaxon.Text = "Crop taxa:";
//                        else
//                            lblCropTaxon.Text = "Crop taxon:";

//                        rptTaxon.DataSource = sd.GetData("web_taxonomycwr_croptaxon", ":cropid=" + cropid, 0, 0).Tables["web_taxonomycwr_croptaxon"];
//                        rptTaxon.DataBind();

                        
                        // genepool
                        if (Primary)
                        {
                            sql = sqlbase + "  and tcm.alternate_crop_name = :acn and tcm.is_primary_genepool  = 'Y' order by taxonomy_name";

                            dt = dm.Read(sql, new DataParameters(":acn", altcropname, DbType.String));
                            if (dt.Rows.Count > 0)
                            {
                                hasdata = true;
                                rptPrimary.DataSource = dt;                             
                                rptPrimary.DataBind();                        
                                rowPrimary.Visible = true;
                            }
                            else rowPrimary.Visible = false;
                        }

                        if (Secondary)
                        {
                            sql = sqlbase + "  and tcm.alternate_crop_name = :acn and tcm.is_secondary_genepool = 'Y' order by taxonomy_name";

                            dt = dm.Read(sql, new DataParameters(":acn", altcropname, DbType.String));
                            if (dt.Rows.Count > 0)
                            {
                                hasdata = true;

                                rptSecondary.DataSource = dt;
                                rptSecondary.DataBind();
                                rowSecondary.Visible = true;
                            }
                            else rowSecondary.Visible = false;
                        
                        }

                        if (Tertiary)
                        {
                            sql = sqlbase + " and tcm.alternate_crop_name = :acn and tcm.is_tertiary_genepool = 'Y' order by taxonomy_name";

                            dt = dm.Read(sql, new DataParameters(":acn", altcropname, DbType.String));
                            if (dt.Rows.Count > 0)
                            {
                                hasdata = true;

                                rptTertiary.DataSource = dt;
                                rptTertiary.DataBind();
                                rowTertiary.Visible = true;
                            }
                            else rowTertiary.Visible = false;
                        }

                        if (Graftstock)
                        {
                            sql = sqlbase + "  and tcm.alternate_crop_name = :acn and tcm.is_graftstock_genepool  = 'Y' order by taxonomy_name";

                            dt = dm.Read(sql, new DataParameters(":acn", altcropname , DbType.String));
                            if (dt.Rows.Count > 0)
                            {
                                hasdata = true;

                                rptGraftstock.DataSource = dt;
                                rptGraftstock.DataBind();
                                rowGraftstock.Visible = true;
                            }
                            else rowGraftstock.Visible = false;
                        }

                        if (hasdata)  // only show when there is some cwr data
                        {
                            // crop
                            dt = dm.Read(@"
                                            select alternate_crop_name, crop_genepool_reviewers, crop_id from taxonomy_crop_map where alternate_crop_name = :acn and crop_genepool_reviewers is not null",
                            new DataParameters(":acn", altcropname , DbType.String));


                            if (dt.Rows.Count > 0)
                            {
                                DataRow dr = dt.Rows[0];
                                lblCrop.Text = "Crop:  " + dr["alternate_crop_name"].ToString();

                                lblCompiled.Text = "(" + dr["crop_genepool_reviewers"].ToString() + ")";
                            }

                            // crop taxon
                            dt = sd.GetData("web_taxonomycwr_croptaxon", ":acn=" + altcropname, 0, 0).Tables["web_taxonomycwr_croptaxon"];
                            cropcount = dt.Rows.Count;
                            if (cropcount > 1)
                                lblCropTaxon.Text = "Crop taxa:";
                            else
                                lblCropTaxon.Text = "Crop taxon:";


                            rptTaxon.DataSource = dt;
                            rptTaxon.DataBind();
                            pnlControl.Visible = true;
                        }
                        else
                            pnlControl.Visible = false;
                    }
                    catch (Exception e)
                    {
                        string err = e.Message;
                        pnlControl.Visible = false;
                    }
                }
            }
        }

        
    }
}