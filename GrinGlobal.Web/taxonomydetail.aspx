﻿<%@ Page Title="Taxonomy" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="taxonomydetail.aspx.cs" Inherits="GrinGlobal.Web.TaxonomyDetail" %>

<%@ Import Namespace="GrinGlobal.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type='text/javascript'>
        $(document).ready(function () {
            $(".searchBox").keypress(function (event) {
                var keycode = event.charCode || event.keyCode || 0;
                if (event.ctrlKey && keycode == 13) {
                    $("#<%= btnSearch.ClientID %>").click();
               }
           });

           $(function () {
               $('input[type=text]').focus(function () {
                   if ($(this).val() == 'New Search')
                       $(this).val('');
               });

           });

       });
    </script>
    <style type="text/css">
        .status {
            font-size: 17px;
            font-weight: bold;
        }

        .continent {
            margin-left: 20px;
            font-size: 15px;
            font-weight: bold;
        }

        .subcontinent {
            margin-left: 10px;
            line-height: 1.5;
        }

        .country {
            font-weight: bold;
        }

        .space {
            margin-left: 5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <table>
        <tr>
            <td>
                <asp:Label ID="lblLinks" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:DetailsView ID="dvTaxonomy" runat="server" AutoGenerateRows="false" DefaultMode="ReadOnly" CssClass='detail' GridLines="None">
                    <FieldHeaderStyle CssClass="" />
                    <HeaderTemplate>
                        <h1 style="font-size: 150%"><a href='taxon/abouttaxonomy.aspx?chapter=scient' target='_blank'>Taxon:</a>
                            <%# Eval("taxonomy_name") %></h1>

                        <%#Eval("synonym_for_taxonomy")%>
                    </HeaderTemplate>
                    <EmptyDataTemplate>
                        No taxonomy data found
                    </EmptyDataTemplate>
                    <Fields>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <table runat="server" cellpadding='1' cellspacing='1' border='0' class='grid horiz' style='width: 635px; border: 1px solid black'>
                                    <tr id="tr_genus">
                                        <th>Genus:</th>
                                        <td><i><%# Eval("genus_name") %></i></td>
                                    </tr>
                                    <tr id="tr_subgenus">
                                        <th>Subgenus:</th>
                                        <td><span style="padding: 5px"></span><i><%# Eval("subgenus_name") %></i></td>
                                    </tr>
                                    <tr id="tr_section">
                                        <th>Section:</th>
                                        <td><span style="padding: 10px"></span><i><%# Eval("section_name") %></i></td>
                                    </tr>
                                    <tr id="tr_subsection">
                                        <th>Subsection:</th>
                                        <td><span style="padding: 15px"></span><i><%# Eval("subsection_name") %></i></td>
                                    </tr>
                                    <tr id="tr_family">
                                        <th>Family:</th>
                                        <td><i><%# Eval("family_name") %></i><%# Eval("alt_familyname").ToString() == "" ? "" : " (alt.<i>" + Eval("alt_familyname").ToString() + "</i>)"%></td>
                                    </tr>
                                    <tr id="tr_subfamily">
                                        <th>Subfamily:</th>
                                        <td><span style="padding: 5px"></span><i><%# Eval("subfamily") %></i></td>
                                    </tr>
                                    <tr id="tr_tribe">
                                        <th>Tribe:</th>
                                        <td><span style="padding: 10px"></span><i><%# Eval("tribe") %></i></td>
                                    </tr>
                                    <tr id="tr_subtribe">
                                        <th>Subtribe:</th>
                                        <td><i><%# Eval("subtribe")%></i></td>
                                    </tr>
                                    <tr>
                                        <th>Nomen number:</th>
                                        <td><%# Eval("Nomen_number") %></td>
                                    </tr>
                                    <tr id="tr_protologue">
                                        <th>Place of publication:</th>
                                        <td><%# Eval("protologue") %></td>
                                    </tr>
                                    <tr id="tr_protologue_path">
                                        <th>Link to protologue:</th>
                                        <td><a href="<%# Eval("protologue_virtual_path") %>" target="_blank"><%# Eval("protologue_virtual_path") %></a></td>
                                    </tr>
                                    <tr id="tr_comment">
                                        <th><%# DisplayCommentHead(Eval("note")) %></th>
                                        <td><%# DisplayComment(Eval("note")) %>
                                        </td>
                                    </tr>
                                    <tr id="tr_typification">
                                        <th>Typification:</th>
                                        <td><%# Eval("typification") %></td>
                                    </tr>
                                    <tr>
                                        <th>Name Verified on:</th>
                                        <td><%# DisplayVerified(Eval("name_verified_date"), Eval("verifier_cooperator_id"), Eval("verifier_name"))%> </td>

                                    </tr>
                                   <%--KMK 5/6/19  Removed per taxonomist 
                                        <tr>
                                        <th>Species priority site is:</th>
                                        <td><%# Eval("site_1_long") %>  <%# Eval("site_1_long").ToString() == "" ? "" : "(" + Eval("priority_site_1") + ")"%></td>
                                    </tr>--%>
                                    <tr>
                                        <th>Accessions:</th>
                                        <td><a href='view2.aspx?dv=web_taxonomyspecies_view_accessionlist&params=:taxonomyid=<%# Eval("taxonomy_species_id")%>' title="Active: Living material present in NPGS; Available: Can be ordered.">
                                            <%# Eval("access_count") %> (<%# Eval("active_count") %> active, <%# Eval("avail_count") %> available)</a>
                                            in National Plant Germplasm System <%# Eval("acc_mapcnt").ToString() == "0" || Eval("Nolocation").ToString() == "Y" || Convert.ToInt32((Eval("acc_mapcnt").ToString())) > 3000 ? "" : " (" + "<a href=\"maps.aspx?taxonomyid=" + Eval("taxonomy_species_id") + "\"</a>" + "Map)"%></td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Fields>
                </asp:DetailsView>
            </td>
            <asp:Button ID="btnSearch" runat="server" Text="" class="right"
                OnClick="btnSearch_Click" BackColor="White" Width="1px"
                BorderStyle="None" />
            <div class="top right">
                <asp:TextBox ID="txtSearch" runat="server" Text="New Search"
                    onclick="btnSearch_Click"></asp:TextBox>
        </tr>
    </table>

    <asp:Panel ID="pnlConspecific" runat="server" Visible="false">
        <asp:Repeater ID="rptConspecific" runat="server">
            <HeaderTemplate>
                <h1><%= Page.DisplayText("htmlOther", "See other conspecific taxa:")%></h1>
                <ul style="list-style: none">
            </HeaderTemplate>
            <ItemTemplate>
                <li><a href='taxonomydetail.aspx?id=<%# Eval("taxonomy_species_id")%>'><i><%# ItalicTaxon(Eval("name"))%></i></a> (<a href='view2.aspx?dv=web_taxonomyspecies_view_accessionlist&params=:taxonomyid=<%# Eval("taxonomy_species_id")%>'><%# Eval("accession_count") %> accessions</a>)</li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>

    <asp:Panel ID="pnlSynonyms" runat="server" Visible="false">
        <a name="Synonyms"></a>
        <asp:Repeater ID="rptSynonyms" runat="server">
            <HeaderTemplate>
                <h1>Autonyms (not in current use), synonyms and invalid designations:</h1>
                (≡ homotypic synonym, = heterotypic synonym, - autonym, I invalid designation)
                <ul style="list-style: none">
            </HeaderTemplate>
            <ItemTemplate>
                <li><%# SynonymSign(Eval("synonym_code"))%><a href='taxonomydetail.aspx?id=<%# Eval("taxonomy_species_id") %>'><i><%# ItalicTaxon(Eval("name"))%></i></a> <%# Eval("authority") %></li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>

    <asp:Panel ID="pnlCommonNames" runat="server" Visible="false">
        <a name="CommonNames"></a>
        <asp:Repeater ID="rptCommonNames" runat="server">
            <HeaderTemplate>
                <h1><a href='taxon/abouttaxonomy.aspx?chapter=common' target='_blank'><%= Page.DisplayText("htmlCommon", "Common names:")%></a></h1>
                <ul style="list-style: none">
            </HeaderTemplate>
            <ItemTemplate>
                <li><b><%# Eval("name") %></b>&nbsp;&nbsp;<small><%# DisplayCommonNameCitation(Eval("literature_id"), Eval("source"))%> - <%#Eval("language_description") %></small></li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>

    <asp:Panel ID="pnlEconomicImportance" runat="server" Visible="false">
       <%-- KMK 05/30/18 Completely changed this to display correctly.--%>
        <a name="Economic"></a>
        <h1><a href='taxon/abouttaxonomy.aspx?chapter=econ' target='_blank'><%= Page.DisplayText("htmlEconomic", "Economic Importance:")%></a></h1>
        <asp:Literal ID="ltEcon" runat="server"></asp:Literal>
    </asp:Panel>


    <asp:Panel ID="pnlDistributionRange" runat="server" Visible="false">
        <a name="Distribution"></a>
        <h1><a href='taxon/abouttaxonomy.aspx?chapter=distrib' target='_blank'><%= Page.DisplayText("htmlDistribution", "Distributional Range:")%></a></h1>
        <asp:Repeater ID="rptDistribution" runat="server">
            <HeaderTemplate>
                <ul style="list-style: none">
            </HeaderTemplate>
            <ItemTemplate>

                <span class="status"><%# Eval("geo_status")%></span><span class="space"></span>
                <asp:Label ID="lblGeoNote" runat="server" Visible="False"></asp:Label><br />
                <br />
                <asp:Repeater ID="rptDistributionRange" runat="server">
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <span class="continent"><%# Eval("continent") %></span><span class="space"></span>
                        <asp:Label ID="lblContinent" runat="server" Visible="False"></asp:Label><br />
                        <asp:Repeater ID="rptDistSubcontinent" runat="server">
                            <HeaderTemplate>
                                <ul style="list-style: none">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <%--  KMK 12/27/17 Changed display a little to account for no subcontinent --%>
                                    <span class="subcontinent"><%# Eval("subcontinent").ToString() == "" ? "" : Eval("subcontinent").ToString().ToUpper()+ ":"%></span>

                                    <asp:Repeater ID="rptDistCountry" runat="server">
                                        <ItemTemplate>
                                            <b><%# Eval("country_name") %></b><%# Eval("note").ToString() == "" ? "" : Eval("note") %>

                                            <asp:Repeater ID="rptDistState" runat="server" Visible="false">
                                                <ItemTemplate>
                                                    <%# Eval("state_name").ToString() == "" ? "" : Eval("state_name") %>
                                                    <%# Eval("note").ToString() == "" ? "" : Eval("note")   %>
                                                </ItemTemplate>

                                            </asp:Repeater>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                </li>
                            </ItemTemplate>
                            <FooterTemplate></ul></FooterTemplate>
                        </asp:Repeater>
                        <br />
                    </ItemTemplate>
                    <FooterTemplate>
                        <br />
                    </FooterTemplate>
                </asp:Repeater>

            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>

    <asp:Panel ID="pnlReferences" runat="server" Visible="false">
        <a name="References"></a>
        <h1><a href='taxon/abouttaxonomy.aspx?chapter=liter' target='_blank'>
            <asp:Label ID="lblReference" runat="server" Visible="false"></asp:Label></a></h1>
        <asp:Repeater ID="rptReferences" runat="server">
           <HeaderTemplate>
            <ol>
        </HeaderTemplate>
        <ItemTemplate>
            <li><%# Eval("reference") %></li>
        </ItemTemplate>
        <FooterTemplate>
            </ol>
        </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <a name="Other"></a>
    <asp:Repeater ID="rptCheckOther" runat="server">

        <HeaderTemplate>
            <h1><%= Page.DisplayText("htmlCheck", "Check other web resources for")%> <%# getName()%>:</h1>
            <ul style="list-style: none">
        </HeaderTemplate>
        <ItemTemplate>
            <li><%# Eval("otherPre") %><b><%# Eval("otherDBlink") %></b> <%# Eval("otherDB") %> </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>

    <asp:Panel ID="pnlImage" runat="server" Visible="false">
        <a name="Images"></a>
        <asp:Repeater ID="rptImages" runat="server">
            <HeaderTemplate>
                <h1><%= Page.DisplayText("htmlImages", "Images:")%></h1>
                <ul style="list-style: none">
            </HeaderTemplate>
            <ItemTemplate>
                <li><b><%# Eval("title") %>: </b><%# DisplayNote(Eval("note")) %></li>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </asp:Panel>
    <hr />
    <asp:Panel ID="pnlcite" runat="server" Visible="false">
        <a name="Cite"></a>
        <asp:Literal ID="ltCite" runat="server"></asp:Literal>
    </asp:Panel>
    <br />
</asp:Content>
