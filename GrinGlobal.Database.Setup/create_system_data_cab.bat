@echo off &setlocal

set "source=.\initial_data"
set "cabFolder=."
set "cabName=system_data.cab"

:: create definition file
>GGdata.ddf echo .Option Explicit
>>GGdata.ddf echo .Set SourceDir="%source%"
>>GGdata.ddf echo .Set DiskDirectoryTemplate="%cabFolder%"
>>GGdata.ddf echo .Set CabinetNameTemplate="%cabName%"
>>GGdata.ddf echo .Set MaxDiskSize=0
>>GGdata.ddf echo .Set CabinetFileCountThreshold=0
>>GGdata.ddf echo .Set UniqueFiles=OFF
>>GGdata.ddf echo .Set Cabinet=ON
>>GGdata.ddf echo .Set Compress=ON
>>GGdata.ddf echo .Set CompressionType=MSZIP

:: add files names from source directory 
for /f "delims=" %%i in ('dir /a-d /b "%source%"') do (
  set "line=%%i"
  setlocal enabledelayedexpansion
  >>GGdata.ddf echo "!line!"
  endlocal
)

makecab /f GGdata.ddf

:: clean up
del setup.inf
del setup.rpt
del GGdata.ddf

pause
