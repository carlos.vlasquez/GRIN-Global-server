@ECHO OFF

set GG=%~dp0\..
set INITDATA=%~dp0\initial_data
set RAWDATA=%GG%\scratch\raw_data_files

echo ****************************************************************************
echo.
echo Exporting schema and data from database to files...
echo.
echo ****************************************************************************

del /q /s /f "%RAWDATA%\*.*"
"%GG%\GrinGlobal.DatabaseCopier\bin\Debug\GrinGlobal.DataBaseCopier.exe" /export "%RAWDATA%" sqlserver "Data Source=localhost\sqlexpress;Initial Catalog=gringlobal;Trusted_Connection=True" gringlobal

REM do not package up the user settings tables, those should be empty from the get-go
del %RAWDATA%\app_user_item_list.txt
del %RAWDATA%\app_user_gui_setting.txt

mkdir %INITDATA%
copy %RAWDATA%\sys_*.* %INITDATA%
copy %RAWDATA%\app_*.* %INITDATA%
copy %RAWDATA%\site.* %INITDATA%
copy %RAWDATA%\cooperator.* %INITDATA%
copy %RAWDATA%\geography.* %INITDATA%
copy %RAWDATA%\code*.* %INITDATA%
copy %RAWDATA%\web_user.* %INITDATA%
copy %RAWDATA%\web_user_preference.* %INITDATA%
copy %RAWDATA%\web_cooperator.* %INITDATA%
copy %RAWDATA%\web_help.* %INITDATA%

copy %RAWDATA%\__schema.xml %INITDATA%\..

echo ****************************************************************************
echo Check for errors above!
echo ****************************************************************************
pause