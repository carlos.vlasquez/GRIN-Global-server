﻿sys_group_lang_id	sys_group_id	sys_lang_id	title	description	created_date	created_by	modified_date	modified_by	owned_date	owned_by
1	1	1	Administrators	GRIN-Global Administrators	2010-03-16 23:04:37	1	2010-03-17 00:15:58	1	2010-03-16 23:04:37	1
2	2	1	All Users	All users of any GRIN-Global application	2010-03-16 23:05:56	1	2010-03-16 23:25:20	1	2010-03-16 23:05:56	1
3	3	1	CT Users	All GRIN-Global users who should have rights to use all functionality of the CT.	2010-03-16 23:06:30	1	2010-07-01 15:19:14	1	2010-03-16 23:06:30	1
4	4	1	Feedback Owners	Users who can manage the feedback programs, reports, and forms.	2010-12-16 08:30:42	48	\N	\N	2010-12-16 08:30:42	48
5	5	1	Feedback Submitters	Users who can submit feedback	2010-12-16 08:41:24	48	2010-12-16 08:42:20	48	2010-12-16 08:41:24	48
6	6	1	Web Query Users	Enable users in this group to use tools menu, with web query tool etc.	2013-01-14 23:09:05	48	\N	\N	2013-01-14 23:09:05	48
