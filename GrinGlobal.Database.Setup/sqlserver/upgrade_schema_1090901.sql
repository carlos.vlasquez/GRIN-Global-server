BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

/* ----- Accession Source ----- */

DECLARE @drop_sql NVARCHAR(MAX) = N'';
DECLARE @table_name nvarchar(255) = 'accession_source';

-- drop default constraints on table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name)
	+ ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + '; '
FROM sys.default_constraints AS dc
INNER JOIN sys.tables AS ct ON dc.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE dc.parent_object_id = OBJECT_ID(@table_name)

-- drop foreing key constraints involving the table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + '; '
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE fk.parent_object_id = OBJECT_ID(@table_name) OR fk.referenced_object_id = OBJECT_ID(@table_name)

-- execute the drops
EXECUTE sp_executesql @drop_sql

GO
COMMIT
GO
CREATE TABLE dbo.Tmp_accession_source
	(
	accession_source_id int NOT NULL IDENTITY (1, 1),
	accession_id int NOT NULL,
	geography_id int NULL,
	acquisition_source_code nvarchar(20) NULL,
	source_type_code nvarchar(20) NOT NULL,
	source_date datetime2(7) NULL,
	source_date_code nvarchar(20) NULL,
	is_origin nvarchar(1) NOT NULL,
	quantity_collected decimal(18, 5) NULL,
	unit_quantity_collected_code nvarchar(20) NULL,
	collected_form_code nvarchar(20) NULL,
	number_plants_sampled int NULL,
	elevation_meters int NULL,
	collector_verbatim_locality nvarchar(MAX) NULL,
	latitude decimal(18, 8) NULL,
	longitude decimal(18, 8) NULL,
	uncertainty int NULL,
	formatted_locality nvarchar(MAX) NULL,
	georeference_datum nvarchar(10) NULL,
	georeference_protocol_code nvarchar(20) NULL,
	georeference_annotation nvarchar(MAX) NULL,
	environment_description nvarchar(MAX) NULL,
	associated_species nvarchar(MAX) NULL,
	is_web_visible nvarchar(1) NOT NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_accession_source SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_accession_source TO [NT AUTHORITY\NETWORK SERVICE]  AS dbo
GO
GRANT INSERT ON dbo.Tmp_accession_source TO [NT AUTHORITY\NETWORK SERVICE]  AS dbo
GO
GRANT SELECT ON dbo.Tmp_accession_source TO [NT AUTHORITY\NETWORK SERVICE]  AS dbo
GO
GRANT SELECT ON dbo.Tmp_accession_source TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_accession_source TO [NT AUTHORITY\NETWORK SERVICE]  AS dbo
GO
ALTER TABLE dbo.Tmp_accession_source ADD CONSTRAINT
	DF__accession__is_we__1920BF5C DEFAULT ('Y') FOR is_web_visible
GO
SET IDENTITY_INSERT dbo.Tmp_accession_source ON
GO
IF EXISTS(SELECT * FROM dbo.accession_source)
	 EXEC('INSERT INTO dbo.Tmp_accession_source (accession_source_id, accession_id, geography_id, acquisition_source_code, source_type_code, source_date, source_date_code, is_origin, quantity_collected, unit_quantity_collected_code, collected_form_code, number_plants_sampled, elevation_meters, collector_verbatim_locality, latitude, longitude, uncertainty, formatted_locality, georeference_datum, georeference_protocol_code, georeference_annotation, environment_description, is_web_visible, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT accession_source_id, accession_id, geography_id, acquisition_source_code, source_type_code, source_date, source_date_code, is_origin, quantity_collected, unit_quantity_collected_code, collected_form_code, number_plants_sampled, elevation_meters, collector_verbatim_locality, latitude, longitude, uncertainty, formatted_locality, georeference_datum, georeference_protocol_code, georeference_annotation, environment_description, is_web_visible, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.accession_source WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_accession_source OFF
GO
DROP TABLE dbo.accession_source
GO
EXECUTE sp_rename N'dbo.Tmp_accession_source', N'accession_source', 'OBJECT' 
GO
ALTER TABLE dbo.accession_source ADD CONSTRAINT
	PK_accession_source PRIMARY KEY CLUSTERED 
	(
	accession_source_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_as_a ON dbo.accession_source
	(
	accession_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_as_created ON dbo.accession_source
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_as_g ON dbo.accession_source
	(
	geography_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_as_modified ON dbo.accession_source
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_as_owned ON dbo.accession_source
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_sr ON dbo.accession_source
	(
	accession_id,
	source_type_code,
	source_date,
	geography_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX _dta_index_accession_source_7_389576426__K2_K8_K1_K3_K15_K16_K13_K9_K10_K11_K12_6_7_14_22_24 ON dbo.accession_source
	(
	accession_id,
	is_origin,
	accession_source_id,
	geography_id,
	latitude,
	longitude,
	elevation_meters,
	quantity_collected,
	unit_quantity_collected_code,
	collected_form_code,
	number_plants_sampled
	) INCLUDE (source_date_code, collector_verbatim_locality, environment_description, note, source_date) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX _dta_index_accession_source_7_389576426__K8_K2_K15_K16_K13_K9_K10_K11_K12_1_3_6_7_14_22_24 ON dbo.accession_source
	(
	is_origin,
	accession_id,
	latitude,
	longitude,
	elevation_meters,
	quantity_collected,
	unit_quantity_collected_code,
	collected_form_code,
	number_plants_sampled
	) INCLUDE (source_date_code, collector_verbatim_locality, environment_description, note, accession_source_id, geography_id, source_date) 
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.accession_source ADD CONSTRAINT
	fk_as_a FOREIGN KEY
	(
	accession_id
	) REFERENCES dbo.accession
	(
	accession_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_source ADD CONSTRAINT
	fk_as_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_source ADD CONSTRAINT
	fk_as_g FOREIGN KEY
	(
	geography_id
	) REFERENCES dbo.geography
	(
	geography_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_source ADD CONSTRAINT
	fk_as_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_source ADD CONSTRAINT
	fk_as_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_source', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_source', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_source', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_source_map ADD CONSTRAINT
	fk_asm_as FOREIGN KEY
	(
	accession_source_id
	) REFERENCES dbo.accession_source
	(
	accession_source_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_source_map SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_source_map', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_source_map', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_source_map', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.source_desc_observation ADD CONSTRAINT
	fk_sodo_as FOREIGN KEY
	(
	accession_source_id
	) REFERENCES dbo.accession_source
	(
	accession_source_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.source_desc_observation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.source_desc_observation', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.source_desc_observation', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.source_desc_observation', 'Object', 'CONTROL') as Contr_Per IF EXISTS(SELECT 1 FROM sys.fulltext_catalogs WHERE name = 'gringlobalftsc') CREATE FULLTEXT INDEX ON dbo.accession_source
( 
	collector_verbatim_locality LANGUAGE 1033, 
	formatted_locality LANGUAGE 1033, 
	environment_description LANGUAGE 1033, 
	note LANGUAGE 1033
 )
KEY INDEX PK_accession_source
ON gringlobalftsc
 WITH  CHANGE_TRACKING  AUTO 
GO
ALTER FULLTEXT INDEX ON dbo.accession_source
ENABLE 
GO
