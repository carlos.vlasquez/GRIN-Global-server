/*
   Tuesday, March 20, 201812:12:47 PM
   User: 
   Server: ARSMDBE3445BLD2\SQLEXPRESS
   Database: gg191
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_inv_name
	DROP CONSTRAINT fk_ain_ng
GO
ALTER TABLE dbo.name_group SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_inv_name
	DROP CONSTRAINT fk_ain_i
GO
ALTER TABLE dbo.inventory SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_inv_name
	DROP CONSTRAINT fk_ain_c
GO
ALTER TABLE dbo.accession_inv_name
	DROP CONSTRAINT fk_ain_created
GO
ALTER TABLE dbo.accession_inv_name
	DROP CONSTRAINT fk_ain_modified
GO
ALTER TABLE dbo.accession_inv_name
	DROP CONSTRAINT fk_ain_owned
GO
ALTER TABLE dbo.cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_accession_inv_name
	(
	accession_inv_name_id int NOT NULL IDENTITY (1, 1),
	inventory_id int NOT NULL,
	category_code nvarchar(20) NOT NULL,
	plant_name nvarchar(100) NOT NULL,
	plant_name_rank int NULL,
	name_group_id int NULL,
	name_source_cooperator_id int NULL,
	is_web_visible nvarchar(1) NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_accession_inv_name SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_accession_inv_name TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_accession_inv_name TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_accession_inv_name TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_accession_inv_name TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_accession_inv_name TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_accession_inv_name ON
GO
IF EXISTS(SELECT * FROM dbo.accession_inv_name)
	 EXEC('INSERT INTO dbo.Tmp_accession_inv_name (accession_inv_name_id, inventory_id, category_code, plant_name, plant_name_rank, name_group_id, name_source_cooperator_id, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT accession_inv_name_id, inventory_id, category_code, plant_name, plant_name_rank, name_group_id, name_source_cooperator_id, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.accession_inv_name WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_accession_inv_name OFF
GO
DROP TABLE dbo.accession_inv_name
GO
EXECUTE sp_rename N'dbo.Tmp_accession_inv_name', N'accession_inv_name', 'OBJECT' 
GO
ALTER TABLE dbo.accession_inv_name ADD CONSTRAINT
	PK_accession_inv_name PRIMARY KEY CLUSTERED 
	(
	accession_inv_name_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_ain_c ON dbo.accession_inv_name
	(
	name_source_cooperator_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ain_created ON dbo.accession_inv_name
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ain_i ON dbo.accession_inv_name
	(
	inventory_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ain_modified ON dbo.accession_inv_name
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ain_ng ON dbo.accession_inv_name
	(
	name_group_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ain_owned ON dbo.accession_inv_name
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_ain ON dbo.accession_inv_name
	(
	inventory_id,
	plant_name,
	name_group_id,
	category_code
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.accession_inv_name ADD CONSTRAINT
	fk_ain_c FOREIGN KEY
	(
	name_source_cooperator_id
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_name ADD CONSTRAINT
	fk_ain_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_name ADD CONSTRAINT
	fk_ain_i FOREIGN KEY
	(
	inventory_id
	) REFERENCES dbo.inventory
	(
	inventory_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_name ADD CONSTRAINT
	fk_ain_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_name ADD CONSTRAINT
	fk_ain_ng FOREIGN KEY
	(
	name_group_id
	) REFERENCES dbo.name_group
	(
	name_group_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_name ADD CONSTRAINT
	fk_ain_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT



/****** Object:  Index [ndx_uniq_tcn]    Script Date: 3/20/2018 3:49:48 PM ******/
DROP INDEX [ndx_uniq_tcn] ON [dbo].[taxonomy_common_name]
GO

/****** Object:  Index [ndx_uniq_tcn]    Script Date: 3/20/2018 3:49:48 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcn] ON [dbo].[taxonomy_common_name]
(
	[taxonomy_genus_id] ASC,
	[taxonomy_species_id] ASC,
	[language_description] ASC,
	[name] ASC,
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



/****** Object:  Index [ndx_uniq_tgm]    Script Date: 3/20/2018 3:51:22 PM ******/
DROP INDEX [ndx_uniq_tgm] ON [dbo].[taxonomy_geography_map]
GO

/****** Object:  Index [ndx_uniq_tgm]    Script Date: 3/20/2018 3:51:22 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tgm] ON [dbo].[taxonomy_geography_map]
(
	[taxonomy_species_id] ASC,
	[geography_id] ASC,
	[geography_status_code] ASC,
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



/****** Object:  Index [ndx_uniq_tu]    Script Date: 3/20/2018 3:52:26 PM ******/
DROP INDEX [ndx_uniq_tu] ON [dbo].[taxonomy_use]
GO

/****** Object:  Index [ndx_uniq_tu]    Script Date: 3/20/2018 3:52:26 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tu] ON [dbo].[taxonomy_use]
(
	[taxonomy_species_id] ASC,
	[economic_usage_code] ASC,
	[usage_type] ASC,
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

