/*
   Schema change to add provider_identifier column to site table
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

DECLARE @drop_sql NVARCHAR(MAX) = N'';
DECLARE @table_name nvarchar(255) = 'site';

-- drop default constraints on table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name)
	+ ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + '; '
FROM sys.default_constraints AS dc
INNER JOIN sys.tables AS ct ON dc.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE dc.parent_object_id = OBJECT_ID(@table_name)

-- drop foreign key constraints involving the table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + '; '
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE fk.parent_object_id = OBJECT_ID(@table_name) OR fk.referenced_object_id = OBJECT_ID(@table_name)

-- execute the drops
EXECUTE sp_executesql @drop_sql
GO

CREATE TABLE dbo.Tmp_site
	(
	site_id int NOT NULL IDENTITY (1, 1),
	site_short_name nvarchar(20) NOT NULL,
	site_long_name nvarchar(200) NOT NULL,
	provider_identifier nvarchar(6) NULL,
	organization_abbrev nvarchar(20) NULL,
	is_internal nvarchar(1) NOT NULL,
	is_distribution_site nvarchar(1) NOT NULL,
	type_code nvarchar(20) NULL,
	fao_institute_number nvarchar(20) NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_site SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_site TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_site TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_site TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_site TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_site TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_site ON
GO
IF EXISTS(SELECT * FROM dbo.site)
	 EXEC('INSERT INTO dbo.Tmp_site (site_id, site_short_name, site_long_name, organization_abbrev, is_internal, is_distribution_site, type_code, fao_institute_number, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT site_id, site_short_name, site_long_name, organization_abbrev, is_internal, is_distribution_site, type_code, fao_institute_number, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.site WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_site OFF
GO
DROP TABLE dbo.site
GO
EXECUTE sp_rename N'dbo.Tmp_site', N'site', 'OBJECT' 
GO
ALTER TABLE dbo.site ADD CONSTRAINT
	PK_site PRIMARY KEY CLUSTERED 
	(
	site_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_s_created ON dbo.site
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_s_modified ON dbo.site
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_s_owned ON dbo.site
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_s ON dbo.site
	(
	site_short_name,
	site_long_name,
	organization_abbrev,
	fao_institute_number
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
select Has_Perms_By_Name(N'dbo.site', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.site', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.site', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	fk_a_s1 FOREIGN KEY
	(
	backup_location1_site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	fk_a_s2 FOREIGN KEY
	(
	backup_location2_site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_s FOREIGN KEY
	(
	priority1_site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_s2 FOREIGN KEY
	(
	priority2_site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_species SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.cooperator_group ADD CONSTRAINT
	fk_cg_s FOREIGN KEY
	(
	site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.cooperator_group SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.cooperator_group', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator_group', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator_group', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.cooperator ADD CONSTRAINT
	fk_c_s FOREIGN KEY
	(
	site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.site ADD CONSTRAINT
	fk_s_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.site ADD CONSTRAINT
	fk_s_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.site ADD CONSTRAINT
	fk_s_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.cooperator SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per 