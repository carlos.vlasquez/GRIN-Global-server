/*
   Add protologue_virtual_path field to taxonomy_species table
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

DECLARE @drop_sql NVARCHAR(MAX) = N'';
DECLARE @table_name nvarchar(255) = 'taxonomy_species';

-- drop default constraints on table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name)
	+ ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + '; '
FROM sys.default_constraints AS dc
INNER JOIN sys.tables AS ct ON dc.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE dc.parent_object_id = OBJECT_ID(@table_name)

-- drop foreign key constraints involving the table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + '; '
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE fk.parent_object_id = OBJECT_ID(@table_name) OR fk.referenced_object_id = OBJECT_ID(@table_name)

-- execute the drops
EXECUTE sp_executesql @drop_sql

GO
COMMIT

select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_taxonomy_species
	(
	taxonomy_species_id int NOT NULL IDENTITY (1, 1),
	current_taxonomy_species_id int NULL,
	nomen_number int NULL,
	is_specific_hybrid nvarchar(1) NOT NULL,
	species_name nvarchar(30) NOT NULL,
	species_authority nvarchar(100) NULL,
	is_subspecific_hybrid nvarchar(1) NOT NULL,
	subspecies_name nvarchar(30) NULL,
	subspecies_authority nvarchar(100) NULL,
	is_varietal_hybrid nvarchar(1) NOT NULL,
	variety_name nvarchar(30) NULL,
	variety_authority nvarchar(100) NULL,
	is_subvarietal_hybrid nvarchar(1) NOT NULL,
	subvariety_name nvarchar(30) NULL,
	subvariety_authority nvarchar(100) NULL,
	is_forma_hybrid nvarchar(1) NOT NULL,
	forma_rank_type nvarchar(30) NULL,
	forma_name nvarchar(30) NULL,
	forma_authority nvarchar(100) NULL,
	taxonomy_genus_id int NOT NULL,
	priority1_site_id int NULL,
	priority2_site_id int NULL,
	curator1_cooperator_id int NULL,
	curator2_cooperator_id int NULL,
	restriction_code nvarchar(20) NULL,
	life_form_code nvarchar(20) NULL,
	common_fertilization_code nvarchar(20) NULL,
	is_name_pending nvarchar(1) NOT NULL,
	synonym_code nvarchar(20) NULL,
	verifier_cooperator_id int NULL,
	name_verified_date datetime2(7) NULL,
	name nvarchar(100) NOT NULL,
	name_authority nvarchar(100) NULL,
	protologue nvarchar(500) NULL,
	protologue_virtual_path nvarchar(255) NULL,
	note nvarchar(MAX) NULL,
	site_note nvarchar(MAX) NULL,
	alternate_name nvarchar(2000) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_taxonomy_species SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_taxonomy_species TO gg_user  AS dbo
GRANT INSERT ON dbo.Tmp_taxonomy_species TO gg_user  AS dbo
GRANT SELECT ON dbo.Tmp_taxonomy_species TO gg_user  AS dbo
GRANT SELECT ON dbo.Tmp_taxonomy_species TO gg_search  AS dbo
GRANT UPDATE ON dbo.Tmp_taxonomy_species TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_taxonomy_species ON
GO
IF EXISTS(SELECT * FROM dbo.taxonomy_species)
	 EXEC('INSERT INTO dbo.Tmp_taxonomy_species (taxonomy_species_id, current_taxonomy_species_id, nomen_number, is_specific_hybrid, species_name, species_authority, is_subspecific_hybrid, subspecies_name, subspecies_authority, is_varietal_hybrid, variety_name, variety_authority, is_subvarietal_hybrid, subvariety_name, subvariety_authority, is_forma_hybrid, forma_rank_type, forma_name, forma_authority, taxonomy_genus_id, priority1_site_id, priority2_site_id, curator1_cooperator_id, curator2_cooperator_id, restriction_code, life_form_code, common_fertilization_code, is_name_pending, synonym_code, verifier_cooperator_id, name_verified_date, name, name_authority, protologue, note, site_note, alternate_name, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT taxonomy_species_id, current_taxonomy_species_id, nomen_number, is_specific_hybrid, species_name, species_authority, is_subspecific_hybrid, subspecies_name, subspecies_authority, is_varietal_hybrid, variety_name, variety_authority, is_subvarietal_hybrid, subvariety_name, subvariety_authority, is_forma_hybrid, forma_rank_type, forma_name, forma_authority, taxonomy_genus_id, priority1_site_id, priority2_site_id, curator1_cooperator_id, curator2_cooperator_id, restriction_code, life_form_code, common_fertilization_code, is_name_pending, synonym_code, verifier_cooperator_id, name_verified_date, name, name_authority, protologue, note, site_note, alternate_name, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.taxonomy_species WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_taxonomy_species OFF
GO

DROP TABLE dbo.taxonomy_species
GO
EXECUTE sp_rename N'dbo.Tmp_taxonomy_species', N'taxonomy_species', 'OBJECT' 
GO

ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	PK_taxonomy_species PRIMARY KEY CLUSTERED (taxonomy_species_id) 
	WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ts_c ON dbo.taxonomy_species (verifier_cooperator_id)
	WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ts_created ON dbo.taxonomy_species
	(created_by) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ts_cur_t ON dbo.taxonomy_species
	(current_taxonomy_species_id) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ts_modified ON dbo.taxonomy_species
	(modified_by) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ts_owned ON dbo.taxonomy_species
	(owned_by) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ts_s ON dbo.taxonomy_species
	(priority1_site_id) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ts_s2 ON dbo.taxonomy_species
	(priority2_site_id) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_ts_tg ON dbo.taxonomy_species
	(taxonomy_genus_id) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_ts_name ON dbo.taxonomy_species
	(name) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_ts_nomen ON dbo.taxonomy_species
	(nomen_number) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_ts_s_name ON dbo.taxonomy_species
	(species_name) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_ts ON dbo.taxonomy_species
	(name, name_authority) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_c FOREIGN KEY
	(verifier_cooperator_id) REFERENCES dbo.cooperator
	(cooperator_id) ON UPDATE  NO ACTION  ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_created FOREIGN KEY
	(created_by) REFERENCES dbo.cooperator
	(cooperator_id) ON UPDATE  NO ACTION  ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_cur_t FOREIGN KEY
	(current_taxonomy_species_id) REFERENCES dbo.taxonomy_species
	(taxonomy_species_id) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_curator1 FOREIGN KEY
	(
	curator1_cooperator_id
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_curator2 FOREIGN KEY
	(
	curator2_cooperator_id
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_s FOREIGN KEY
	(
	priority1_site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_s2 FOREIGN KEY
	(
	priority2_site_id
	) REFERENCES dbo.site
	(
	site_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_species ADD CONSTRAINT
	fk_ts_tg FOREIGN KEY
	(
	taxonomy_genus_id
	) REFERENCES dbo.taxonomy_genus
	(
	taxonomy_genus_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_species', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_common_name ADD CONSTRAINT
	fk_tcn_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_common_name SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_common_name', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_common_name', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_common_name', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.inventory_viability_rule_map ADD CONSTRAINT
	fk_ivrm_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.inventory_viability_rule_map SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.inventory_viability_rule_map', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.inventory_viability_rule_map', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.inventory_viability_rule_map', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_attach ADD CONSTRAINT
	fk_tat_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_attach SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_attach', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_attach', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_attach', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession_inv_annotation ADD CONSTRAINT
	fk_aian_t_new FOREIGN KEY
	(
	new_taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_annotation ADD CONSTRAINT
	fk_aian_t_old FOREIGN KEY
	(
	old_taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_annotation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession_inv_annotation', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession_inv_annotation', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession_inv_annotation', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.accession ADD CONSTRAINT
	fk_a_t FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.accession', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.accession', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.accession', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_use ADD CONSTRAINT
	fk_tus_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_use SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_use', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_use', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_use', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.citation', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_noxious ADD CONSTRAINT
	fk_tn_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_noxious SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_noxious', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_noxious', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_noxious', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_geography_map ADD CONSTRAINT
	fk_tgm_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_geography_map SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_geography_map', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_geography_map', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_geography_map', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.taxonomy_crop_map ADD CONSTRAINT
	fk_tcm_ts FOREIGN KEY
	(
	taxonomy_species_id
	) REFERENCES dbo.taxonomy_species
	(
	taxonomy_species_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.taxonomy_crop_map SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.taxonomy_crop_map', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.taxonomy_crop_map', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.taxonomy_crop_map', 'Object', 'CONTROL') as Contr_Per 