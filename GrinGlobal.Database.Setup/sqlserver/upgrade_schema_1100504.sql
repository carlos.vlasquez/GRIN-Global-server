/*
   Schema change to add taxonomy_cwr table
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT

CREATE TABLE [dbo].[taxonomy_cwr](
	[taxonomy_cwr_id] [int] IDENTITY(1,1) NOT NULL,
	[taxonomy_species_id] [int] NOT NULL,
	[crop_name] [nvarchar](100) NOT NULL,
	[display_name] [nvarchar](100) NULL,
	[crop_common_name] [nvarchar](100) NULL,
	[is_crop] [nvarchar](1) NOT NULL,
	[genepool_code] [nvarchar](20) NULL,
	[is_graftstock_genepool] [nvarchar](1) NOT NULL,
	[trait_class_code] [nvarchar](20) NULL,
	[is_potential] [nvarchar](1) NOT NULL,
	[breeding_type_code] [nvarchar](20) NULL,
	[breeding_usage] [nvarchar](100) NULL,
	[ontology_trait_identifier] [nvarchar](20) NULL,
	[citation_id] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_taxonomy_cwr] PRIMARY KEY CLUSTERED 
(
	[taxonomy_cwr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[taxonomy_cwr] ADD  DEFAULT ('N') FOR [is_graftstock_genepool]
GO

ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_cit] FOREIGN KEY([citation_id])
REFERENCES [dbo].[citation] ([citation_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_cit]
GO

ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_created]
GO

ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_modified]
GO

ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_owned]
GO

ALTER TABLE [dbo].[taxonomy_cwr]  WITH CHECK ADD  CONSTRAINT [fk_tcwr_ts] FOREIGN KEY([taxonomy_species_id])
REFERENCES [dbo].[taxonomy_species] ([taxonomy_species_id])
GO

ALTER TABLE [dbo].[taxonomy_cwr] CHECK CONSTRAINT [fk_tcwr_ts]
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_tcwr] ON [dbo].[taxonomy_cwr]
(
	[taxonomy_species_id] ASC,
	[crop_name] ASC,
	[crop_common_name] ASC,
	[genepool_code] ASC,
	[is_graftstock_genepool] ASC,
	[ontology_trait_identifier] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_ts] ON [dbo].[taxonomy_cwr]
(
	[taxonomy_species_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_created] ON [dbo].[taxonomy_cwr]
(
	[created_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_modified] ON [dbo].[taxonomy_cwr]
(
	[modified_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_owned] ON [dbo].[taxonomy_cwr]
(
	[owned_by] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_tcwr_c] ON [dbo].[taxonomy_cwr]
(
	[citation_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
