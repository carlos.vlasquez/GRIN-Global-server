/*
   Schema change to add url column to literature table
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

DECLARE @drop_sql NVARCHAR(MAX) = N'';
DECLARE @table_name nvarchar(255) = 'literature';

-- drop default constraints on table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name)
	+ ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + '; '
FROM sys.default_constraints AS dc
INNER JOIN sys.tables AS ct ON dc.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE dc.parent_object_id = OBJECT_ID(@table_name)

-- drop foreign key constraints involving the table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + '; '
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE fk.parent_object_id = OBJECT_ID(@table_name) OR fk.referenced_object_id = OBJECT_ID(@table_name)

-- execute the drops
EXECUTE sp_executesql @drop_sql
GO

COMMIT
select Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.cooperator', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_literature
	(
	literature_id int NOT NULL IDENTITY (1, 1),
	abbreviation nvarchar(20) NOT NULL,
	standard_abbreviation nvarchar(2000) NULL,
	reference_title nvarchar(2000) NULL,
	editor_author_name nvarchar(2000) NULL,
	literature_type_code nvarchar(20) NULL,
	publication_year nvarchar(50) NULL,
	publisher_name nvarchar(2000) NULL,
	publisher_location nvarchar(2000) NULL,
	url nvarchar(500) NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_literature SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_literature TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_literature TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_literature TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_literature TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_literature TO gg_user  AS dbo
GO
SET IDENTITY_INSERT dbo.Tmp_literature ON
GO
IF EXISTS(SELECT * FROM dbo.literature)
	 EXEC('INSERT INTO dbo.Tmp_literature (literature_id, abbreviation, standard_abbreviation, reference_title, editor_author_name, literature_type_code, publication_year, publisher_name, publisher_location, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT literature_id, abbreviation, standard_abbreviation, reference_title, editor_author_name, literature_type_code, publication_year, publisher_name, publisher_location, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.literature WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_literature OFF
GO
DROP TABLE dbo.literature
GO
EXECUTE sp_rename N'dbo.Tmp_literature', N'literature', 'OBJECT' 
GO
ALTER TABLE dbo.literature ADD CONSTRAINT
	PK_literature PRIMARY KEY CLUSTERED 
	(
	literature_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_l_created ON dbo.literature
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_l_modified ON dbo.literature
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_l_owned ON dbo.literature
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_l ON dbo.literature
	(
	abbreviation
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.literature ADD CONSTRAINT
	fk_l_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.literature ADD CONSTRAINT
	fk_l_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.literature ADD CONSTRAINT
	fk_l_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.literature', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.literature', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.literature', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.citation ADD CONSTRAINT
	fk_ci_l FOREIGN KEY
	(
	literature_id
	) REFERENCES dbo.literature
	(
	literature_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.citation SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.citation', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.citation', 'Object', 'CONTROL') as Contr_Per 