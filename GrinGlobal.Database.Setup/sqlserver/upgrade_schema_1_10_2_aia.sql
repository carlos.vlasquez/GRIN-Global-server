/*
   Thursday, March 22, 201812:05:30 PM
   User: 
   Server: ARSMDBE3445BLD2\SQLEXPRESS
   Database: gringlobal
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

DECLARE @drop_sql NVARCHAR(MAX) = N'';
DECLARE @table_name nvarchar(255) = 'accession_inv_attach';

-- drop default constraints on table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name)
	+ ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + '; '
FROM sys.default_constraints AS dc
INNER JOIN sys.tables AS ct ON dc.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE dc.parent_object_id = OBJECT_ID(@table_name)

-- drop foreign key constraints involving the table
SELECT @drop_sql += N'ALTER TABLE ' + QUOTENAME(cs.name) + '.' + QUOTENAME(ct.name) 
    + ' DROP CONSTRAINT ' + QUOTENAME(fk.name) + '; '
FROM sys.foreign_keys AS fk
INNER JOIN sys.tables AS ct ON fk.parent_object_id = ct.object_id
INNER JOIN sys.schemas AS cs ON ct.schema_id = cs.schema_id
WHERE fk.parent_object_id = OBJECT_ID(@table_name) OR fk.referenced_object_id = OBJECT_ID(@table_name)

-- execute the drops
EXECUTE sp_executesql @drop_sql
COMMIT

BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_accession_inv_attach
	(
	accession_inv_attach_id int NOT NULL IDENTITY (1, 1),
	inventory_id int NOT NULL,
	virtual_path nvarchar(255) NOT NULL,
	thumbnail_virtual_path nvarchar(255) NULL,
	sort_order int NULL,
	title nvarchar(500) NULL,
	description nvarchar(500) NULL,
	description_code nvarchar(20) NULL,
	content_type nvarchar(100) NULL,
	category_code nvarchar(20) NOT NULL,
	copyright_information nvarchar(100) NULL,
	attach_cooperator_id int NULL,
	is_web_visible nvarchar(1) NOT NULL,
	attach_date datetime2(7) NULL,
	attach_date_code nvarchar(20) NULL,
	note nvarchar(MAX) NULL,
	created_date datetime2(7) NOT NULL,
	created_by int NOT NULL,
	modified_date datetime2(7) NULL,
	modified_by int NULL,
	owned_date datetime2(7) NOT NULL,
	owned_by int NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_accession_inv_attach SET (LOCK_ESCALATION = TABLE)
GO
GRANT DELETE ON dbo.Tmp_accession_inv_attach TO gg_user  AS dbo
GO
GRANT INSERT ON dbo.Tmp_accession_inv_attach TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_accession_inv_attach TO gg_user  AS dbo
GO
GRANT SELECT ON dbo.Tmp_accession_inv_attach TO gg_search  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_accession_inv_attach TO gg_user  AS dbo
GO
ALTER TABLE dbo.Tmp_accession_inv_attach ADD CONSTRAINT
	DF__accession__categ__060DEAE8 DEFAULT ('IMAGE') FOR category_code
GO
ALTER TABLE dbo.Tmp_accession_inv_attach ADD CONSTRAINT
	DF__accession__is_we__07020F21 DEFAULT ('Y') FOR is_web_visible
GO
SET IDENTITY_INSERT dbo.Tmp_accession_inv_attach ON
GO
IF EXISTS(SELECT * FROM dbo.accession_inv_attach)
	 EXEC('INSERT INTO dbo.Tmp_accession_inv_attach (accession_inv_attach_id, inventory_id, virtual_path, thumbnail_virtual_path, sort_order, title, description, content_type, category_code, copyright_information, attach_cooperator_id, is_web_visible, attach_date, attach_date_code, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by)
		SELECT accession_inv_attach_id, inventory_id, virtual_path, thumbnail_virtual_path, sort_order, title, description, content_type, category_code, copyright_information, attach_cooperator_id, is_web_visible, attach_date, attach_date_code, note, created_date, created_by, modified_date, modified_by, owned_date, owned_by FROM dbo.accession_inv_attach WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_accession_inv_attach OFF
GO
DROP TABLE dbo.accession_inv_attach
GO
EXECUTE sp_rename N'dbo.Tmp_accession_inv_attach', N'accession_inv_attach', 'OBJECT' 
GO
ALTER TABLE dbo.accession_inv_attach ADD CONSTRAINT
	PK_accession_inv_attach PRIMARY KEY CLUSTERED 
	(
	accession_inv_attach_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX ndx_fk_aiat_c ON dbo.accession_inv_attach
	(
	attach_cooperator_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_aiat_created ON dbo.accession_inv_attach
	(
	created_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_aiat_iid ON dbo.accession_inv_attach
	(
	inventory_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_aiat_modified ON dbo.accession_inv_attach
	(
	modified_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ndx_fk_aiat_owned ON dbo.accession_inv_attach
	(
	owned_by
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX ndx_uniq_aiat ON dbo.accession_inv_attach
	(
	inventory_id,
	virtual_path
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.accession_inv_attach ADD CONSTRAINT
	fk_aiat_i FOREIGN KEY
	(
	inventory_id
	) REFERENCES dbo.inventory
	(
	inventory_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_attach ADD CONSTRAINT
	fk_aiat_c FOREIGN KEY
	(
	attach_cooperator_id
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_attach ADD CONSTRAINT
	fk_aiat_created FOREIGN KEY
	(
	created_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_attach ADD CONSTRAINT
	fk_aiat_modified FOREIGN KEY
	(
	modified_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.accession_inv_attach ADD CONSTRAINT
	fk_aiat_owned FOREIGN KEY
	(
	owned_by
	) REFERENCES dbo.cooperator
	(
	cooperator_id
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
