BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO

/* ----- Inventory Viability ----- */

/********* Drop FK constraints in other tables ***********/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_ivd_iv]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability_data]'))
ALTER TABLE [dbo].[inventory_viability_data] DROP CONSTRAINT [fk_ivd_iv]
GO

/********* Drop FK constraints in the inventory_viability table ***********/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_iv_created]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability]'))
ALTER TABLE [dbo].[inventory_viability] DROP CONSTRAINT [fk_iv_created]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_iv_i]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability]'))
ALTER TABLE [dbo].[inventory_viability] DROP CONSTRAINT [fk_iv_i]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_iv_ivr]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability]'))
ALTER TABLE [dbo].[inventory_viability] DROP CONSTRAINT [fk_iv_ivr]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_iv_modified]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability]'))
ALTER TABLE [dbo].[inventory_viability] DROP CONSTRAINT [fk_iv_modified]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[fk_iv_owned]') AND parent_object_id = OBJECT_ID(N'[dbo].[inventory_viability]'))
ALTER TABLE [dbo].[inventory_viability] DROP CONSTRAINT [fk_iv_owned]
GO




/****************** Make a copy of the inventory_viability table's data ********************/
SELECT * INTO inventory_viability_temp FROM inventory_viability




/****** Object:  Table [dbo].[inventory_viability] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[inventory_viability]') AND type in (N'U'))
DROP TABLE [dbo].[inventory_viability]
GO


/****** Object:  Table [dbo].[inventory_viability] ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[inventory_viability](
	[inventory_viability_id] [int] IDENTITY(1,1) NOT NULL,
	[inventory_id] [int] NOT NULL,
	[inventory_viability_rule_id] [int] NULL,
	[tested_date] [datetime2](7) NOT NULL,
	[tested_date_code] [nvarchar](20) NULL,
	[percent_normal] [int] NULL,
	[percent_abnormal] [int] NULL,
	[percent_dormant] [int] NULL,
	[percent_viable] [int] NULL,
	[percent_hard] [int] NULL,
	[percent_empty] [int] NULL,
	[percent_infested] [int] NULL,
	[percent_dead] [int] NULL,
	[percent_unknown] [int] NULL,
	[vigor_rating_code] [nvarchar](20) NULL,
	[total_tested_count] [int] NULL,
	[replication_count] [int] NULL,
	[note] [nvarchar](max) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [int] NOT NULL,
	[modified_date] [datetime2](7) NULL,
	[modified_by] [int] NULL,
	[owned_date] [datetime2](7) NOT NULL,
	[owned_by] [int] NOT NULL,
 CONSTRAINT [PK_inventory_viability] PRIMARY KEY CLUSTERED 
(
	[inventory_viability_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/********** Load the data back into the inventory_viability_data table *********/
SET IDENTITY_INSERT inventory_viability ON 
GO
INSERT INTO inventory_viability
	(
	inventory_viability_id,
	inventory_id,
	inventory_viability_rule_id,
	tested_date,
	tested_date_code,
	percent_normal,
	percent_abnormal,
	percent_dormant,
	percent_viable,
	percent_hard,
	percent_empty,
	percent_infested,
	percent_dead,
	percent_unknown,
	vigor_rating_code,
	total_tested_count,
	replication_count,
	note,
	created_date,
	created_by,
	modified_date,
	modified_by,
	owned_date,
	owned_by
	)
SELECT 
	inventory_viability_id,
	inventory_id,
	inventory_viability_rule_id,
	tested_date,
	tested_date_code,
	percent_normal,
	percent_abnormal,
	percent_dormant,
	percent_viable,
    NULL,
	percent_empty,
	percent_infested,
	percent_dead,
	percent_unknown,
	vigor_rating_code,
	total_tested_count,
	replication_count,
	note,
	created_date,
	created_by,
	modified_date,
	modified_by,
	owned_date,
	owned_by
FROM inventory_viability_temp
GO
SET IDENTITY_INSERT inventory_viability OFF
GO


/********* RE-Add FK constraints to the inventory_viability table ***********/
ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_created] FOREIGN KEY([created_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_created]
GO

ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_i] FOREIGN KEY([inventory_id])
REFERENCES [dbo].[inventory] ([inventory_id])
GO

ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_i]
GO

ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_ivr] FOREIGN KEY([inventory_viability_rule_id])
REFERENCES [dbo].[inventory_viability_rule] ([inventory_viability_rule_id])
GO

ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_ivr]
GO

ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_modified] FOREIGN KEY([modified_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_modified]
GO

ALTER TABLE [dbo].[inventory_viability]  WITH CHECK ADD  CONSTRAINT [fk_iv_owned] FOREIGN KEY([owned_by])
REFERENCES [dbo].[cooperator] ([cooperator_id])
GO

ALTER TABLE [dbo].[inventory_viability] CHECK CONSTRAINT [fk_iv_owned]
GO

/********* RE-Add FK constraints in other tables ***********/
ALTER TABLE [dbo].[inventory_viability_data]  WITH CHECK ADD  CONSTRAINT [fk_ivd_iv] FOREIGN KEY([inventory_viability_id])
REFERENCES [dbo].[inventory_viability] ([inventory_viability_id])
GO

ALTER TABLE [dbo].[inventory_viability_data] CHECK CONSTRAINT [fk_ivd_iv]
GO

/********* RE-Add indexes to the inventory_viability_data table ***********/
CREATE NONCLUSTERED INDEX [ndx_fk_iv_created] ON [dbo].[inventory_viability] 
(
	[created_by] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_iv_i] ON [dbo].[inventory_viability] 
(
	[inventory_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_iv_ivr] ON [dbo].[inventory_viability] 
(
	[inventory_viability_rule_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_iv_modified] ON [dbo].[inventory_viability] 
(
	[modified_by] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [ndx_fk_iv_owned] ON [dbo].[inventory_viability] 
(
	[owned_by] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [ndx_uniq_iv] ON [dbo].[inventory_viability] 
(
	[inventory_id] ASC,
	[inventory_viability_rule_id] ASC,
	[tested_date] ASC,
	[tested_date_code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

drop table inventory_viability_temp
GO
COMMIT
