// Define a grammar for the GRIN-Global Search Engine Query

parser grammar SeQueryParser;
options { tokenVocab=SeQueryLexer; }

mixedQuery: freeExpr? mixedBool? KEY_WHERE? formattedBlock? ;

mixedBool: K_AND | K_OR ;

freeExpr
    : freeExpr K_AND K_NOT freeExpr	# freeNot
    | freeExpr K_AND freeExpr		# freeAnd
    | freeExpr K_OR freeExpr		# freeOr
    | freeExpr freeExpr+		# freeWhat
    | LPAREN freeExpr RPAREN		# freeParen
    | freeUnit				# freeSingle
    ;

freeUnit
    : DQSTRING
    | STRING
    | BARESTRING
    ;

formattedBlock: multiSelect;

multiSelect
    : multiSelect setOp multiSelect
    | lparen multiSelect rparen
    | singleSelect
    ;

singleSelect : searchCondition ; // entry - clear clauses; exit - form SQL select and save in tree ctx

searchCondition
    : K_NOT searchCondition
    | searchCondition boolOp? searchCondition
    | lparen searchCondition rparen
    | criterion
    | embeddedFreeform		// allow freeform within formatted
    ;

criterion
    : expr compOp expr
    | expr K_NOT? K_LIKE expr ( K_ESCAPE string )?
    | expr K_NOT? K_BETWEEN expr K_AND expr
    | expr K_IS K_NOT? K_NULL
    | expr K_NOT? K_IN ( parenList | subSelect )
    | expr compOp (K_ALL|K_SOME|K_ANY) subSelect
    | K_EXISTS subSelect
    ;

expr
    : field
    | literal
    | ( '+' | '-' ) expr  // unary plus or minus
    | expr ( '*' | '/' | '%' | '+' | '-' ) expr
    | function parenList
    | LPAREN expr RPAREN
    | subSelect
    | K_CASE ( K_WHEN searchCondition K_THEN expr )+ ( K_ELSE expr )? K_END
    | K_CASE expr ( K_WHEN expr K_THEN expr )+ ( K_ELSE expr )? K_END
    | expr K_COLLATE ID
    ;

parenList
    : lparen ( expr ( ',' expr )* )? rparen
    ;

subSelect: lparen selectStatement rparen ;

selectStatement
    : K_SELECT K_DISTINCT? ( K_TOP INT )? selectColumn ( ',' selectColumn )*
          fromClause (K_WHERE searchCondition)? 
          ( K_ORDER K_BY expr ( ',' expr )* ( K_ASC | K_DESC )? )?
    ;

selectColumn
    : '*'
    | ID . '*'
    | K_COUNT lparen ( '*' | ( K_ALL | K_DISTINCT )? expr ) rparen ( K_AS? ID )?
    | expr ( K_AS? ID )?
    ;

fromClause: K_FROM fromTable ( (',' fromTable )* | ( join )* ) ;

join: joinOp fromTable K_ON searchCondition;

joinOp: ( K_INNER | K_OUTER | K_LEFT | K_RIGHT )? K_JOIN;

fromTable: ID ( K_AS? ID )?;

field: (AT | ATSIGN)? (table '.')? ID ;

table: ID;

function: ( ID '.' )? ID;

embeddedFreeform
    : STRING
    | DQSTRING
    | ID
    | INT
    | FLOAT
    | BARESTRING
    ;

compOp:  '='|'!='|'<>'|'<='|'>='|'<'|'>';
boolOp:  K_AND | K_OR;
setOp:   K_UNION | K_MINUS | K_EXCEPT | K_INTERSECT;

literal: number | string ;
number:  FLOAT | INT;
string:  STRING;
lparen:  LPAREN | FREEOPEN  ;
rparen:  RPAREN | FREECLOSE ;