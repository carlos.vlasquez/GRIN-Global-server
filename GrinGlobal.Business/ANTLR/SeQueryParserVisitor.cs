//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     ANTLR Version: 4.6.6
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// Generated from SeQueryParser.g4 by ANTLR 4.6.6

// Unreachable code detected
#pragma warning disable 0162
// The variable '...' is assigned but its value is never used
#pragma warning disable 0219
// Missing XML comment for publicly visible type or member '...'
#pragma warning disable 1591
// Ambiguous reference in cref attribute
#pragma warning disable 419

using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
using IToken = Antlr4.Runtime.IToken;

/// <summary>
/// This interface defines a complete generic visitor for a parse tree produced
/// by <see cref="SeQueryParser"/>.
/// </summary>
/// <typeparam name="Result">The return type of the visit operation.</typeparam>
[System.CodeDom.Compiler.GeneratedCode("ANTLR", "4.6.6")]
[System.CLSCompliant(false)]
public interface ISeQueryParserVisitor<Result> : IParseTreeVisitor<Result> {
	/// <summary>
	/// Visit a parse tree produced by the <c>freeWhat</c>
	/// labeled alternative in <see cref="SeQueryParser.freeExpr"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFreeWhat([NotNull] SeQueryParser.FreeWhatContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>freeParen</c>
	/// labeled alternative in <see cref="SeQueryParser.freeExpr"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFreeParen([NotNull] SeQueryParser.FreeParenContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>freeSingle</c>
	/// labeled alternative in <see cref="SeQueryParser.freeExpr"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFreeSingle([NotNull] SeQueryParser.FreeSingleContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>freeNot</c>
	/// labeled alternative in <see cref="SeQueryParser.freeExpr"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFreeNot([NotNull] SeQueryParser.FreeNotContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>freeAnd</c>
	/// labeled alternative in <see cref="SeQueryParser.freeExpr"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFreeAnd([NotNull] SeQueryParser.FreeAndContext context);

	/// <summary>
	/// Visit a parse tree produced by the <c>freeOr</c>
	/// labeled alternative in <see cref="SeQueryParser.freeExpr"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFreeOr([NotNull] SeQueryParser.FreeOrContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.mixedQuery"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitMixedQuery([NotNull] SeQueryParser.MixedQueryContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.mixedBool"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitMixedBool([NotNull] SeQueryParser.MixedBoolContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.freeExpr"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFreeExpr([NotNull] SeQueryParser.FreeExprContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.freeUnit"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFreeUnit([NotNull] SeQueryParser.FreeUnitContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.formattedBlock"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFormattedBlock([NotNull] SeQueryParser.FormattedBlockContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.multiSelect"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitMultiSelect([NotNull] SeQueryParser.MultiSelectContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.singleSelect"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitSingleSelect([NotNull] SeQueryParser.SingleSelectContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.searchCondition"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitSearchCondition([NotNull] SeQueryParser.SearchConditionContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.criterion"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitCriterion([NotNull] SeQueryParser.CriterionContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.expr"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitExpr([NotNull] SeQueryParser.ExprContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.parenList"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitParenList([NotNull] SeQueryParser.ParenListContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.subSelect"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitSubSelect([NotNull] SeQueryParser.SubSelectContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.selectStatement"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitSelectStatement([NotNull] SeQueryParser.SelectStatementContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.selectColumn"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitSelectColumn([NotNull] SeQueryParser.SelectColumnContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.fromClause"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFromClause([NotNull] SeQueryParser.FromClauseContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.join"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitJoin([NotNull] SeQueryParser.JoinContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.joinOp"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitJoinOp([NotNull] SeQueryParser.JoinOpContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.fromTable"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFromTable([NotNull] SeQueryParser.FromTableContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.field"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitField([NotNull] SeQueryParser.FieldContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.table"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitTable([NotNull] SeQueryParser.TableContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.function"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitFunction([NotNull] SeQueryParser.FunctionContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.embeddedFreeform"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitEmbeddedFreeform([NotNull] SeQueryParser.EmbeddedFreeformContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.compOp"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitCompOp([NotNull] SeQueryParser.CompOpContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.boolOp"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitBoolOp([NotNull] SeQueryParser.BoolOpContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.setOp"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitSetOp([NotNull] SeQueryParser.SetOpContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.literal"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitLiteral([NotNull] SeQueryParser.LiteralContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.number"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitNumber([NotNull] SeQueryParser.NumberContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.string"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitString([NotNull] SeQueryParser.StringContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.lparen"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitLparen([NotNull] SeQueryParser.LparenContext context);

	/// <summary>
	/// Visit a parse tree produced by <see cref="SeQueryParser.rparen"/>.
	/// </summary>
	/// <param name="context">The parse tree.</param>
	/// <return>The visitor result.</return>
	Result VisitRparen([NotNull] SeQueryParser.RparenContext context);
}
