// Define a two mode token lexography for the GRIN-Global Search Engine Query

lexer grammar SeQueryLexer;

// Default "mode": freeform section - everything before the formatted criteria section

AT : '@'				-> pushMode(INSIDE);
KEY_WHERE:	[Ww][Hh][Ee][Rr][Ee]	-> pushMode(INSIDE);
KEY_AND:	[Aa][Nn][Dd]		-> type(K_AND);
KEY_OR:		[Oo][Rr]		-> type(K_OR);
KEY_NOT:	[Nn][Oo][Tt]		-> type(K_NOT);

FREEOPEN:	'('	-> type (LPAREN);
FREECLOSE:	')'	-> type (RPAREN);

String: '\'' ( ~'\'' | '\'\'' )* '\''	-> type(STRING);	// doubled ' allowed inside
DqString: '"' ( ~'"' | '""' )* '"'	-> type(DQSTRING);	// doubled " allowed inside

EOL_COMMENT: '--' ~[\r\n]* -> skip;
COMMENT: '/*' (MULTI_LINE_COMMENT|.)*? ( '*/' | EOF ) -> skip;  // nesting allowed

RAWSTRING: ~[ \t\r\n(@"'] ~[ \t\r\n)]*	-> type(BARESTRING);
WhiteSpace : [ \t\r\n]+ -> channel(HIDDEN) ; // hide spaces, tabs, newlines


// ----------------- The formatted criterion section mode ---------------------
mode INSIDE;

ATSIGN:	'@' ;
PLUS:	'+' ;
MINUS:	'-' ;
TIMES:	'*' ;
SLASH:	'/' ;
PERCENT: '%' ;
DOT:	'.' ;
COMMA:	',' ;
EQUAL:	'=' ;
LT:	'<' ;
GT:	'>' ;
LTEQ:	'<=' ;
GTEQ:	'>=' ;
NEQ:	'<>' ;
BANGEQ:	'!=' ;
LPAREN:	'(' ;
RPAREN: ')' ;

// keywords

K_ALL:		[Aa][Ll][Ll];
K_AND:		[Aa][Nn][Dd];
K_ANY:		[Aa][Nn][Yy];
K_AS:		[Aa][Ss];
K_ASC:		[Aa][Ss][Cc];
K_BETWEEN:	[Bb][Ee][Tt][Ww][Ee][Ee][Nn];
K_BY:		[Bb][Yy];
K_CASE:		[Cc][Aa][Ss][Ee];
K_COLLATE:	[Cc][Oo][Ll][Ll][Aa][Tt][Ee];
K_COUNT:	[Cc][Oo][Uu][Nn][Tt];
K_DESC:		[Dd][Ee][Ss][Cc];
K_DISTINCT:	[Dd][Ii][Ss][Tt][Ii][Nn][Cc][Tt];
K_ELSE:		[Ee][Ll][Ss][Ee];
K_END:		[Ee][Nn][Dd];
K_ESCAPE:	[Ee][Ss][Cc][Aa][Pp][Ee];
K_EXCEPT:	[Ee][Xx][Cc][Ee][Pp][Tt];
K_EXISTS:	[Ee][Xx][Ii][Ss][Tt][Ss];
K_FROM:		[Ff][Rr][Oo][Mm];
K_IN:		[Ii][Nn];
K_INTERSECT:	[Ii][Nn][Tt][Ee][Rr][Ss][Ee][Cc][Tt];
K_INNER:	[Ii][Nn][Nn][Ee][Rr];
K_IS:		[Ii][Ss];
K_JOIN:		[Jj][Oo][Ii][Nn];
K_LEFT:		[Ll][Ee][Ff][Tt];
K_LIKE:		[Ll][Ii][Kk][Ee];
K_MINUS:	[Mm][Ii][Nn][Uu][Ss];
K_NOT:		[Nn][Oo][Tt];
K_NULL:		[Nn][Uu][Ll][Ll];
K_ON:		[Oo][Nn];
K_OR:		[Oo][Rr];
K_ORDER:	[Oo][Rr][Dd][Ee][Rr];
K_OUTER:	[Oo][Uu][Tt][Ee][Rr];
K_RIGHT:	[Rr][Ii][Gg][Hh][Tt];
K_SELECT:	[Ss][Ee][Ll][Ee][Cc][Tt];
K_SOME:		[Ss][Oo][Mm][Ee];
K_THEN:		[Tt][Hh][Ee][Nn];
K_TOP:		[Tt][Oo][Pp];
K_UNION:	[Uu][Nn][Ii][Oo][Nn];
K_WHEN:		[Ww][Hh][Ee][Nn];
K_WHERE:	[Ww][Hh][Ee][Rr][Ee];

// other tokens

STRING: '\'' ( ~'\'' | '\'\'' )* '\'';	// doubled ' allowed inside
DQSTRING: '"' ( ~'"' | '""' )* '"';
FLOAT: [0-9]* '.' [0-9]+ ;
INT: [0-9]+ ;
ID : [a-zA-Z_] [a-zA-Z_0-9]* ;
SINGLE_LINE_COMMENT: '--' ~[\r\n]* -> skip;
MULTI_LINE_COMMENT: '/*' (MULTI_LINE_COMMENT|.)*? ( '*/' | EOF ) -> skip;  // nesting allowed
BARESTRING: ~[ \t\r\n(,)@."'=<>!+*/%\-]+ ;
WS : [ \t\r\n]+ -> channel(HIDDEN) ; // hide spaces, tabs, newlines