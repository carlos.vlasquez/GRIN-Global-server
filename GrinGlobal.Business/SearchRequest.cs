﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using GrinGlobal.Core; // KFE
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;

namespace GrinGlobal.Business
{
    class SearchRequest : IDisposable
    {
        SecureData _sd;
        GrinGlobal.Core.DataManager _dm;

        public SearchRequest(SecureData sd, GrinGlobal.Core.DataManager dm)
        {
            _sd = sd;
            _dm = dm;
        }

        #region IDisposable Members

        void IDisposable.Dispose() {
            //throw new NotImplementedException();
        }

        #endregion

        //
        // GG Search Engine - version 4, now with ANTLR4 parsing
        //

        public DataSet FindPKeys(string pKeyType, string searchText, string defaultOperator, string seFilter, int searchLimit) {
            DataSet searchResults = _sd.CreateReturnDataSet();
            List<int> pkeys = new List<int>();
            //Logger.LogText("FindPKeys start filter: " + seFilter + " search: \n" + searchText); // KDebug

            // Set the default boolean operation if it is empty or contains incorrect values...
            defaultOperator = defaultOperator.Trim().ToUpper();
            if (string.IsNullOrEmpty(defaultOperator) || (defaultOperator != "AND" && defaultOperator != "OR" && defaultOperator != "LIST")) {
                defaultOperator = "AND";
            }

            // get list of primary keys satisfying the search
            pkeys = FindPKeyList(pKeyType, searchText, defaultOperator);

            // Winnow by accession status flags if necessary
            /*
            if (!string.IsNullOrEmpty(seFilter) && pKeyType == "accession_id" && pkeys != null && pkeys.Count > 0) {
                //Logger.LogText("FindPKeys filter: " + seFilter); // KDebug

                string delimitedParameterList = ":" + pKeyType.Replace("_", "") + "=" + idListToString(pkeys);
                DataSet blorp = _sd.GetData("sys_accession_status", delimitedParameterList, 0, 0);
                if (blorp.Tables.Contains("sys_accession_status")) {
                    List<int> fkeys = new List<int>();
                    DataTable sasTable = blorp.Tables["sys_accession_status"];
                    DataRow[] result = sasTable.Select(seFilter + " = 'Y'");
                    foreach (DataRow row in result) {
                        //Logger.LogText("FindPKeys filter row: " + row[0]); // KDebug
                        fkeys.Add((int)row[0]);
                    }
                    pkeys = pkeys.Intersect(fkeys).ToList();
                }
            }
            */

            // convert the search results from a List<int> to a dataset...
            if (pkeys != null && pkeys.Count > -1) {
                searchResults.Tables.Add("SearchResult");
                searchResults.Tables["SearchResult"].Columns.Add("ID", typeof(int));
                int stopCount = pkeys.Count;
                if (searchLimit > 0 && searchLimit < stopCount)
                    stopCount = searchLimit;
                for (int i = 0; i < stopCount; i++) {
                    searchResults.Tables["SearchResult"].Rows.Add(pkeys[i]);
                }
            }
            //Logger.LogText("FindPKeys returning results"); // KDebug

            return searchResults;
        }


        private List<int> FindPKeyList(string pKeyType, string searchText, string defaultOperator) {
            List<int> pkeys = new List<int>();

            // Is this list processing or regular?
            if (defaultOperator == "LIST") {
                pkeys = ResolveItemList(searchText, pKeyType, "ALL");
                //Logger.LogText("FindPKeys found: " + pkeys.Count + " record on ID list search"); // KDebug

                // If no formated criteria we're done finding IDs
                searchText = ExtractFormattedFromSearch(searchText);
                if (searchText == "") return pkeys;

                // Convert ids to a formated criteria for further processing
                searchText = "@" + pKeyType.Substring(0, pKeyType.Length - 3) + "."
                    + pKeyType + " IN (" + idListToString(pkeys) + ")"
                    + "\r\n" + searchText;
                //Logger.LogText("FindPKeys non-list searchText: " + '"' + searchText + '"'); // KDebug
                defaultOperator = "AND";
            }

            pkeys = ResolveQuery(searchText, pKeyType, defaultOperator);
            //Logger.LogText("FindPKeys found: " + pkeys.Count + " records in combined search"); // KDebug

            // eliminate possible duplicates
            int before = pkeys.Count;   // KDebug
            pkeys = pkeys.Distinct().ToList();
            int after = pkeys.Count;    // KDebug
            //if (after < before) Logger.LogText("FindPKeys reduced duplicates from " + before + " to " + after); // KDebug

            return pkeys;
        }


        private string ExtractFormattedFromSearch(string searchText) {
            string nonIdText = "";
            bool endOfIds = false;

            string[] lines = searchText.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines) {
                if (endOfIds || line == "WHERE" || line.Contains("@")) {
                    nonIdText += line + '\n';
                    endOfIds = true;
                }
            }
            return nonIdText;
        }

        private List<int> ResolveQuery(string searchText, string pKeyType, string defaultOperator) {
            List<int> pkeys = new List<int>();
            
            // lets analyze the query with ANTLR parser
            AntlrInputStream stream = new AntlrInputStream(searchText);
            ITokenSource lexer = new SeQueryLexer(stream);
            ITokenStream tokens = new CommonTokenStream(lexer);
            SeQueryParser parser = new SeQueryParser(tokens);
            parser.BuildParseTree = true;
            IParseTree tree = parser.mixedQuery();
            ParseTreeWalker walker = new ParseTreeWalker(); // create standard walker

            // Use a listener to modify the formated criteria into usable SQL
            JoinMatrix sjm = new JoinMatrix(_sd, _dm);
            RewritingListener listen = new RewritingListener(_sd, _dm, sjm, tokens, pKeyType, defaultOperator);
            walker.Walk(listen, tree); // initiate walk of tree with listener

            string sql = "";
            // If a formatted block of criteria was seen, extract the rewritten SQL 
            if (listen.formattedPresent) {
                sql = listen.rewriter.GetText();

                // Dump the prepared SQL if keyword used at start
                if (searchText.ToUpper().Replace(" ", "").StartsWith("--DUMPSQL")) {
                    throw Library.CreateBusinessException(getDisplayMember("SEDumpSql", "The Search Engine prepared this SQL statement:\r\n\r\n{0}", sql));
                }

                // double check that neither a single quote or semicolon got into the SQL
                if ((sql.IndexOf("'") >= 0) || (sql.IndexOf(";") >= 0)) {
                    throw Library.CreateBusinessException(getDisplayMember("SEbadChar", "Search Engine error: found illegal char in prepared SQL.\r\n" + sql));
                }
                //Logger.LogText("ResolveQuery initial sql: \r\n" + sql); // KDebug

                // If there are no freeform words to process, execute the SQL ane return the results
                if (!listen.freeformPresent) return addSelectPKeys(_dm, pkeys, pKeyType, sql, listen.dps);
            }

            // if a "Google-like" freeform query block was seen process that seperately and combine if necessary
            if (listen.freeformPresent) {

                // Use a parse tree visitor to process freeform tokens and get results
                FreeformVisitor free = new FreeformVisitor(_sd, _dm, sjm, pKeyType, defaultOperator);
                List<int> freeKeys = free.Visit(tree);
                //Logger.LogText("ResolveQuery freeKeys.Count: " + freeKeys.Count); // KDebug
                if (freeKeys.Count < 1 || !listen.formattedPresent) return freeKeys;

                // add freeforms results to the SQL if not too many IDs, this speeds criteria checks that are too general
                if (freeKeys.Count < 1000) {
                    sql += " INTERSECT SELECT " + pKeyType + " FROM " + pKeyType.Substring(0, pKeyType.Length - 3)
                        + " WHERE " + pKeyType + " IN (" + idListToString(freeKeys) + ")";
                    //Logger.LogText("ResolveQuery extra sql: " + '"' + sql + '"'); // KDebug
                }

                //execute SQL and intersect with freeform results
                pkeys = addSelectPKeys(_dm, pkeys, pKeyType, sql, listen.dps);
                //Logger.LogText("ResolveQuery SQL pkeys.Count: " + pkeys.Count); // KDebug
                pkeys = pkeys.Intersect(freeKeys).ToList();
                //Logger.LogText("ResolveQuery both pkeys.Count: " + pkeys.Count); // KDebug

            }
            return pkeys;
        }


        // parse tree listener that rewrites the query into good SQL replacing user supplied strings with parameters

        class RewritingListener : SeQueryParserBaseListener {
            SecureData sd;
            GrinGlobal.Core.DataManager dm;
            JoinMatrix sjm; // Schema Join Matrix
            public TokenStreamRewriter rewriter;
            string pKeyType;
            string defaultOperator;
            public DataParameters dps;
            List<string> neededTables;
            public bool freeformPresent;
            public bool formattedPresent;
            public string joiningOperator;
            public string joinType;
            int subSelectLevel;

            public RewritingListener(SecureData sd, GrinGlobal.Core.DataManager dm, JoinMatrix sjm, ITokenStream tokens, string pKeyType, string defaultOperator) {
                this.sd = sd;
                this.dm = dm;
                this.sjm = sjm;
                rewriter = new TokenStreamRewriter(tokens);
                this.pKeyType = pKeyType;
                this.defaultOperator = defaultOperator;
                freeformPresent = false;
                formattedPresent = false;
                joiningOperator = "DEFAULT";
            }

            public override void ExitFreeUnit(SeQueryParser.FreeUnitContext ctx) {
                freeformPresent = true;
            }

            public override void EnterFormattedBlock(SeQueryParser.FormattedBlockContext ctx) {
                dps = new DataParameters();
                // erase everything before the formatted block
                int tokenIndex = ctx.Start.TokenIndex;
                if (tokenIndex > 0) {
                    rewriter.Delete(0, tokenIndex - 1);
                }
            }

            public override void EnterSingleSelect(SeQueryParser.SingleSelectContext ctx) {
                neededTables = new List<string>();
                subSelectLevel = 0;
                joinType = "\r\nINNER ";
            }

            public override void EnterField(SeQueryParser.FieldContext ctx) {
                // Hide optional starting @
                if (ctx.Start.Text == "@") {
                    rewriter.Delete(ctx.Start);
                }
            }

            // If we see any ORs outside subselects we sould do left joins
            public override void EnterBoolOp(SeQueryParser.BoolOpContext ctx) {
                if (subSelectLevel < 1 && ctx.GetText().ToUpper() == "OR") {
                    joinType = "\r\nLEFT ";
                }
            }

            public override void EnterSubSelect(SeQueryParser.SubSelectContext ctx) {
                subSelectLevel += 1;
            }

            public override void ExitSubSelect(SeQueryParser.SubSelectContext ctx) {
                subSelectLevel -= 1;
            }

            public override void EnterTable(SeQueryParser.TableContext ctx) {
                if (subSelectLevel < 1) {
                    string table = ctx.GetText();
                    if (!neededTables.Contains(table)) neededTables.Add(table);
                }
            }

            public override void EnterFunction(SeQueryParser.FunctionContext ctx) {
                // review use of functions
                string name = ctx.GetText().ToLower();
                if (name == "exec") {
                    rewriter.Replace(ctx.Start, "NULL");
                }
            }

            public override void EnterString(SeQueryParser.StringContext ctx) {
                // Replace literal strings with parameters to avoid SQL injection
                int pNo = dps.Count + 1;
                string pName = ":param" + pNo.ToString();
                string pText = ctx.Start.Text.TrimStart('\'').TrimEnd('\'').Replace("''", "'");
                dps.Add(new DataParameter(pName, pText, DbType.String));
                rewriter.Replace(ctx.Start, pName);
            }

            public override void EnterEmbeddedFreeform(SeQueryParser.EmbeddedFreeformContext ctx) {
                // replace freeform token with parametized SQL
                string token = ctx.GetText(); int pNo = dps.Count + 1;
                string pName = ":param" + pNo.ToString();
                DataParameter dp;
                DataParameters tokendps = new DataParameters();
                dp = new DataParameter(pName+"str", unquoteForSQL(token));
                dps.Add(dp);
                tokendps.Add(dp);

                int intToken;
                bool wasInt = false;
                if (int.TryParse(token, out intToken)) {
                    dp = new DataParameter(pName+"int", intToken);
                    dps.Add(dp);
                    tokendps.Add(dp);
                    wasInt = true;
                }

                Decimal decimalToken;
                bool wasDec = false;
                if (Decimal.TryParse(token, out decimalToken)) {
                    dp = new DataParameter(pName+"dec", decimalToken);
                    dps.Add(dp);
                    tokendps.Add(dp);
                    wasDec = true;
                }

                string oper = " = ";
                if (token.Contains("%")) oper = " LIKE ";
                int hitcnt = 0;
                string pKeyTable = pKeyType.Replace("_id", "");
                string result = pKeyTable + "." + pKeyType + " IN ( ";
                DataSet autoFields = sd.GetData("get_search_autofields", "", 0, 0);
                foreach (DataRow dr in autoFields.Tables["get_search_autofields"].Rows) {
                    string afTable = dr["table_name"].ToString().ToLower();
                    string afField = dr["field_name"].ToString().ToLower();
                    string afType = dr["field_type"].ToString().ToUpper();

                    string sqlStart = "SELECT COUNT(*) FROM " + afTable + " WHERE";
                    string whereClause = " " + afTable + "." + afField + oper;

                    if (afType == "STRING") {
                        whereClause += pName + "str";
                    } else if (afType == "INTEGER") {
                        if (!wasInt) continue;
                        whereClause += pName + "int";
                    } else if (afType == "DECIMAL") {
                        if (!wasDec) continue;
                        whereClause += pName + "dec";
                    } else {
                        continue;   // don't know how to handle field type 
                    }

                    int rowcnt = Toolkit.ToInt32(dm.ReadValue(sqlStart + whereClause, tokendps), -1);
                    if (rowcnt > 0) {
                        if (++hitcnt > 1) result += "\r\nUNION ";
                        result += "SELECT DISTINCT " + pKeyTable + "." + pKeyType;
                        string sqlFrom = sjm.generateFromClause(pKeyTable, "\r\nINNER ", new List<string> { afTable });
                        sqlFrom = parameterize(sqlFrom);
                        result += sqlFrom + "\r\n  WHERE" + whereClause;
                    }
                }

                //if (hitcnt > 1) result = " (" + result + " ) ";
                result += " ) ";
                if (hitcnt > 0) {
                    rewriter.Replace(ctx.Start, result);
                } else {
                    rewriter.Replace(ctx.Start, " 1=2 ");
                }
            }

            // replace missing boolean operator with default
            public override void ExitSearchCondition(SeQueryParser.SearchConditionContext ctx) {
                if (ctx.ChildCount == 2 && ctx.searchCondition(1) != null) {
                    rewriter.InsertAfter(ctx.searchCondition(0).Stop, "\r\n" + defaultOperator + " ");
                }
            }

            public override void ExitSingleSelect(SeQueryParser.SingleSelectContext ctx) {
                // Insert in the whole SELECT ... FROM ... WHERE bit
                string pKeyTable = pKeyType.Replace("_id", "");
                string sqlStart = "SELECT DISTINCT " + pKeyTable + "." + pKeyType; // +" FROM " + pKeyTable;

                string sqlFrom;
                if (neededTables.Count < 1) {
                    sqlFrom = " FROM " + pKeyTable;
                } else { // going to need to add some joins for the criteria tables
                    sqlFrom = sjm.generateFromClause(pKeyTable, joinType, neededTables);
                    sqlFrom = parameterize(sqlFrom);
                }
                sqlStart += sqlFrom;

                sqlStart += "\r\nWHERE\r\n";
                rewriter.InsertBefore(ctx.Start, sqlStart);
            }

            // replaces single quoted strings with parameters, does not handle escaped quotes in string
            public string parameterize(string sql) {
                int first;
                while ((first = sql.IndexOf("'")) > -1) {
                    int next = sql.Substring(first + 1).IndexOf("'") + first + 1;
                    if (first == next) return sql; // no next quote, unbalanced
                    int pNo = dps.Count + 1;
                    string pName = ":param" + pNo.ToString();
                    string quote = sql.Substring(first, next+1 - first);
                    string pText = quote.TrimStart('\'').TrimEnd('\'').Replace("''", "'");
                    sql = sql.Replace(quote, pName);
                    dps.Add(new DataParameter(pName, pText, DbType.String));
                }
                return sql;
            }

            public override void ExitFormattedBlock(SeQueryParser.FormattedBlockContext ctx) {
                formattedPresent = true;
            }

            public override void ExitMixedQuery(SeQueryParser.MixedQueryContext ctx) {
                 if (ctx.mixedBool() != null) {
                    // note boolean operator and then hide it
                    joiningOperator = ctx.mixedBool().Start.Text;
                    rewriter.Delete(ctx.mixedBool().Start);
                }
            }

            public override void VisitErrorNode(IErrorNode node) {
                string etext = node.GetText();
                throw Library.CreateBusinessException(getDisplayMember("ANTLR", "Search engine error parsing query: "+ etext));
            }
        }

        // parse tree visitor to evaluate the results of the freeform query section

        class FreeformVisitor : SeQueryParserBaseVisitor<List<int>> {
            JoinMatrix sjm;
            string pKeyType;
            string defaultOperator;
            SecureData sd;
            GrinGlobal.Core.DataManager dm;

            public FreeformVisitor(SecureData sd, GrinGlobal.Core.DataManager dm, JoinMatrix sjm, string pKeyType, string defaultOperator) {
                this.sd = sd;
                this.dm = dm;
                this.sjm = sjm;
                this.pKeyType = pKeyType;
                this.defaultOperator = defaultOperator.ToUpper();
             }


            public override List<int> VisitMixedQuery(SeQueryParser.MixedQueryContext ctx) {
                if (ctx.freeExpr() == null) {
                    return new List<int>();
                } else {
                    return Visit(ctx.freeExpr());
                }
            }

            //public override List<int> VisitFreeformBlock(SeQueryParser.FreeformBlockContext ctx) {
            //    return Visit(ctx.freeExpr());
            //}

            public override List<int> VisitFreeAnd(SeQueryParser.FreeAndContext ctx) {
                List<int> list1 = Visit(ctx.freeExpr(0));
                List<int> list2 = Visit(ctx.freeExpr(1));
                return list2.Intersect(list1).ToList();
            }

            public override List<int> VisitFreeOr(SeQueryParser.FreeOrContext ctx) {
                List<int> list1 = Visit(ctx.freeExpr(0));
                List<int> list2 = Visit(ctx.freeExpr(1));
                return list2.Union(list1).ToList();
            }

            public override List<int> VisitFreeNot(SeQueryParser.FreeNotContext ctx) {
                List<int> list1 = Visit(ctx.freeExpr(0));
                List<int> list2 = Visit(ctx.freeExpr(1));
                return list1.Except(list2).ToList();
            }

            public override List<int> VisitFreeWhat(SeQueryParser.FreeWhatContext ctx) {
                // try shortcut of putting consectutive free tokens together and testing for id match
                string composite = ctx.freeExpr(0).GetText();
                IParseTree subtree = ctx.GetChild(1);
                while (subtree.ChildCount > 1) {
                    composite += " " + subtree.GetChild(0).GetText();
                    subtree = subtree.GetChild(1);
                }
                composite += " " + subtree.GetText();
                //Logger.LogText("VisitFreeWhat composite: " + composite);
                List<int> pkeys = ResolveItemList(sd, dm, sjm, composite, pKeyType, "ID");
                if (pkeys.Count > 0) return pkeys;

                // otherwise process the tokens seperately
                List<int> list1 = Visit(ctx.freeExpr(0));
                List<int> list2 = Visit(ctx.freeExpr(1));
                if (defaultOperator == "OR") {
                    return list2.Union(list1).ToList();
                } else {
                    return list2.Intersect(list1).ToList();
                }
            }

            public override List<int> VisitFreeParen(SeQueryParser.FreeParenContext ctx) {
                return Visit(ctx.freeExpr());
            }

            public override List<int> VisitFreeUnit(SeQueryParser.FreeUnitContext ctx) {
                int value = ctx.GetText().Length;
                List<int> manyi = new List<int>();
                manyi.Add(value);
                //return manyi;
                return ResolveTokenByAutoFields(sd, dm, pKeyType, ctx.GetText(), sjm);
            }
        }


        private static List<int> ResolveTokenByAutoFields(SecureData sd, GrinGlobal.Core.DataManager dm, string pKeyType, string rawToken, JoinMatrix sjm) {
            List<int> resultPKeys = new List<int>();
            string pKeyTable = pKeyType.Replace("_id", "");
            DataSet autoFields = sd.GetData("get_search_autofields", "", 0, 0);

            foreach (DataRow dr in autoFields.Tables["get_search_autofields"].Rows) {
                DataParameters dps = new DataParameters();
                string afTable = dr["table_name"].ToString().ToLower();
                string sql = "SELECT " + pKeyTable + "." + pKeyType;
                List<string> neededTables = new List<string>();
                neededTables.Add(afTable);
                sql += sjm.generateFromClause(pKeyTable, "\r\nINNER ", neededTables);


                string afType = dr["field_type"].ToString().ToUpper();
                string oper = "=";
                if (afType == "STRING") {
                    string strToken = rawToken.Replace('*', '%');
                    if (strToken.Contains("%")) oper = " LIKE ";
                    dps = new DataParameters(":token", unquoteForSQL(strToken));

                } else if (afType == "INTEGER") {
                    int integerToken = 0;
                    if (!int.TryParse(rawToken, out integerToken)) continue;
                    dps = new DataParameters(":token", integerToken);

                } else if (afType == "DECIMAL") {
                    Decimal decimalToken = 0.0M;
                    if (!Decimal.TryParse(rawToken, out decimalToken)) continue;
                    dps = new DataParameters(":token", decimalToken);

                } else if (afType == "DATETIME") {
                    DateTime datetimeToken = DateTime.MinValue;
                    if (!DateTime.TryParse(rawToken, out datetimeToken)) continue;
                    dps = new DataParameters(":token", datetimeToken);
                } else {
                    continue;
                }

                string afField = dr["field_name"].ToString().ToLower();
                sql += "\r\nWHERE " + afTable + "." + afField + oper + ":token";
                resultPKeys = addSelectPKeys(dm, resultPKeys, pKeyType, sql, dps);
            }

            // Try full text
            if (dm.DataConnectionSpec.EngineName.Trim().ToLower() != "sqlserver") return resultPKeys;
            string fisql = @"SELECT t.name
FROM sys.tables t 
INNER JOIN  sys.fulltext_indexes fi ON t.[object_id] = fi.[object_id]
JOIN sys_table st ON st.table_name = t.name";
            DataSet ftTables = new DataSet();
            ftTables = dm.Read(fisql, ftTables, "ftTables");

            if (ftTables.Tables.Count == 1 &&
                ftTables.Tables.Contains("ftTables") &&
                ftTables.Tables["ftTables"].Columns.Count == 1) {
                DataParameters dps = new DataParameters();
                dps = new DataParameters(":token", '"' + unquoteForSQL(rawToken) +'"');

                foreach (DataRow drFtTable in ftTables.Tables["ftTables"].Rows) {
                    string tableName = drFtTable["name"].ToString();
                    //Logger.LogText("ResolveTokenByAutoFields see full text index on table: "  + tableName ); // KDebug

                    string sql = "SELECT " + pKeyTable + "." + pKeyType;
                    List<string> neededTables = new List<string>();
                    neededTables.Add(tableName);
                    sql += sjm.generateFromClause(pKeyTable, "\r\nINNER ", neededTables);
                    sql += "\r\nWHERE contains (" + tableName + ".*, :token)";
                    //Logger.LogText("ResolveTokenByAutoFields FTI sql: " + sql); // KDebug
                    resultPKeys = addSelectPKeys(dm, resultPKeys, pKeyType, sql, dps);
                    //Logger.LogText("ResolveTokenByAutoFields FTI count " + resultPKeys.Count + " after token " + rawToken + " sql: " + sql); // KDebug

                }
            }


            return resultPKeys;
        }


        // executes a SQL select statement with data parameters and adds results to a list of integer primary keys
        private static List<int> addSelectPKeys(DataManager dm, List<int> pKeysList, string pKeyType, string sqlStatement, DataParameters dps) {
            DataSet searchResults = new DataSet();
            searchResults = dm.Read(sqlStatement, searchResults, "SearchResults", dps);
            if (searchResults.Tables.Count == 1 &&
                searchResults.Tables.Contains("SearchResults") &&
                searchResults.Tables["SearchResults"].Columns.Count == 1) {
                int pkey;
                foreach (DataRow drPKey in searchResults.Tables["SearchResults"].Rows) {
                    if (int.TryParse(drPKey[pKeyType].ToString(), out pkey)) {
                        pKeysList.Add(pkey);
                    }
                }
            }
            return pKeysList;
        }

        // for debug messages
        private static string getDisplayMember(string resourceName, string defaultValue, params string[] substitutes) {
            return ResourceHelper.GetDisplayMember(null, "MiddleTier", "SecureData", resourceName, null, defaultValue, substitutes);
        }

        private static string unquoteForSQL(string rawToken) {
            if (rawToken.Trim().StartsWith("\"") && rawToken.Trim().EndsWith("\"")) {
                rawToken = rawToken.Trim().TrimStart('"').TrimEnd('"').Replace("\"\"", "\"");
            } else if (rawToken.Trim().StartsWith("'") && rawToken.Trim().EndsWith("'")) {
                rawToken = rawToken.TrimStart('\'').TrimEnd('\'').Replace("''", "'");
            }
            return rawToken;
        }

        List<int> ResolveItemList(string itemList, string pKeyType, string searchType) {
            JoinMatrix sjm = new JoinMatrix(_sd, _dm);
            return ResolveItemList(_sd, _dm, sjm, itemList, pKeyType, searchType);
        }

        private static List<int> ResolveItemList(SecureData sd, GrinGlobal.Core.DataManager dm, JoinMatrix sjm, string itemList, string pKeyType, string searchType) {
            //Logger.LogText("ResolveItemList beginning itemList: " + '"' + itemList + '"'); // KDebug
            List<int> acids = new List<int>();
            List<int> ivids = new List<int>();
            List<int> pnids = new List<int>();
            List<int> orids = new List<int>();
            string sql;

            // split on and loop thru the lines
            string[] items = itemList.Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string item in items) {
                //Logger.LogText("ResolveItemList loop item: " + '"' + item + '"'); // KDebug

                int preCaseCount = acids.Count + ivids.Count + orids.Count;
                // splitting into tokens
                string[] tokens = item.Split(new char[] { '\t', ' ' }, StringSplitOptions.RemoveEmptyEntries);
                int number = 0;
                if (searchType != "PN") switch (tokens.Length) {
                    case 1:
                            // Assume this is an order_request_id
                            number = 0;
                            if (int.TryParse(tokens[0], out number) && searchType == "ALL") {
                                sql = "SELECT order_request_id FROM order_request WHERE order_request.order_request_id = :order_number";
                                orids = addSelectPKeys(dm, orids, "order_request_id", sql, new DataParameters(":order_number", number));
                            } else { // Try as a one part accession or inventory ID
                                int beforeCount = acids.Count;
                                sql = "SELECT accession_id FROM accession WHERE accession.accession_number_part1= :prefix AND accession.accession_number_part2 IS NULL";
                                acids = addSelectPKeys(dm, acids, "accession_id", sql, new DataParameters(":prefix", tokens[0]));

                                if (acids.Count == beforeCount) {
                                    sql = "SELECT inventory_id FROM inventory WHERE inventory.inventory_number_part1= :prefix AND inventory.inventory_number_part2 IS NULL";
                                    ivids = addSelectPKeys(dm, ivids, "inventory_id", sql, new DataParameters(":prefix", tokens[0]));
                                }
                            }
                            break;
                    case 2:
                        // Assume this is a xxx_part1 and xxx_part2 item...
                        number = 0;
                        if (!int.TryParse(tokens[0], out number) && int.TryParse(tokens[1], out number)) {
                            int beforeCount = acids.Count;
                            sql = "SELECT accession_id FROM accession WHERE accession.accession_number_part1= :prefix AND accession.accession_number_part2= :number";
                            acids = addSelectPKeys(dm, acids, "accession_id", sql, new DataParameters(":prefix", tokens[0], ":number", number));

                            if (acids.Count == beforeCount) {
                                sql = "SELECT inventory_id FROM inventory WHERE inventory.inventory_number_part1= :prefix AND inventory.inventory_number_part2= :number";
                                ivids = addSelectPKeys(dm, ivids, "inventory_id", sql, new DataParameters(":prefix", tokens[0], ":number", number));
                            }
                        }
                        break;
                    case 3:
                        // Assume this is a xxx_part1, xxx_part2 and xxx_part3 item...
                        number = 0;
                        if (!int.TryParse(tokens[0], out number) && int.TryParse(tokens[1], out number)) {
                            sql = "SELECT accession_id FROM accession WHERE accession.accession_number_part1= :prefix AND accession.accession_number_part2= :number AND accession.accession_number_part3= :suffix";
                            acids = addSelectPKeys(dm, acids, "accession_id", sql, new DataParameters(":prefix", tokens[0], ":number", number, ":suffix", tokens[2]));

                            sql = "SELECT inventory_id FROM inventory WHERE inventory.inventory_number_part1= :prefix AND inventory.inventory_number_part2= :number AND inventory.inventory_number_part3= :suffix";
                            ivids = addSelectPKeys(dm, ivids, "inventory_id", sql, new DataParameters(":prefix", tokens[0], ":number", number, ":suffix", tokens[2]));
                        }
                        break;
                    case 4:
                        // Assume this is a xxx_part1, xxx_part2, xxx_part3 and xxx_part4 item (which can only be an inventory)...
                        number = 0;
                        if (!int.TryParse(tokens[0], out number) && int.TryParse(tokens[1], out number)) {
                            sql = "SELECT inventory_id FROM inventory WHERE inventory.inventory_number_part1 = :prefix AND inventory.inventory_number_part2 = :number"
                                + " AND inventory.inventory_number_part3 = :suffix AND inventory.form_type_code = :form";
                            ivids = addSelectPKeys(dm, ivids, "inventory_id", sql, new DataParameters(":prefix", tokens[0], ":number", number, ":suffix", tokens[2], ":form", tokens[3]));
                        }
                        break;
                    default:
                        // Ignore this item...
                        break;
                }
                int postCaseCount = acids.Count + ivids.Count + orids.Count;
                if (postCaseCount == preCaseCount && searchType != "ID") {
                    // If nothing else worked maybe the entire line is a plant name
                    sql = "SELECT accession_inv_name_id FROM accession_inv_name WHERE plant_name = :plant_name_identifier";
                    pnids = addSelectPKeys(dm, pnids, "accession_inv_name_id", sql, new DataParameters(":plant_name_identifier", item));
                }
            }
            //Logger.LogText("ResolveItemList loop done"); // KDebug

            // Convert to resolve pKeyType
            List<int> pKeyIds = new List<int>();
            if (pnids.Count() > 0) {
                //List<int> pnpkeys = ResolveQuery("@accession_inv_name.accession_inv_name_id IN (" + idListToString(pnids) + ")", pKeyType, "", 2);
                List<int> pnpkeys = convertIdList(sd, dm, sjm, pnids, "accession_inv_name_id", pKeyType);
                pKeyIds = pKeyIds.Union(pnpkeys).ToList();
            }
            
            if (acids.Count() > 0) {
                if (pKeyType == "accession_id") {
                    pKeyIds = pKeyIds.Union(acids).ToList();
                } else {
                    //List<int> acpkeys = ResolveQuery("@accession.accession_id IN (" + idListToString(acids) + ")", pKeyType, "", 2);
                    List<int> acpkeys = convertIdList(sd, dm, sjm, acids, "accession_id", pKeyType);
                    pKeyIds = pKeyIds.Union(acpkeys).ToList();
                }
            }
            if (ivids.Count() > 0) {
                //List<int> ivpkeys = ResolveQuery("@inventory.inventory_id IN (" + idListToString(ivids) + ")", pKeyType, "", 2);
                List<int> ivpkeys = convertIdList(sd, dm, sjm, ivids, "inventory_id", pKeyType);
                pKeyIds = pKeyIds.Union(ivpkeys).ToList();
            }
            if (orids.Count() > 0) {
                //List<int> orpkeys = ResolveQuery("@order_request.order_request_id IN (" + idListToString(orids) + ")", pKeyType, "", 2);
                List<int> orpkeys = convertIdList(sd, dm, sjm, orids, "order_request_id", pKeyType);
                pKeyIds = pKeyIds.Union(orpkeys).ToList();
            }
            //Logger.LogText("ResolveItemList returning " + pKeyIds.Count); // KDebug

            return pKeyIds;
        }

        private static List<int> convertIdList(SecureData sd, GrinGlobal.Core.DataManager dm, JoinMatrix sjm, List<int> pkeys, string fromKeyType, string toKeyType ) {
            List<int> resultPKeys = new List<int>();
            DataParameters dps = new DataParameters();
            string fromKeyTable = fromKeyType.Replace("_id", "");
            string toKeyTable = toKeyType.Replace("_id", "");
            string sql = "SELECT " + toKeyTable + "." + toKeyType;
            List<string> neededTables = new List<string>();
            neededTables.Add(fromKeyTable);
            sql += sjm.generateFromClause(toKeyTable, "\r\nINNER ", neededTables);
            sql += " WHERE " + fromKeyTable + "." + fromKeyType + " IN (" + idListToString(pkeys) + ")";
            //Logger.LogText("convertIdList sql: " + sql); // KDebug
            resultPKeys = addSelectPKeys(dm, resultPKeys, toKeyType, sql, dps);
            return resultPKeys;
        }

        static string idListToString(List<int> idList) {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < idList.Count; i++) {
                builder.Append(idList[i].ToString());
                if (i != idList.Count - 1) builder.Append(",");
            }
            return builder.ToString();
        }

        
        //
        // JoinMatrix - the magic of creating FROM clauses to join multiple tables happens here
        //

        public class JoinMatrix {
            public class KJoin {
                private string FromTableName;
                private string FromFieldName;
                public string ToTableName;
                private string ToFieldName;
                private string ExtraJoinCode;
                public KJoin(string fromTableName, string fromFieldName, string toTableName, string toFieldName, string extraJoinCode) {
                    this.FromTableName = fromTableName;
                    this.FromFieldName = fromFieldName;
                    this.ToTableName = toTableName;
                    this.ToFieldName = toFieldName;
                    this.ExtraJoinCode = extraJoinCode;
                }

                public override string ToString() {
                    string returnJoin;

                    string fromField = FromTableName.Trim();

                    if (fromField.Contains(" ")) {
                        int pos = fromField.IndexOf(" ");
                        fromField = fromField.Substring(pos, fromField.Length - pos);
                    }
                    fromField += "." + FromFieldName;

                    string toField = ToTableName.Trim();
                    if (toField.Contains(" ")) {
                        int pos = toField.IndexOf(" ");
                        toField = toField.Substring(pos, toField.Length - pos);
                    }
                    toField += "." + ToFieldName;

                    returnJoin = "JOIN " + ToTableName;
                    returnJoin += " ON " + toField + " = ";
                    returnJoin += fromField;
                    if (!string.IsNullOrEmpty(ExtraJoinCode)) {
                        returnJoin += " " + ExtraJoinCode;
                    }
                    return returnJoin;
                }
            }

            private List<string> sysTableList;
            private Dictionary<string, string> alias2Full;
            private KJoin[,] joinPathArray;

            public JoinMatrix(SecureData sd, GrinGlobal.Core.DataManager dm) {
                InitJoinPathArray(sd, dm);
            }

            // adds a direct link join to the matrix and computes all the indirect links
            private void addLink(string fromTable, string toTable, string nextTable, string fromField, string nextField, string extraJoinCode) {
                int fromId = -1;
                int toId = -1;
                for (int i = 0; i < sysTableList.Count; i++) {
                    if (sysTableList[i] == fromTable) { fromId = i; }
                    if (sysTableList[i] == toTable) { toId = i; }
                }
                if (fromId < 0 || toId < 0) return;

                // Add the join to the matrix unless it already knows a path between the two tables
                if (joinPathArray[fromId, toId] != null) return;
                joinPathArray[fromId, toId] = new KJoin(fromTable, fromField, nextTable, nextField, extraJoinCode);

                // If something knows the direction to from table but not the to table add that
                for (int i = 0; i < sysTableList.Count; i++) {
                    if (joinPathArray[i, fromId] != null && joinPathArray[i, toId] == null) {
                        joinPathArray[i, toId] = joinPathArray[i, fromId];
                    }
                }

                // If we know how to get somewhere from the to but not the from fix it
                for (int i = 0; i < sysTableList.Count; i++) {
                    if (joinPathArray[toId, i] != null && joinPathArray[fromId, i] == null) {
                        joinPathArray[fromId, i] = joinPathArray[fromId, toId];
                    }
                }

                // combine info on all the ways we know to get to from whti all the plces we know to go from to
                for (int i = 0; i < sysTableList.Count; i++) {
                    if (joinPathArray[i, fromId] != null) {
                        for (int j = 0; j < sysTableList.Count; j++) {
                            if (joinPathArray[toId, j] != null) {
                                if (joinPathArray[i, j] == null) {
                                    joinPathArray[i, j] = joinPathArray[i, fromId];
                                }
                            }
                        }
                    }
                }
            }

            public string generateFromClause(string fromTable, string joinType, List<string> joinTables) {
                string sql = " FROM " + fromTable;

                // Add joins for each of the tables 
                List<string> joinedTables = new List<string>();
                joinedTables.Add(fromTable);
                foreach (string nTable in joinTables) {
                    string needed = nTable;
                    if (alias2Full.ContainsKey(needed)) { needed = alias2Full[needed]; }
                    if (!joinedTables.Contains(needed)) {
                        List<KJoin> joinPath = joinPathToTable(fromTable, needed);
                        // add each join in the path to the table unless it's already joined
                        foreach (KJoin nextJoin in joinPath) {
                            if (!joinedTables.Contains(nextJoin.ToTableName)) {
                                sql += joinType + nextJoin.ToString();
                                joinedTables.Add(nextJoin.ToTableName);
                            }
                        }
                    }
                }
                return sql;
            }

            private void InitJoinPathArray(SecureData sd, GrinGlobal.Core.DataManager dm) {

                // pull in the prefered join links from dataview (including all extra links)
                DataSet spanningTreeJoinList = sd.GetData("sys_matrix_input", "" + dm.DataConnectionSpec.EngineName.Trim().ToLower(), 0, 0);
                if (!spanningTreeJoinList.Tables.Contains("sys_matrix_input")) return;

                // Create a list of distinct tables and table aliases seen in the input
                sysTableList = new List<string>();
                alias2Full = new Dictionary<string, string>();
                foreach (DataRow dr in spanningTreeJoinList.Tables["sys_matrix_input"].Rows) {
                    string parentTable = dr["parent_table"].ToString().Trim().ToLower();
                    string childTable = dr["child_table"].ToString().Trim().ToLower();
                    if (!sysTableList.Contains(parentTable)) {
                        sysTableList.Add(parentTable);
                        if (parentTable.Contains(" ")) {
                            string alias = parentTable.Substring(parentTable.IndexOf(" ") + 1);
                            //throw Library.CreateBusinessException(getDisplayMember("hasPermission{nomask}", "DEBUG: adding alias '" + alias + "' = '" + parentTable + "'"));
                            alias2Full.Add(alias, parentTable);
                        }
                    }
                    if (!sysTableList.Contains(childTable)) {
                        sysTableList.Add(childTable);
                        if (childTable.Contains(" ")) {
                            string alias = childTable.Substring(childTable.IndexOf(" ") + 1);
                            alias2Full.Add(alias, childTable);
                        }
                    }
                }

                // create the appropriatly sized matrix to hold all the joins used to get from any table to another
                joinPathArray = new KJoin[sysTableList.Count, sysTableList.Count];

                // process each matrix input to configure the join matrix
                foreach (DataRow dr in spanningTreeJoinList.Tables["sys_matrix_input"].Rows) {
                    string childTable = dr["child_table"].ToString().Trim().ToLower();
                    string parentTable = dr["parent_table"].ToString().Trim().ToLower();
                    string childField = dr["child_field"].ToString().Trim().ToLower();
                    if (string.IsNullOrEmpty(childField)) { childField = parentTable + "_id"; }
                    string parentField = dr["parent_field"].ToString().Trim().ToLower();
                    if (string.IsNullOrEmpty(parentField)) { parentField = parentTable + "_id"; }
                    string extra = dr["extra_join_code"].ToString().Trim().ToLower();
                    string command = dr["command"].ToString().Trim().ToLower();
                    if (command == "map") {
                        addLink(parentTable, childTable, childTable, parentField, childField, extra);
                        addLink(childTable, parentTable, parentTable, childField, parentField, extra);
                    }
                    if (command == "replace") {
                        string destTable = dr["dest_table"].ToString().Trim().ToLower();
                        if (string.IsNullOrEmpty(destTable)) { destTable = parentTable; }
                        replaceStep(sysTableList, joinPathArray, childTable, destTable, parentTable, childField, parentField);
                        if (destTable == parentTable) {
                            replaceStep(sysTableList, joinPathArray, parentTable, childTable, childTable, parentField, childField);
                        }
                    }
                }
            }


            private void replaceStep(List<string> sysTableArray, KJoin[,] joinPathArray, string fromTable, string toTable, string nextTable, string fromField, string nextField) {
                int fromId = -1;
                int toId = -1;
                for (int i = 0; i < sysTableArray.Count; i++) {
                    if (sysTableArray[i] == fromTable) { fromId = i; }
                    if (sysTableArray[i] == toTable) { toId = i; }
                }
                if (fromId < 0 || toId < 0) return;
                joinPathArray[fromId, toId] = new KJoin(fromTable, fromField, nextTable, nextField, null);
            }

            private List<KJoin> joinPathToTable(string fromTable, string toTable) {
                List<KJoin> joinResult = new List<KJoin>();
                string sofar = fromTable;
                do {
                    KJoin nextStep = nextJoinInPath(sofar, toTable);
                    if (nextStep == null) break;
                    joinResult.Add(nextStep);
                    sofar = nextStep.ToTableName;
                } while (sofar != toTable);
                return joinResult;
            }

            private KJoin nextJoinInPath(string fromTable, string toTable) {
                int fromId = -1;
                int toId = -1;
                for (int i = 0; i < sysTableList.Count; i++) {
                    if (sysTableList[i] == fromTable) { fromId = i; }
                    if (sysTableList[i] == toTable) { toId = i; }
                }
                if (fromId < 0 || toId < 0) return null;
                return joinPathArray[fromId, toId];
            }
        }
    }
}
