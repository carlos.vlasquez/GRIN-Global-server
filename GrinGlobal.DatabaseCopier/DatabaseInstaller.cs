﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using Microsoft.Win32;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Windows.Forms;
using GrinGlobal.Core;
using GrinGlobal.Core.DataManagers;
using GrinGlobal.DatabaseInspector;
using System.Text;
using GrinGlobal.DatabaseInspector.PostgreSql;
using System.Data;
using GrinGlobal.InstallHelper;
using System.Net;

using System.Threading;
using System.Threading.Tasks;

namespace GrinGlobal.DatabaseCopier {
    [RunInstaller(true)]
    public partial class DatabaseInstaller : Installer {
        public DatabaseInstaller() {
            InitializeComponent();

        }

        //private void setSuperUserPassword(IDictionary savedState, string newPassword) {
        //    savedState["PASSWORD"] = Crypto.EncryptText(newPassword);
        //}

        private frmProgress _frmProgress;

        private const int SLEEP_TIME = 10;

        private object _threadSync = new object();
        private void waitForUserInput(BackgroundWorker worker, object state, int maxWaitMilliseconds) {

            // block current thread until user has responded to the input
            if (worker != null) {
                worker.ReportProgress(0, state);
                // THIS CAUSES ISSUES!  Some threading problem exists, do NOT remove timeout...
                // Toolkit.BlockThread();
                Toolkit.BlockThread(maxWaitMilliseconds);
            }



        }

        private bool _workerDone;
        private void progressDone() {
            try {
                _workerDone = true;
                if (_frmProgress != null) {
                    _frmProgress.Cursor = Cursors.Default;
                    _frmProgress.Close();
                    _frmProgress = null;
                }
            } finally {
                // make sure everything continues to process so we will eventually exit at some point
                Toolkit.UnblockBlockedThread();
            }
        }

        void c_OnProgress(object sender, ProgressEventArgs pea) {
            worker_ProgressChanged(sender, new ProgressChangedEventArgs(0, pea.Message)); 
        }


        private DialogResult promptForDatabaseLoginInfo(BackgroundWorker worker, string engine, string instanceName, bool windowsAuth, string gguacPath, ref DatabaseEngineUtil dbEngineUtil, ref string superUserPassword, bool allowSkip) {
           
            if (worker != null) {

                var args = new WorkerArguments();
                args.Properties["prompt"] = "dbEngineUtil";
                args.Properties["engine"] = engine;
                args.Properties["instanceName"] = instanceName;
                args.Properties["windowsAuth"] = windowsAuth;
                args.Properties["gguacPath"] = gguacPath;
                args.Properties["dbEngineUtil"] = dbEngineUtil;
                args.Properties["superUserPassword"] = superUserPassword;
                args.Properties["dialogresult"] = null;
                args.Properties["allowSkip"] = allowSkip;
                waitForUserInput(worker, args, int.MaxValue);  // wait forever?

                dbEngineUtil = args.Properties["dbEngineUtil"] as DatabaseEngineUtil;
                superUserPassword = args.Properties["superUserPassword"] as string;

                var dr = (DialogResult)args.Properties["dialogresult"];
                return dr;

            } else {


                var f = new frmDatabaseLoginPrompt();
                var autoLogin = Utility.GetAutoLogin(Context, null);  // enable auto login only if it's a passive install
                //MessageBox.Show("autologin: " + enableAutoLogin);
                //throw new InvalidOperationException("unconditionally bombing out");
                var dr = f.ShowDialog(
                        Toolkit.GetWindowHandle("GRIN-Global Database"), 
                        dbEngineUtil, 
                        true,
                        true,
                        false, 
                        superUserPassword, 
                        dbEngineUtil == null ? null : (bool?)dbEngineUtil.UseWindowsAuthentication,
                        autoLogin,
                        allowSkip);
                if (dr == DialogResult.OK) {
                    superUserPassword = f.txtPassword.Text;
                    dbEngineUtil = f.DatabaseEngineUtil;
                }
                return dr;
            }
        }

        public DialogResult showMessageBox(BackgroundWorker worker, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon) {
            if (worker == null) {
                try {
                    return MessageBox.Show(_frmProgress, text, caption, buttons, icon);
                } catch {
                    // if the form is disposing, showing a messagebox with it as a owner will fail.
                    return MessageBox.Show(text, caption, buttons, icon);
                }
            } else {
                var args = new WorkerArguments();
                args.Properties["prompt"] = "messagebox";
                args.Properties["text"] = text;
                args.Properties["caption"] = caption;
                args.Properties["buttons"] = buttons;
                args.Properties["icon"] = icon;
                waitForUserInput(worker, args, int.MaxValue);  // wait forever?
                object result = null;
                if (args.Properties.TryGetValue("dialogresult", out result)) {
                    return (DialogResult)result;
                } else {
                    return DialogResult.OK;
                }
            }
        }


        private BackgroundWorker initWorker() {
            var worker = new BackgroundWorker();
            _workerDone = false;
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            return worker;
        }

        private void waitForWorkerToComplete(BackgroundWorker worker) {
            // do a cheesy spin wait here to make sure gui thread displays updates to user...
            while (worker.IsBusy && !_workerDone) {
                Thread.Sleep(SLEEP_TIME);
                Application.DoEvents();
            }
        }

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            progressDone();
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            bool unblockThread = true;
            try {
                if (e.UserState is string) {
                    // background thread should not be blocked, so don't unblock it in the finally {}
                    unblockThread = false;
                    updateProgressForm(e.UserState as string, true);
                } else if (e.UserState is ApplicationException) {
                    var msg = Toolkit.Cut((e.UserState as ApplicationException).Message, 0, 7990);
                    EventLog.WriteEntry("GRIN-Global Database", msg, EventLogEntryType.Error);
                    updateProgressForm(msg, false);
                    showMessageBox(null, msg, getDisplayMember("ApplicationException", "Application Exception Occurred"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else if (e.UserState is Exception) {
                    var msg = Toolkit.Cut((e.UserState as Exception).Message, 0, 7990);
                    EventLog.WriteEntry("GRIN-Global Database", msg, EventLogEntryType.Error);
                    updateProgressForm(msg, false);
                    showMessageBox(null, msg, getDisplayMember("Exception", "Exception Occurred"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else if (e.UserState is WorkerArguments) {
                    var args = e.UserState as WorkerArguments;
                    var promptType = ("" + args.Properties["prompt"]).ToLower();
                    switch(promptType){
                        case "dbengineutil":
                            var engine = args.Properties["engine"] as string;
                            var instanceName = args.Properties["instanceName"] as string;
                            var windowsAuth = (bool)args.Properties["windowsAuth"];
                            var gguacPath = args.Properties["gguacPath"] as string;
                            var dbEngineUtil = args.Properties["dbEngineUtil"] as DatabaseEngineUtil;
                            var superUserPassword = args.Properties["superUserPassword"] as string;
                            var allowSkip = Toolkit.ToBoolean(args.Properties["allowSkip"], false);
                            args.Properties["dialogresult"] = promptForDatabaseLoginInfo(null, engine, instanceName, windowsAuth, gguacPath, ref dbEngineUtil, ref superUserPassword, allowSkip);
                            args.Properties["dbEngineUtil"] = dbEngineUtil;
                            args.Properties["superUserPassword"] = superUserPassword;
                            break;
                        case "messagebox":
                            args.Properties["dialogresult"] = showMessageBox(null, args.Properties["text"] as string, args.Properties["caption"] as string, (MessageBoxButtons)args.Properties["buttons"], (MessageBoxIcon)args.Properties["icon"]);
                            break;
                        default:
                            EventLog.WriteEntry("GRIN-Global Database", "Undefined 'prompt' type in background worker: " + promptType, EventLogEntryType.Error);
                            throw new InvalidOperationException(getDisplayMember("worker_ProgressChanged", "Undefined 'prompt' type in background worker: {0}", promptType));
                    }
                }
            } finally {
                if (unblockThread) {
                    Toolkit.UnblockBlockedThread();
                }
            }
        }

        private string _dbLogFile;
        private void updateProgressForm(string text, bool showMessageBoxOnError) {
            try {
                var msg = DateTime.Now.ToString() + " - " + text + "\r\n";
                _frmProgress.txtProgress.AppendText(msg);
                              
                //_frmProgress.txtProgress.SelectionStart = _frmProgress.txtProgress.Text.Length;
                //_frmProgress.txtProgress.ScrollToCaret();
                if (_dbLogFile == null) {
                    _dbLogFile = Toolkit.ResolveFilePath(Utility.GetTargetDirectory(Context, null, "Database") + @"\db_install_log.txt", true);
                }
                File.AppendAllText(_dbLogFile, msg);
                _frmProgress.Refresh();
                Application.DoEvents();
            } catch {
                // window may be gone by the time we get here.  Ignore it if it is since this is just informational for the user anyway
            }
            if (showMessageBoxOnError) {
                if (text.ToLower().Contains("error")) {
                    //Context.LogMessage(text);
                    EventLog.WriteEntry("GRIN-Global Database", "Error message to user: " + text, EventLogEntryType.Error);
                    MessageBox.Show(text, getDisplayMember("updateProgressForm", "Error"));
                    throw new Exception(text);
                }
            }
        }

        private void initProgressForm() {
            if (_frmProgress == null) {
                _frmProgress = new frmProgress();
                _frmProgress.btnDone.Visible = false;
                _frmProgress.Cursor = Cursors.WaitCursor;
                _frmProgress.Show();
                updateProgressForm("Initializing, please be patient...", false);
                Toolkit.ActivateApplication(_frmProgress.Handle);
                Application.DoEvents();
            }
        }

        private void initContextVariables(BackgroundWorker worker, StringBuilder sb, InstallEventArgs e, ref string engine, ref string instanceName, ref string databaseName, ref bool windowsAuth, ref DatabaseEngineUtil dbEngineUtil, ref string targetDir, ref string sourceDir, ref string gguacPath, ref string dataDestinationFolder, ref string superUserPassword, bool allowSkip) {
            // init variables
            if (engine == null) {
                engine = Utility.GetDatabaseEngine(this.Context, e.SavedState, false, "");
            }
            sb.AppendLine("engine=" + engine);
            if (instanceName == null) {
                instanceName = Utility.GetDatabaseInstanceName(this.Context, e.SavedState, "");
            }
            sb.AppendLine("instance=" + instanceName);

            if (databaseName == null)
            {
                databaseName = Utility.GetDatabaseName(this.Context, e.SavedState, "gringlobal");
            }
            sb.AppendLine("databasename=" + databaseName);

            if (targetDir == null) {
                targetDir = Utility.GetTargetDirectory(this.Context, e.SavedState, "Database");
            }
            sb.AppendLine("targetdir=" + targetDir);
            if (sourceDir == null) {
                sourceDir = Utility.GetSourceDirectory(this.Context, e.SavedState, null);
            }
            sb.AppendLine("sourcedir=" + sourceDir);
            if (gguacPath == null) {
                gguacPath = Toolkit.ResolveFilePath(targetDir + @"\gguac.exe", false);
            }
            sb.AppendLine("gguacPath=" + gguacPath);
            if (dataDestinationFolder == null) {
                dataDestinationFolder = (Utility.GetTempDirectory(1500) + @"\data").Replace(@"\\", @"\");
            }
            sb.AppendLine("data temp folder=" + dataDestinationFolder);
            if (superUserPassword == null) {
                superUserPassword = Utility.GetSuperUserPassword(Context, e.SavedState);
            }
            sb.AppendLine("superuser db password specified? " + (String.IsNullOrEmpty(superUserPassword) ? "Not yet" : "Yes"));
            windowsAuth = Utility.GetDatabaseWindowsAuth(Context, e.SavedState);
            sb.AppendLine("windowsauth=" + windowsAuth.ToString());

            var useRegSettings = Utility.GetUseRegSettings(Context, e.SavedState);
            if (dbEngineUtil == null) {
                if (!DatabaseEngineUtil.IsValidEngine(engine)) {
                    // default to sql server,  SQLExpress instance
                    engine = "sqlserver";
                //    windowsAuth = true;
                    instanceName = "MSSQLSERVER";
                }

                // prompt the user to tell us which engine and connection information they want us to use in case they want to change it
                var resp = promptForDatabaseLoginInfo(worker, engine, instanceName, windowsAuth,  gguacPath, ref dbEngineUtil, ref superUserPassword, allowSkip);
                if (resp == DialogResult.OK){
                    engine = dbEngineUtil.EngineName;
                    sb.AppendLine("engine=" + engine);
                    if (dbEngineUtil is SqlServerEngineUtil) {
                        instanceName = (dbEngineUtil as SqlServerEngineUtil).InstanceName;
                        sb.AppendLine("instanceName=" + instanceName);
                    }
                    windowsAuth = dbEngineUtil.UseWindowsAuthentication;
                    sb.AppendLine("windowsauth=" + windowsAuth);
                    //setSuperUserPassword(e.SavedState, superUserPassword);
                    sb.AppendLine("superuser db password specified? " + (String.IsNullOrEmpty(superUserPassword) ? "Not yet" : "Yes"));

                } else if (resp == DialogResult.Ignore){
                    dbEngineUtil = null;
                } else {
                    throw new InvalidOperationException(getDisplayMember("initContextVariables{usercancel}", "User cancelled out of database connection dialog"));
                }

            }

            // read resonse value stashed in registry
            databaseName = Utility.GetDatabaseName(this.Context, e.SavedState, "gringlobal");
        }

        /// <summary>
        /// inner class used for negotiating between main and background threads at start and end of background thread execution
        /// </summary>
        private class WorkerArguments {
            public Exception Exception;
            public InstallEventArgs InstallEventArgs;
            public Dictionary<string, object> Properties;
            public WorkerArguments() {
                Properties = new Dictionary<string, object>();
            }
        }

        private void DatabaseInstaller_AfterInstall(object sender, InstallEventArgs e) {

            try {
                initProgressForm();

                int installerWindowHandle = Toolkit.GetWindowHandle("GRIN-Global Database");
                if (installerWindowHandle > 0) {
                    Toolkit.MinimizeWindow(installerWindowHandle);
                }

                using (var worker = initWorker()) {
                    var wa = new WorkerArguments { InstallEventArgs = e };
                    worker.DoWork += new DoWorkEventHandler(installworker_DoWork);
                    try {
                        worker.RunWorkerAsync(wa);
                        waitForWorkerToComplete(worker);
                    } catch (Exception ex) {
                        MessageBox.Show(getDisplayMember("afterInstall{exceptionbody}", "Exception running worker async: {0}", ex.Message), getDisplayMember("afterInstall{exceptiontitle}", "Worker Launch Exception"));
                    } finally {
                        if (installerWindowHandle > 0) {
                            Toolkit.RestoreWindow(installerWindowHandle);
                        }

                        // exceptions thrown in the background worker thread won't cause us to bomb here.
                        // so the code running in background worker context needs to tell us if it encountered an exception.
                        // we rethrow it on the primary process thread here if needed.
                        if (wa.Exception != null) {
                            throw wa.Exception;
                        }
                    }
                }
            } catch (Exception ex){
                EventLog.WriteEntry("GRIN-Global Database", Toolkit.Cut("Exception in DatabaseInstaller_AfterInstall: " + ex.Message, 0, 8000), EventLogEntryType.Error);
                throw;
            } finally {
                _workerDone = true;
            }

        }

        void installworker_DoWork(object sender, DoWorkEventArgs dwea) {
            StringBuilder sb = new StringBuilder();
            var wa = dwea.Argument as WorkerArguments;
            var worker = sender as BackgroundWorker;
            var e = wa.InstallEventArgs;

            try {
                doInstall(sender as BackgroundWorker, sb, (dwea.Argument as WorkerArguments));
            } finally {
                progressDone();
            }
        }

        void doInstall(BackgroundWorker worker, StringBuilder sb, WorkerArguments wa) {

            var e = wa.InstallEventArgs;

            // showMessageBox("Running in background = " + Thread.CurrentThread.IsBackground, "Background thread?", MessageBoxButtons.OK, MessageBoxIcon.Information);

            try {

                worker.ReportProgress(0, getDisplayMember("doInstall{verifying}", "Verifying database credentials..."));

                // init vars
                string engine = null;
                string instanceName = null;
                string databaseName = null;
                bool windowsAuth = false;
                DatabaseEngineUtil dbEngineUtil = null;
                string targetDir = null;
                string sourceDir = null;
                string gguacPath = null;
                string dataDestinationFolder = null;
                string superUserPassword = null;
                //worker.ReportProgress(0, getDisplayMember("doInstall{kfe1}", "Before initContextVariables databaseName = {0}", databaseName));
                initContextVariables(worker, sb, e, ref engine, ref instanceName, ref databaseName, ref windowsAuth, ref dbEngineUtil, ref targetDir, ref sourceDir, ref gguacPath, ref dataDestinationFolder, ref superUserPassword, false);
                //worker.ReportProgress(0, getDisplayMember("doInstall{kfe2}", "After initContextVariables databaseName = {0}", databaseName));

                // pluck data source db name found after < in database name - a kludge to avoid another field on the form
                string fromDbName = null;
                int i;
                i = databaseName.IndexOf('<');
                if (i > -1) {
                    fromDbName = databaseName.Substring(i+1).TrimStart();
                    databaseName = databaseName.Substring(0, i).TrimEnd();
                }

                // prepare the x64-specific exe in case we need to use it to look up registry keys that can only be retrieved from a 64-bit process on a 64-bit OS
                worker.ReportProgress(0, getDisplayMember("doInstall{extracting}", "Extracting utilities..."));
                extractUtility64File(targetDir, gguacPath);

                // prepare to talk to the database engine

                //determineSuperUserPassword(dbEngineUtil, instanceName, ref superUserPassword);
                string connectionString = dbEngineUtil.GetDataConnectionSpec(databaseName, "__USERID__", "__PASSWORD__").ConnectionString;

                // make sure no previous data is pulled in to current data load
                Toolkit.EmptyDirectory(dataDestinationFolder);

                // extract system data that was included in the msi
                extractSystemDataFile(worker, sb, targetDir, gguacPath, dataDestinationFolder);

                // 2010-10-14 Brock Weaver brock@circaware.com
                // disabled, no longer necessary for testing...
                // prompt for additional data
                promptForAndExtractAdditionalData(worker, sb, dataDestinationFolder, gguacPath);


                // create and populate database
                worker.ReportProgress(0, getDisplayMember("doInstall{starting}", "Starting install to database engine={0}, service={1}, connection string={2} ...", dbEngineUtil.EngineName, dbEngineUtil.ServiceName, connectionString));
           
                sb.AppendLine("Creating "+databaseName+" database");
                bool doingDbUpgrade = false;
                bool ask = true;
                try {

                    createDatabase(worker, dbEngineUtil, targetDir, superUserPassword, databaseName);

                } catch (Exception exCreate) {
                    if (!exCreate.Message.ToLower().Contains("already exists. choose a different database name.")) {
                        // let exceptions other than database already exists continue on up
                        throw;
                    }

                    EventLog.WriteEntry("GRIN-Global Database", "Exception creating database: " + exCreate.Message, EventLogEntryType.Error);

                    var dr = showMessageBox(worker,
                        getDisplayMember("createDatabase{dbalreadyexistsbody}", @"Database {0} already exists. Do you want to upgrade it?

    No or Cancel will exit", databaseName),
                        getDisplayMember("createDatabase{dbalreadyexiststitle}", "Database Exists"),
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);
                    if (dr == DialogResult.Yes) {
                        doingDbUpgrade = true;

                        string question = getDisplayMember("createDatabase{autoupgradebody}", @"Do you want to automatically apply all upgrade scripts and data?

No will prompt for each step");
                        string title = getDisplayMember("createDatabase{autoupgradetitle}", "Automatic Upgrade");
                        if (showMessageBox(worker, question, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
                            ask = false;
                        } else {
                            ask = true;
                        }

                    } else {
                        // let the exception cancel the install
                        throw;
                    }
                }

                // KMK 10/6/2017  DB install bombs at times because the newly created db is not accessible
                //Added code to check if database is accessible
                string available = CheckDBAvailability(databaseName, superUserPassword, dbEngineUtil);
                if (available == null) throw new ApplicationException(getDisplayMember("createDatabase{createDatabsefailed}", "Creation of GRIN-Global database may have failed.\nPlease try running the installer again."));

                Creator c = null;
                List<TableInfo> tables = null;

                if (!doingDbUpgrade) { 

                    // normal install
                    // create tables / fill with data / create users / create indexes / create constraints
                    populateDatabase(worker, sb, dbEngineUtil, ref c, ref tables, dataDestinationFolder, targetDir, superUserPassword, databaseName, fromDbName);

                    // create sequences / triggers as needed
                    createDependentDatabaseObjects(worker, sb, dbEngineUtil, c, tables, targetDir, superUserPassword, databaseName);

                    // perform any clean up
                    finalizeDatabaseInstall(worker, sb, dbEngineUtil, c, tables, superUserPassword, databaseName, targetDir);


                } else { 
                    // upgrade time!
                    c = Creator.GetInstance(dbEngineUtil.GetDataConnectionSpec(databaseName, dbEngineUtil.SuperUserName, superUserPassword));
                    //using (var du = new DatabaseUpgrade(this, worker, sb, dbEngineUtil, c, dataDestinationFolder, targetDir, superUserPassword, databaseName, ask)) {
                    //    du.upgradeDatabaseSchemaAndSystemTables();
                    //}
                    upgradeDatabaseSchemaAndSystemTables(worker, sb, dbEngineUtil, c, dataDestinationFolder, targetDir, superUserPassword, databaseName, ask);
                }


            } catch (Exception ex) {

                try {
                    sb.AppendLine("Current Directory: " + Environment.CurrentDirectory);
                    sb.AppendLine("Current Username: " + DomainUser.Current);
                    //showMessageBox(worker, "Error during install: " + ex.Message, "Installation Failed", MessageBoxButtons.OK, MessageBoxIcon.Error); // + "\n\nHistory: " + sb.ToString(), "Exception during install", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    var msg = "Exception: " + ex.Message + "\n\nHistory: " + sb.ToString() + "\nException stack: " + ex.ToString(true);
                    EventLog.WriteEntry("GRIN-Global Database", Toolkit.Cut(msg, 0, 8000), EventLogEntryType.Error);
                } catch { 
                    // eat all errors writing to the event log
                }


                var exWrapper = new ApplicationException("Exception: " + ex.Message, ex);
                waitForUserInput(worker, exWrapper, int.MaxValue); // wait forever?
                wa.Exception = ex;
                return;
            }


        }

       
        private void DatabaseInstaller_BeforeUninstall(object sender, InstallEventArgs e) {

            try {
                initProgressForm();


                // HACK: try to give MSI form a moment to focus before we show our dialog...
                Thread.Sleep(1000);
                Application.DoEvents();
                Thread.Sleep(500);

                int installerWindowHandle = Toolkit.GetWindowHandle("GRIN-Global Database");
                if (installerWindowHandle > 0) {
                    Toolkit.MinimizeWindow(installerWindowHandle);
                }


                using (var worker = initWorker()) {
                    var wa = new WorkerArguments { InstallEventArgs = e };
                    worker.DoWork += new DoWorkEventHandler(uninstallworker_DoWork);
                    try {
                        worker.RunWorkerAsync(wa);
                        waitForWorkerToComplete(worker);
                    } finally {
                        try {
                            if (installerWindowHandle > 0) {
                                Toolkit.RestoreWindow(installerWindowHandle);
                            }
                        } catch { }
                        // exceptions thrown in the background worker thread won't cause us to bomb here.
                        // so the code running in background worker context needs to tell us if it encountered an exception.
                        // we rethrow it on the primary process thread here if needed.
                        if (wa.Exception != null) {
                            throw wa.Exception;
                        }
                    }
                }
            } catch (Exception ex) {
                EventLog.WriteEntry("GRIN-Global Database", Toolkit.Cut("Exception in DatabaseInstaller_BeforeUninstall: " + ex.Message, 0, 8000), EventLogEntryType.Error);
                throw;
            } finally {
                _workerDone = true;
            }
            
        }

        void uninstallworker_DoWork(object sender, DoWorkEventArgs dwea) {
            StringBuilder sb = new StringBuilder();
            //string superUserPassword = null;
            try {
                // uninstall previous version if needed
                // doUninstall(sender as BackgroundWorker, sb, null, true, ref superUserPassword, (dwea.Argument as WorkerArguments).InstallEventArgs);
            } catch (Exception ex) {
                // eat any errors from uninstall
            } finally {
                progressDone();
            }

        }

        private bool IsUpgrade {
            get {
                return !string.IsNullOrEmpty(this.Context.Parameters["oldproductcode"]);
            }
        }


        private void extractUtility64File(string targetDir, string gguacPath) {
            var utility64CabPath = Toolkit.ResolveFilePath(targetDir + @"\utility64.cab", false);
            var utility64ExePath = Toolkit.ResolveFilePath(targetDir + @"\ggutil64.exe", false);

            var tempPath = Utility.GetTempDirectory(15);

            if (!File.Exists(utility64ExePath)) {
                if (File.Exists(utility64CabPath)) {
                    // wipe out any existing utility64.exe file in the temp folder
                    var extracted = Toolkit.ResolveFilePath(tempPath + @"\ggutil64.exe", false);
                    if (File.Exists(extracted)) {
                        File.Delete(extracted);
                    }
                    var extracted2 = Toolkit.ResolveFilePath(tempPath + @"\ggutil64.pdb", false);
                    if (File.Exists(extracted2)) {
                        File.Delete(extracted2);
                    }
                    // extract it from our cab
                    var cabOutput = Utility.ExtractCabFile(utility64CabPath, tempPath, gguacPath);
                    // move it to the final target path (we can't do this up front because expand.exe tells us "can't expand cab file over itself" for some reason.
                    if (File.Exists(extracted)) {
                        File.Move(extracted, utility64ExePath);
                    }
                }
            }
        }

        private void finalizeDatabaseInstall(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, List<TableInfo> tables, string superUserPassword, string databaseName , string targetDir) {
            // each database engine has its own nuances after dumping a bunch of data into it...
            if (dbEngineUtil is MySqlEngineUtil) {
                // rebuild the indexes
                sb.AppendLine("Rebuilding indexes");
                worker.ReportProgress(0, getDisplayMember("finalizeDatabaseInstall{rebuilding}", "Rebuilding indexes..."));
                c.RebuildIndexes(tables);

                if ((dbEngineUtil.Version + "").Trim().StartsWith("5.")) {

                    // mysql versions < 6.0 do not support unique indexes with null columns properly.
                    // so we fudge this by creating triggers to throw errors when data violates the unique index
                    // http://www.brokenbuild.com/blog/2006/08/15/mysql-triggers-how-do-you-abort-an-insert-update-or-delete-with-a-trigger/

                    worker.ReportProgress(0, getDisplayMember("finalizeDatabaseInstall{triggers}", "Creating triggers to compensate for nullable unique indexes..."));
                    dbEngineUtil.ExecuteSqlFile(superUserPassword, databaseName, Toolkit.ResolveFilePath(targetDir + @"\mysql_create_triggers.sql", false)); 

                }

            } else if (dbEngineUtil is SqlServerEngineUtil) {

                worker.ReportProgress(0, getDisplayMember("finalizeDatabaseInstall{triggers}", "Creating database functions..."));
                try {
                    dbEngineUtil.ExecuteSqlFile(superUserPassword, databaseName, Toolkit.ResolveFilePath(targetDir + @"\sqlserver\create_functions.sql", false));
                } catch { }

            } else if (dbEngineUtil is OracleEngineUtil) {

                // Oracle doesn't need to do anything here...

            } else if (dbEngineUtil is PostgreSqlEngineUtil) {

                // Postgresql doesn't need to do anything here...
            }

        }
   

        private void populateDatabase(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, ref Creator c, ref List<TableInfo> tables, string dataDestinationFolder, string targetDir, string superUserPassword, string databaseName, string fromDbName) {

            sb.AppendLine("Getting instance of creator");
            // read schema info from the unzipped files

            if (dbEngineUtil is OracleEngineUtil) {
                // The built-in oracleconnection class in .NET uses OCI (Oracle Callable Interface) to do the actual work.
                // OCI does not allow logins to use AS SYSDBA or AS SYSOPER.
                // Since all the work we're doing will be within the gringlobal user's table space, we can login as them and do it instead.
                // (we initially create the database using the sql_plus.exe itself, which does support AS SYSDBA or AS SYSOPER)
                c = Creator.GetInstance(dbEngineUtil.GetDataConnectionSpec(databaseName, databaseName, "TempPA55"));
            } else {
                c = Creator.GetInstance(dbEngineUtil.GetDataConnectionSpec(databaseName, dbEngineUtil.SuperUserName, superUserPassword));
            }
            c.Worker = worker;
            c.OnProgress += new ProgressEventHandler(c_OnProgress);

            sb.AppendLine("Reading schema information");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{readingschema}", "Reading schema information..."));
            // __schema.xml is in the installation path...
            tables = c.LoadTableInfo(dataDestinationFolder);

            sb.AppendLine("Creating tables");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{creatingtables}", "Creating tables..."));

       //     showMessageBox(worker, c.DataConnectionSpec.ToString(), "Connection Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            c.CreateTables(tables, databaseName);
            sb.AppendLine("Loading data");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{loadingdata}", "Loading data..."));
            // actual data files are in the data destination folder created when we extracted the cab files...
            c.CopyDataToDatabase(dataDestinationFolder, databaseName, tables, false, false);

            // Copy user data from another local database if specified
            if (!string.IsNullOrEmpty(fromDbName)) {
                // decide which tables to copy
                foreach (TableInfo ti in tables) {
                    if (ti.TableName.StartsWith("sys_")) {
                        ti.IsSelected = false;
                        if (ti.TableName == "sys_user") ti.IsSelected = true;
                    } else {
                        ti.IsSelected = true;
                        if (ti.TableName.StartsWith("code_value") || ti.TableName == "app_setting" || ti.TableName == "app_resource") ti.IsSelected = false;
                        
                    }
                }
                worker.ReportProgress(0, getDisplayMember("populateDatabase{copyingdata}", "Copying data from {0}, this may take some time...", fromDbName));
                string result;
                result = c.CopyDataBetweenDatabases(databaseName, fromDbName, tables);
                var form = new frmPreview();
                form.txtPreview.Text = result;
                form.ShowDialog();
            }

            // create users / assign rights
            createDatabaseUsers(worker, sb, dbEngineUtil, c, tables, targetDir, superUserPassword, databaseName);


            //// reload table info, CopyDataToDatabase empties out the collection
            //tables = c.LoadTableInfo(dataDestinationFolder);

            sb.AppendLine("Creating indexes...");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{creatingindexes}", "Creating indexes..."));
            c.CreateIndexes(tables);


            // 2010-09-18 Brock Weaver brock@circaware.com
            // HACK: since this installer code does not support pulling in portions of a table, we need to make sure the only data that
            //       exists is what the user chose.  This problem only occurs in tables which have two FKs, each of which are in different
            //       cab files (i.e. citation_map point at both taxonomy tables and accession tables, but those are in different cabs. Putting
            //       citation_map in either cab doesn't solve the problem.)
            //       The solution we're going to use is hardcode here those certain tables that straddle cab files and remove records as needed
            //       after the data was imported and indexes created (just above) but before constraints are applied (just after this method call)
            scrubTablesThatPointAtMissingData(worker, dataDestinationFolder, databaseName, c.DataConnectionSpec);


            sb.AppendLine("Creating foreign key constraints");
            worker.ReportProgress(0, getDisplayMember("populateDatabase{creatingforeignkeys}", "Creating foreign key constraints..."));
            c.CreateConstraints(tables, true, true);
        }

        private void scrubTablesThatPointAtMissingData(BackgroundWorker worker, string dataDestinationFolder, string databaseName, DataConnectionSpec dcs){


            // delete data from tables that straddle cab files that reference data the user chose not to install.  
            // case in point: the citation_map table...
            //  taxonomy_genus <-- citation_map --> accession

            using (var dm = DataManager.Create(dcs)) {

                var files = Directory.GetFiles(dataDestinationFolder, "*.txt");


                if (!files.Contains(@"\accession.txt")){
                    // user did not install accession data. remove records that point at accession data.
                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removingaccession}", "Removing unnecessary accession citation information..."));
//                    dm.Write(@"
//delete from 
//    citation 
//where 
//    (accession_id is not null and accession_id not in (select accession_id from accession))
//    or (accession_ipr_id is not null and accession_ipr_id not in (select accession_ipr_id from accession_ipr))
//    or (accession_pedigree_id is not null and accession_pedigree_id not in (select accession_pedigree_id from accession_pedigree))
//");
                }

                if (!files.Contains(@"\genetic_marker.txt")) {
                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removinggenetic}", "Removing unnecessary genetic citation information..."));
                    dm.Write(@"delete from citation where genetic_marker_id is not null and genetic_marker_id not in (select genetic_marker_id from genetic_marker)");
                }

                if (!files.Contains(@"\method.txt")) {
                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removingmethod}", "Removing unnecessary method citation information..."));
                    dm.Write(@"delete from citation where method_id is not null and method_id not in (select method_id from method)");
                }

                if (!files.Contains(@"\order_request.txt")) {
                    // user did not install order data.  remove records that point at order data.
                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removingaccession_annotation}", "Removing unnecessary accession_inv_annotation citation information..."));
                    dm.Write(@"delete from accession_inv_annotation where order_request_id is not null and order_request_id not in (select order_request_id from order_request)");

                    worker.ReportProgress(0, getDisplayMember("scrubTablesThatPointAtMissingData{removinginventory_viability}", "Removing unnecessary inventory_viability citation information..."));
                    dm.Write(@"delete from inventory_viability_data where order_request_item_id is not null and order_request_item_id not in (select order_request_item_id from order_request_item)");

                }


            }
        }

        private void promptForAndExtractAdditionalData(BackgroundWorker worker, StringBuilder sb, string dataDestinationFolder, string gguacPath) {

            // HACK: try to give MSI form a moment to focus before we show our dialog...
            Thread.Sleep(1000);
            Application.DoEvents();
            Thread.Sleep(500);

            // we may need to prompt user to see what data they want to download from the server and copy into the database
            var fid = new frmInstallData();

            DialogResult result = DialogResult.Cancel;
            var autoIncludeOptionalData = Utility.GetParameter("optionaldata", null, this.Context, null);
            if (("" + autoIncludeOptionalData).ToUpper() == "TRUE" || ("" + autoIncludeOptionalData).ToUpper() == "1") {
                // command line tells us to include optional data, don't prompt for it
                worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{autodownloading}", "Auto-downloading optional data..."));
                sb.AppendLine("Auto-downloading optional data...");
                fid.DownloadAllFiles();
                result = DialogResult.OK;
            } else if (("" + autoIncludeOptionalData).ToUpper() == "FALSE" || ("" + autoIncludeOptionalData).ToUpper() == "0"){
                worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{autoskipping}", "Auto-skipping download of optional data..."));
                sb.AppendLine("Auto-skipping download of optional data...");
                result = DialogResult.OK;
            } else {
                worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{prompting}", "Prompting for optional data..."));
                sb.AppendLine("Prompting for optional data...");
                result = fid.ShowDialog();
            }

            if (result == DialogResult.OK) {
                if (fid.DataFiles == null || fid.DataFiles.Count == 0) {
                    worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{nonselected}", "User did not select any optional data files."));
                    sb.AppendLine("User did not select any optional data files.");
                    Application.DoEvents();
                } else {
                    var splash = new frmSplash();
                    try {
                        var extracting = getDisplayMember("promptForAndExtractAdditionalData{extracting}", "Extracting optional data files...");
                        worker.ReportProgress(0, extracting);
                        sb.AppendLine("Extracting optional data files...");
                        splash.Show(extracting, false, null);
                        foreach (var s in fid.DataFiles) {
                            sb.AppendLine("Extracting files from " + s + " to folder " + dataDestinationFolder);
                            // extract each cab file
                            //                        string fn = Path.GetFileNameWithoutExtension(s).Replace("_", " ");

                            worker.ReportProgress(0, getDisplayMember("promptForAndExtractAdditionalData{extractingfile}", "Extracting {0} to {1}...", s, dataDestinationFolder));
                            splash.ChangeText(getDisplayMember("promptForAndExtractAdditionalData{extractingfilesplash}", "Extracting {0}\n\nPlease wait while data is extracted...", s));
                            Application.DoEvents();
                            Utility.ExtractCabFile(s, dataDestinationFolder, gguacPath);

                            // we expanded the cab file, now delete it since we don't need it anymore (and want the next install to re-request data from the server and ignore the cache)
                            try {
                                var moveTo = s.Replace(@"\downloaded\", @"\installed\");
                                if (File.Exists(moveTo)) {
                                    File.Delete(moveTo);
                                }
                                File.Move(s, moveTo);
                            } catch {
                                try {
                                    // move failed, try to delete it
                                    File.Delete(s);
                                } catch {
                                    // ultimately ignore all file movement errors
                                }
                            }
                        }
                    } finally {
                        splash.Close();
                        Application.DoEvents();
                    }
                }
            } else {
                EventLog.WriteEntry("GRIN-Global Database", "User cancelled out of optional data selection dialog", EventLogEntryType.Error);
                //Context.LogMessage("User cancelled out of optional data selection dialog");
                //this.Rollback(e.SavedState);
                throw new InvalidOperationException(getDisplayMember("promptForAndExtractAdditionalData{usercancel}", "User cancelled out of optional data selection dialog."));
            }

            if (_frmProgress != null) {
                Toolkit.ActivateApplication(_frmProgress.Handle);
            }
        }

        private void extractSystemDataFile(BackgroundWorker worker, StringBuilder sb, string targetDir, string gguacPath, string dataDestinationFolder) {

            //                string helperPath = (targetDir + @"\gguac.exe").Replace(@"\\", @"\");
            worker.ReportProgress(0, getDisplayMember("extractSystemDataFile", "Extracting system data file..."));
            sb.AppendLine("Extracting system data file...");
            var sourceCab = (targetDir + @"\system_data.cab").Replace(@"\\", @"\");
            var extractOutput = Utility.ExtractCabFile(sourceCab, dataDestinationFolder, gguacPath);



            // copy schema file to data dest folder
            string tgtXml = (dataDestinationFolder + @"\__schema.xml").Replace(@"\\", @"\");
            if (File.Exists(tgtXml)) {
                File.Delete(tgtXml);
            }
            File.Copy((targetDir + @"\__schema.xml").Replace(@"\\", @"\"), tgtXml);
        }

        private void createDatabase(BackgroundWorker worker, DatabaseEngineUtil dbEngineUtil, string targetDir, string superUserPassword, string databaseName) {
            // special case:
            // this is the first time we try to login to the database after the db engine is installed.
            // if we get user login problems, prompt them whether to bail or continue after they've taken some action.

            bool done = false;
            string output = null;
            while (!done) {
                try {
                  done = true;

                    output = dbEngineUtil.CreateDatabase(superUserPassword, databaseName);

                } catch (Exception exCreate) {

                    if (exCreate.Message.ToLower().Contains("already exists. choose a different database name.")) {
                        // catch above
                        throw;
                    } else if (exCreate.Message.ToLower().Contains("local security authority cannot be contacted")) {
                        // special case: using windows login for sql server.
                        //               computer is part of a domain
                        //               computer can't authenticate with domain (i.e. laptop not VPN'd into network with the necessary domain controller)
                        // http://sqldbpool.wordpress.com/2008/09/05/%E2%80%9Ccannot-generate-sspi-context%E2%80%9D-error-message-more-comments-for-sql-server/
                        // http://blogs.msdn.com/sql_protocols/archive/2005/10/19/482782.aspx
                        EventLog.WriteEntry("GRIN-Global Database", "Exception on first pass of creating database: " + exCreate.Message, EventLogEntryType.Error);
                        var dr = showMessageBox(worker, getDisplayMember("createDatabase{windowsauthfailedbody}", "Windows login could not be authenticated.\nIf your computer is part of a Windows domain, please do either one of the following and try again:\n\nA) VPN into the network that hosts your Windows domain\nB) Disconnect from all networks\n\nAlso, you can try enabling Mixed Mode in SQL Server and logging in with the SQL Server 'sa' account.\n\nDo you want to try again?"), getDisplayMember("createDatabase{windowsauthfailedtitle}", "Could not verify login to an authority"), MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (dr == DialogResult.Yes) {
                            done = false;
                        }

                    }

                    if (done) {
                        //Context.LogMessage("Exception creating database: " + exCreate.Message);
                        EventLog.WriteEntry("GRIN-Global Database", "Exception creating database: " + exCreate.Message, EventLogEntryType.Error);
                        showMessageBox(worker, getDisplayMember("createDatabase{exceptionbody}", "Exception creating database: {0}", exCreate.Message), getDisplayMember("createDatabase{exceptiontitle}", "Failed Creating Database"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //this.Rollback(e.SavedState);
                        throw;
                    }
                }
            }

        }

        private string createDependentDatabaseObjects(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, List<TableInfo> tables, string targetDir, string superUserPassword, string databaseName) {

            if (dbEngineUtil is MySqlEngineUtil){

                // MySQL has nothing to do here
            } else if (dbEngineUtil is SqlServerEngineUtil){

                // SQL Server has nothing to do here

            } else if (dbEngineUtil is OracleEngineUtil) {

                // Oracle needs to create triggers and sequences...

                // get all the sequences up to snuff so new inserts work properly (i.e. make sure sequence id's are past the last one currently in each table)
                worker.ReportProgress(0, getDisplayMember("createDependentDatabaseObjects{sync}", "Synchronizing sequences with table data..."));

                var oraUtil = dbEngineUtil as OracleEngineUtil;

                foreach (var t in tables) {
                    worker.ReportProgress(0, getDisplayMember("createDependentDatabaseObjects{synctable}", "Synchronizing sequence for table {0}...", t.TableName));
                    var ret = oraUtil.RestartSequenceForTable(superUserPassword, databaseName, "TempPA55", t.TableName, t.PrimaryKey.Name);
                }
            } else if (dbEngineUtil is PostgreSqlEngineUtil) {

                // PostgreSQL needs to create triggers and sequences...
                
                // get all the sequences up to snuff so new inserts work properly (i.e. make sure sequence id's are past the last one currently in each table)
                worker.ReportProgress(0, getDisplayMember("createDependentDatabaseObjects{sync}", "Synchronizing sequences with table data..."));

                var pgUtil = dbEngineUtil as PostgreSqlEngineUtil;

                foreach (var t in tables) {
                    worker.ReportProgress(0, getDisplayMember("createDependentDatabaseObjects{synctable}", "Synchronizing sequence for table {0}...", t.TableName));
                    pgUtil.RestartSequenceForTable(superUserPassword, databaseName, null, t.TableName, t.PrimaryKey.Name);
                }
            }

            return "";
        }

        private string createDatabaseUsers(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, List<TableInfo> tables, string targetDir, string superUserPassword, string databaseName) {
            // execute sql to create users

            worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{create}", "Creating new gringlobal users..."));
            sb.AppendLine("Creating users");
            string output = null;
           //KMK 10/5/17  Moved the try to better handle errors
                //KMK 09/08/17  These passwords do not meet password policy
                //string userPassword = "gg_user_PA55w0rd!";
                //string searchPassword = "gg_search_PA55w0rd!";
                string userPassword = "gguPA55w0rd!!11";
                string searchPassword = "ggsPA55w0rd!!11";
                var machineName = Toolkit.Cut(Dns.GetHostName(), 0, 15);
            if (dbEngineUtil is SqlServerEngineUtil)
                {
                    worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{grant}", "Granting rights to all gringlobal tables for users..."));
                    sb.AppendLine("Granting rights");

                #region outdated code
                // KMK 04/20/17  None ofthe below will work with anything past Windows 7. 
                // SQL Server, Integrated authority -- XP
                //output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "NETWORK SERVICE", machineName, null, false, true);
                //output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "ASPNET", machineName, null, false, true);

                //////// SQL Server, Integrated authority -- Vista
                //output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "NETWORK SERVICE", "NT AUTHORITY", null, false, true);
                //    output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "SYSTEM", "NT AUTHORITY", null, false, true);

                // SQL Server, Integrated authority -- Windows 7
                //       output += dbEngineUtil.CreateUser(superUserPassword, "gringlobal", "DefaultAppPool", "IIS APPPOOL", null, false, true);
                #endregion
                try
                {
                    //     mixed mode support
                    //KMK 10/4/17  Sometimes, if the db is not uninstalled properly, it will bomb on creating users, thinking they already exists
                    //because sometimes it doesn't actually remove them before the install.  
                    //Changed the code so that if either user does exist, just ignore the error, since we need the users anyway.
                    //Did this only for SQLServer
                    output += dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_user", machineName, userPassword, false, false);
                    
                }
                catch (Exception ex)
                {
                    if (!ex.ToString().Contains("already exists"))
                    {
                        EventLog.WriteEntry("GRIN-Global Database", "Exception creating users: " + ex.Message, EventLogEntryType.Error);
                        showMessageBox(worker, getDisplayMember("createDatabaseUsers{exceptionbody}", "Exception creating users: {0}", ex.Message), getDisplayMember("createDatabaseUsers{exceptiontitle}", "Creating Users Failed"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                        throw;
                    }

                }
                try {
                    output += dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_search", machineName, searchPassword, true, false);
                }
                catch (Exception ex)
                {
                    if (!ex.ToString().Contains("already exists"))
                    {
                        EventLog.WriteEntry("GRIN-Global Database", "Exception creating users: " + ex.Message, EventLogEntryType.Error);
                        showMessageBox(worker, getDisplayMember("createDatabaseUsers{exceptionbody}", "Exception creating users: {0}", ex.Message), getDisplayMember("createDatabaseUsers{exceptiontitle}", "Creating Users Failed"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                        throw;
                    }

                }
                return output;

            }

            try { 
             if (dbEngineUtil is MySqlEngineUtil) {            
                    // MySQL 
                    worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{grant}", "Granting rights to all gringlobal tables for users..."));
                    sb.AppendLine("Granting rights");
                    output += dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_user", "%", userPassword, false, false);
                    output += dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_search", "%", searchPassword, true, false);

                } else if (dbEngineUtil is OracleEngineUtil) {


                    var res = dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_user", null, userPassword, false, false);
                    output += res;
                    //showMessageBox(worker, res, "Create user gg_user result", MessageBoxButtons.OK, MessageBoxIcon.None);


                    res = dbEngineUtil.CreateUser(superUserPassword, databaseName, "gg_search", null, searchPassword, true, false);
                    output += res;
                    //showMessageBox(worker, res, "Create user gg_search result", MessageBoxButtons.OK, MessageBoxIcon.None);


                    // also oracle requires us to enumerate each table to grant rights...
                    sb.AppendLine("Granting rights");
                    foreach (var ti in tables) {
                        worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{granttable}", "Granting rights to {0} for necessary users...", ti.TableName));
                        //dbEngineUtil.GrantRightsToTable(superUserPassword, "gringlobal", "gringlobal", null, ti.TableName, false);
                        var grtt = dbEngineUtil.GrantRightsToTable(superUserPassword, databaseName, "gg_user", null, ti.TableName, false);
                        output += grtt;
                        grtt = dbEngineUtil.GrantRightsToTable(superUserPassword, databaseName, "gg_search", null, ti.TableName, true);
                        output += grtt;
                    }
                } else if (dbEngineUtil is PostgreSqlEngineUtil) {

                    // PostgreSQL
                    output += dbEngineUtil.CreateUser(superUserPassword, null, "gg_user", "localhost", userPassword, false, false);
                    output += dbEngineUtil.CreateUser(superUserPassword, null, "gg_search", "localhost", searchPassword, true, false);

                    // postgresql requires us to enumerate each table to grant rights...
                    sb.AppendLine("Granting rights");
                    foreach (var ti in tables) {
                        worker.ReportProgress(0, getDisplayMember("createDatabaseUsers{grantrightstable}", "Granting rights to {0} for necessary users...", ti.TableName));
                        
                        dbEngineUtil.GrantRightsToTable(superUserPassword, databaseName, "gg_user", null, ti.TableName, false);
                        dbEngineUtil.GrantRightsToTable(superUserPassword, databaseName, "gg_search", null, ti.TableName, true);

                    }
                } else if (dbEngineUtil is SqliteEngineUtil){
                    // sqlite has no concept of users, just ignore...
                } else {
                    throw new NotImplementedException(getDisplayMember("createDatabaseUsers{notimplemented}", null, "createDatabaseUsers is not implemented for {0}", dbEngineUtil.EngineName));
                }

            } catch (Exception exCreateUsers) {
                //Context.LogMessage("Exception creating users: " + exCreateUsers.Message);
                EventLog.WriteEntry("GRIN-Global Database", "Exception creating users: " + exCreateUsers.Message, EventLogEntryType.Error);
                showMessageBox(worker, getDisplayMember("createDatabaseUsers{exceptionbody}", "Exception creating users: {0}", exCreateUsers.Message), getDisplayMember("createDatabaseUsers{exceptiontitle}", "Creating Users Failed"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                throw;
            }


            return output;
        }


        //KMK 10/6/17  When creating a database, it may not be available immediately, so this is done 
        //in order to make sure it can be written to
        public string CheckDBAvailability(string databaseName, string superUserPassword, DatabaseEngineUtil dbEngineUtil)
        {
            string available = null;
            if (dbEngineUtil is SqlServerEngineUtil) { 
                
            for (int i = 0; i < 10; i++)
            {
                  
                    //wait before trying, to give it time to become available
                    Thread.Sleep(1000);
                    available = dbEngineUtil.ExecuteSql(superUserPassword, null, "SELECT DATABASEPROPERTYEX('"+databaseName+"', 'Collation')");             
                    if (available != null) { break; }
                   
            }
           }
            return available;
        }


        private string getDisplayMember(string resourceName, string defaultValue, params string[] substitutes) {
            return ResourceHelper.GetDisplayMember(null, "Core", "DatabaseInstaller", resourceName, null, defaultValue, substitutes);
        }

    }
}
