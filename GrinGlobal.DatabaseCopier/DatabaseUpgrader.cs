﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Threading;
using GrinGlobal.Core;
using GrinGlobal.Core.DataManagers;
using GrinGlobal.DatabaseInspector;

namespace GrinGlobal.DatabaseCopier {
    public partial class DatabaseInstaller {

        BackgroundWorker _worker;
        StringBuilder _sb;
        DatabaseEngineUtil _dbEngineUtil;
        Creator _c;
        string _dataDestinationFolder;
        string _targetDir;
        string _superUserPassword;
        string _databaseName;
        bool _ask;

        string _fromPrefix = "temp__";
        string _toPrefix = "";
        List<TableInfo> _newTables;
        List<TableInfo> _currTables;
        DataManager _dm;
        private int _attemptCnt;
        private int _succCnt;
        private int _failCnt;
        DialogResult _dr;
        private bool? _relocate;

        public void upgradeDatabaseSchemaAndSystemTables(BackgroundWorker worker, StringBuilder sb, DatabaseEngineUtil dbEngineUtil, Creator c, string dataDestinationFolder, string targetDir, string superUserPassword, string databaseName, bool ask) {
            _worker = worker;
            _sb = sb;
            _dbEngineUtil = dbEngineUtil;
            _c = c;
            _dataDestinationFolder = dataDestinationFolder;
            _targetDir = targetDir;
            _superUserPassword = superUserPassword;
            _databaseName = databaseName;
            _ask = ask;

            // load up to date information from __schema.xml
            reportProgress(getDisplayMember("populateDatabase{readingnewschema}", "Reading new schema information..."));
            _newTables = _c.LoadTableInfo(_dataDestinationFolder);

            using (_dm = DataManager.Create(_c.DataConnectionSpec)) {
                // ugrade the database schema
                upgradeDatabaseSchema(_newTables);

                // load new entries in select system tables
                upgradeSystemTables();
            }
        }


        private void upgradeDatabaseSchema(List<TableInfo> newTables) {
            if (_dbEngineUtil is SqlServerEngineUtil) {

                reportProgress(getDisplayMember("upgradeDatabaseInstall{readexistingschema}", "Reading existing schema information..."));

                // load info on existing database
                List<string> tablesOfInterest = new List<string> {"cooperator", "app_setting", "taxonomy_genus", "taxonomy_species", "accession_source", "accession_inv_name", "accession_inv_attach"
                    ,"inventory_action", "inventory_viability", "inventory_viability_data", "inventory_viability_rule", "inventory_quality_status", "literature"};
                _currTables = _c.LoadTableInfoQuick();

                reportProgress(getDisplayMember("upgradeDatabaseInstall{schema}", "Checking for database schema upgrades..."));

                try {
                    int adminId = 48;

                    upgradeTableSchema(1090201, "accession_inv_name", "is_web_visible", null, null, @"\sqlserver\upgrade_schema_1_9_2.sql");

                    upgradeTableSchema(1090301, "inventory_viability_rule", "seeds_per_replicate"
                        , new List<string> { "seeds", "replicates", "taxonomy_species_id" }
                        , "Do you want to apply the version 1.9.3 schema updates to rename and resize a few columns"
                        , @"\sqlserver\upgrade_schema_1_9_3.sql");

                    // That script changed a couple other tables so let's remap those as well
                    var ainTi = newTables.Find(item => item.TableName == "accession_inv_name");
                    _c.CreateTableMappings(ainTi, adminId, newTables);
                    var asTi = newTables.Find(item => item.TableName == "app_setting");
                    _c.CreateTableMappings(asTi, adminId, newTables);
                    var cTi = newTables.Find(item => item.TableName == "cooperator");
                    _c.CreateTableMappings(cTi, adminId, newTables);

                    upgradeTableSchema(1090701, "inventory_action", "", new List<string> { "action_date", "action_date_code" }, null
                        , @"\sqlserver\upgrade_schema_1_9_7_ia.sql");

                    upgradeTableSchema(1090702, "taxonomy_genus", "", new List<string> { "is_hybrid" }, null, @"\sqlserver\upgrade_schema_1_9_7_ctg.sql");

                    upgradeTableSchema(1090901, "accession_source", "associated_species", null, null, @"\sqlserver\upgrade_schema_1090901.sql");

                    upgradeTableSchema(1090902, "inventory_viability", "percent_hard", null, null, @"\sqlserver\upgrade_schema_1090902.sql");

                    upgradeTableSchema(1090903, "inventory_viability_data", "hard_count", null, null, @"\sqlserver\upgrade_schema_1090903.sql");

                    upgradeTableSchema(1090904, "inventory_viability_rule", "moisture", new List<string> { "seeds", "replicates", "taxonomy_species_id" }, ""
                        , @"\sqlserver\upgrade_schema_1090904.sql");

                    upgradeTableSchema(1100101, "taxonomy_species", "protologue_virtual_path", null, null, @"\sqlserver\upgrade_schema_1_10_1.sql");

                    upgradeTableSchema(1100201, "inventory_quality_status", "negative_control", null, null, @"\sqlserver\upgrade_schema_1_10_2_iqs.sql");

                    upgradeTableSchema(1100202, "literature", "publication_year", null, null, @"\sqlserver\upgrade_schema_1_10_2_l.sql");

                    upgradeTableSchema(1100203, "accession_inv_attach", "description_code", null, null, @"\sqlserver\upgrade_schema_1_10_2_aia.sql");

                    upgradeTableSchema(1100301, "citation", "is_accepted_name", null, null, @"\sqlserver\upgrade_schema_1100301.sql");

                    upgradeTableSchema(1100302, "accession_inv_group_attach", "virtual_path", null
                        , "Do you want to add the accession_inv_group_attach table", @"\sqlserver\upgrade_schema_1100302.sql");

                    upgradeTableSchema(1100303, "method_attach", "virtual_path", null
                        , "Do you want to add the method_attach table", @"\sqlserver\upgrade_schema_1100303.sql");

                    upgradeTableSchema(1100401, "geography", "is_valid", null, null, @"\sqlserver\upgrade_schema_1100401.sql");

                    upgradeTableSchema(1100402, "accession", "doi", null, null, @"\sqlserver\upgrade_schema_1100402.sql");

                    upgradeTableSchema(1100403, "taxonomy_cwr_priority", "taxonomy_cwr_priority_id", null
                        , "Do you want to add the taxonomy_cwr_priority table", @"\sqlserver\upgrade_schema_1100403.sql");

                    upgradeTableSchema(1100404, "accession_inv_group", "method_id", null, null, @"\sqlserver\upgrade_schema_1100404.sql");

                    upgradeTableSchema(1100405, "order_request_item_action", "order_request_item_action_id", null
                        , "Do you want to add the order_request_item_action table", @"\sqlserver\upgrade_schema_1100405.sql");

                    upgradeTableSchema(1100406, "accession_inv_voucher", "ndx_uniq_aiv", null
                        , "Do you want to add the collector_voucher_number to the accession_inv_voucher unique index", @"\sqlserver\upgrade_schema_1100406.sql");

                    upgradeTableSchema(1100501, "taxonomy_common_name", "alternate_transcription", null, null, @"\sqlserver\upgrade_schema_1100501.sql");

                    upgradeTableSchema(1100502, "site", "provider_identifier", null, null, @"\sqlserver\upgrade_schema_1100502.sql");

                    upgradeTableSchema(1100503, "literature", "url", null, null, @"\sqlserver\upgrade_schema_1100503.sql");

                    upgradeTableSchema(1100504, "taxonomy_cwr", "taxonomy_cwr_id", null
                        , "Do you want to add the taxonomy_cwr table", @"\sqlserver\upgrade_schema_1100504.sql");

                    upgradeTableSchema(1100601, "vc_inventory", "inventory_id", null
                        , "Do you want to add the vc_inventory view used by the get_inventory dataview", @"\sqlserver\upgrade_schema_1100601.sql");

                    upgradeTableSchema(1100602, "taxonomy_cwr_crop", "taxonomy_cwr_crop_id", null
                        , "Do you want to add the taxonomy_cwr tables", @"\sqlserver\upgrade_schema_1100602.sql");

                    upgradeTableSchema(1100603, "taxonomy_regulation", "taxonomy_regulation_id", null
                        , "Do you want to add the taxonomy_regulation tables", @"\sqlserver\upgrade_schema_1100603.sql");

                } catch {
                    reportProgress(getDisplayMember("upgradeDatabase{schemafail}", "Failed to upgrade the database schema..."));
                    return;
                }

                reportProgress(getDisplayMember("upgradeDatabase{schemadone}", "Finished upgrading the database schema..."));
            }
        }


        public void upgradeSystemTables() {

            // make a list of system tables to load temporarily for comparison
            List<TableInfo> tempSysTables = new List<TableInfo>();
            foreach (TableInfo ti in _newTables) {
                if (ti.TableName.StartsWith("sys_")
                    || ti.TableName.StartsWith("code_")
                    || ti.TableName.StartsWith("app_")
                    || ti.TableName.StartsWith("cooperator")
                    || ti.TableName.StartsWith("geography")
                    || ti.TableName == "region"
                    || ti.TableName == "site"
                    || ti.TableName == "literature"
                    || ti.TableName == "citation"
                    || ti.TableName.StartsWith("taxonomy_")) {
                    tempSysTables.Add(ti);
                }
            }

            foreach (TableInfo ti in tempSysTables) {
                ti.TableName = _fromPrefix + ti.TableName;
                //ti.IsSelected = true;
            }

            reportProgress(getDisplayMember("populateDatabase{creatingtemptables}", "Creating temp tables..."));
            // create the temporary system tables
            _c.CreateTables(tempSysTables, _databaseName);

            reportProgress(getDisplayMember("populateDatabase{populatingtemptables}", "Populating temp tables..."));
            // load the up to date system information to the temporary tables
            _c.CopyDataToDatabase(_dataDestinationFolder, _databaseName, tempSysTables, false, false);

            // initialize cooperator local ID translation
            initUpdate();

            // copy new and updated sys_table_field_lang rows from the temporary system tables
            updateStfl();

            // copy new and updated dataviews from the temporary system tables
            updateDataview();

            // copy new and updated datatrigger rows from the temporary system tables
            updateTrigger();

            // copy new and updated datatrigger rows from the temporary system tables
            updateCode();

            // copy new and updated geography rows from the temporary system tables
            updateGeography();

            // copy new and updated taxonomy rows from the temporary system tables
            updateTaxonomy();

            updateTaxDetail();

            reportProgress(getDisplayMember("populateDatabase{populatingtemptables}", "Done Updating the {0} database...", _databaseName));
            Thread.Sleep(2000);

            DialogResult dr = DialogResult.Yes;
            if (_ask) dr = showMessageBox(_worker,
                getDisplayMember("createDatabase{dbdeltempbody}", "Do you want to delete the temp tables used to upgrade?", _databaseName),
                getDisplayMember("createDatabase{dbdeltemptitle}", "Clean Temp Tables"),
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes) {
                _c.DropTables(tempSysTables);
            }
        }


        private void compareFieldsInTable(List<TableInfo> currTables, List<TableInfo> newTables, string tableName, out List<string> newFields, out List<string> missFields, out List<string> diffFields, out List<string> sameFields) {
            newFields = new List<string>();
            missFields = new List<string>();
            diffFields = new List<string>();
            sameFields = new List<string>();

            var currTi = currTables.Find(item => item.TableName == tableName);
            var newTi = newTables.Find(item => item.TableName == tableName);
            if (currTi == null || newTi == null) return;

            foreach (FieldInfo newFi in newTi.Fields) {
                var currFi = currTi.Fields.Find(item => item.Name == newFi.Name);
                if (currFi == null) {
                    newFields.Add(newFi.Name);
                } else if (newFi.DataType == currFi.DataType && newFi.MaxLength == currFi.MaxLength && newFi.IsNullable == currFi.IsNullable) {
                    sameFields.Add(newFi.Name);
                } else {
                    diffFields.Add(newFi.Name);
                }
            }

            foreach (FieldInfo currFi in currTi.Fields) {
                var newFi = newTi.Fields.Find(item => item.Name == currFi.Name);
                if (newFi == null) {
                    missFields.Add(currFi.Name);
                }
            }
        }


        private void upgradeTableSchema(int migration, string tableName, string newField, List<string> ignoreFields, string question, string ScriptName) {
            string warning = "";
            List<string> newFields = new List<string>();
            List<string> missFields = new List<string>();
            List<string> diffFields = new List<string>();
            List<string> sameFields = new List<string>();

            // Compare the fields in the current and up-to-date versions of the table. Generate a warning if upgrade would lose fields.
            compareFieldsInTable(_currTables, _newTables, tableName, out newFields, out missFields, out diffFields, out sameFields);

            if (ignoreFields != null) {
                foreach (string ignore in ignoreFields) {
                    if (missFields.Contains(ignore)) missFields.RemoveAt(missFields.IndexOf(ignore));
                }
            }

            if (missFields.Count() == 1) {
                warning += "Your " + tableName + " table has a custom field: " + missFields[0] + "\r\n";
                warning += "\r\nWARNING - If you upgrade you will lose that field!\r\n\r\n";
            } else if (missFields.Count() > 1) {
                warning += "Your " + tableName + " table has custom fields: " + string.Join(", ", missFields.ToArray()) + "\r\n";
                warning += "\r\nWARNING - If you upgrade you will lose those fields!\r\n\r\n";
            }

            // No need to upgrade if field isn't new to existing database
            if ((!string.IsNullOrEmpty(newField) && sameFields.Contains(newField))
                || (string.IsNullOrEmpty(newField) && newFields.Count() <= 0)) {
                reportProgress(getDisplayMember("upgradetableschema{noneed}", "No need to upgrade table {0} for migration {1}...", tableName, migration.ToString()));
                return;
            }

            if (_ask || !string.IsNullOrEmpty(warning)) {
                if (string.IsNullOrEmpty(question)) question = "Do you want to update the " + tableName + " table using migration " + migration.ToString();
                string message = getDisplayMember("upgradetableschema{body}", "{0}?", warning + question);
                string title = getDisplayMember("upgradetableschema{title}", "Upgrade Table {0}", tableName);
                if (showMessageBox(_worker, message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) {
                    reportProgress(getDisplayMember("upgradetableschema{skipping}", "Skipping Upgrade of table {0} for migration {1}...", tableName, migration.ToString()));
                    return;
                }
            }

            reportProgress(getDisplayMember("upgradetableschema{upgrading}", "Upgrading database schema of table {0} for migration {1}...", tableName, migration.ToString()));
            _dbEngineUtil.ExecuteSqlFile(_superUserPassword, _databaseName, Toolkit.ResolveFilePath(_targetDir + ScriptName, false));

            // note upgrade in sys_database_migration
            string sql = @"INSERT INTO sys_database_migration
(migration_number, sort_order, action_type, action_up, created_date, created_by, owned_date, owned_by)
VALUES (:migno, (select COALESCE(max(sort_order), -1) + 1 FROM sys_database_migration WHERE migration_number = :migno)
,'SQL file', :file, GETUTCDATE(), 1, GETUTCDATE(), 1)";
            _dm.Write(sql, new DataParameters(":migno", migration, ":file", ScriptName));

            sql = @"UPDATE sys_database SET migration_number = :migno, modified_date = GETUTCDATE() WHERE sys_database_id = (SELECT MAX(sys_database_id) FROM sys_database)";
            _dm.Write(sql, new DataParameters(":migno", migration));

            sql = @"INSERT INTO sys_database_migration_lang
(sys_database_migration_id, language_iso_639_3_code, title, description, created_date, created_by, owned_date, owned_by)
VALUES ((SELECT MAX(sys_database_migration_id) FROM sys_database_migration WHERE migration_number = :migno)
,'ENG', 'Schema update of table ' + :table, 'Automatic update by GG Database Installer using script ' + :file
, GETUTCDATE(), 1, GETUTCDATE(), 1)";
            _dm.Write(sql, new DataParameters(":migno", migration, ":table", tableName, ":file", ScriptName));

        }


        // initialize cooperator local ID translation
        public void initUpdate() {
            List<TableSql> tableSql = new List<TableSql> { };
            tableSql.Add(generateTableUpdateSql("geography", null, false));
            tableSql.Add(generateTableUpdateSql("cooperator", new List<string> { "site_id", "sys_lang_id", "secondary_geography_id", "web_cooperator_id" }, false));

            tableSql[0].calc = "";  // can't calculate the geography local cooperators before cooperator initialized

            initializeTemp(tableSql);
        }


        public void updateStfl() {
            reportProgress(getDisplayMember("DatabaseUpgrader{readytoupgrade}", "Ready to upgrade {0}...", "table and field information (st, stf, stfl)"));
            List<TableSql> tableSql = generateSql4Tables(false, new List<string> { "sys_table", "sys_table_field", "sys_lang", "sys_table_field_lang"});

            // add custom name calculation
            tableSql[0].calc += String.Format("\r\nUPDATE {0}sys_table SET temp__name = table_name;", _fromPrefix);
            tableSql[1].calc += String.Format("\r\n"+@"UPDATE xsc SET temp__name = x0sc.temp__name +' '+ field_name
    FROM {0}sys_table_field xsc JOIN {0}sys_table x0sc ON x0sc.sys_table_id = xsc.sys_table_id;", _fromPrefix);
            tableSql[2].calc += String.Format("\r\nUPDATE {0}sys_lang SET temp__name = ietf_tag;", _fromPrefix);
            tableSql[3].calc += String.Format("\r\n" + @"UPDATE xsc SET temp__name = x1sc.temp__name +' '+ x2sc.temp__name
    FROM {0}sys_table_field_lang xsc JOIN {0}sys_table_field x1sc ON x1sc.sys_table_field_id = xsc.sys_table_field_id
    JOIN {0}sys_lang x2sc ON x2sc.sys_lang_id = xsc.sys_lang_id;", _fromPrefix);

            initializeTemp(tableSql);

            _failCnt = 0; _succCnt = 0; _attemptCnt = 0;

            //string loadType = loadWhichType("friendly name (stfl)", tableSql);
            string loadType = loadWhichType("table, field, and friendly name information (column names and definitions displayed in the CT)", tableSql);
            if (loadType == "cancel") return;

            updateSysTables(loadType, tableSql);

        }


        public void updateDataview() {
            reportProgress(getDisplayMember("DatabaseUpgrader{readytoupgrade}", "Ready to upgrade {0}...", "dataviews"));
            List<TableSql> dataviewSql = generateSql4Tables(false, new List<string> { "sys_dataview" });

            // add custom name calculation
            dataviewSql[0].calc += String.Format("\r\nUPDATE {0}sys_dataview SET temp__name = dataview_name;", _fromPrefix);

            // include a tracking field
            dataviewSql[0].alter += String.Format("ALTER TABLE {0}sys_dataview ADD sd_touched_by_updater VARCHAR(1) NULL;\r\n", _fromPrefix);

            // update tracking field as necessary
            dataviewSql[0].update   += String.Format("\r\nUPDATE xsc SET sd_touched_by_updater = 'Y' FROM {0}sys_dataview xsc WHERE ##WHERE##;\r\n", _fromPrefix);
            dataviewSql[0].insertID += String.Format("\r\nUPDATE xsc SET sd_touched_by_updater = 'Y' FROM {0}sys_dataview xsc WHERE ##WHERE##;\r\n", _fromPrefix);
            dataviewSql[0].insert   += String.Format("\r\nUPDATE xsc SET sd_touched_by_updater = 'Y' FROM {0}sys_dataview xsc WHERE ##WHERE##;\r\n", _fromPrefix);

            // clear children of updated dataviews except possibly custom language info 
            dataviewSql[0].update += @"
-- remove just the lang rows for obsolete dv fields
DELETE FROM ##TPRE2##sys_dataview_field_lang WHERE sys_dataview_field_id IN (
	SELECT xsftg.sys_dataview_field_id FROM ##TPRE2##sys_dataview_field xsftg
	 WHERE sys_dataview_id IN (SELECT local_sys_dataview_id FROM ##TPRE1##sys_dataview xsc WHERE ##WHERE##)
	   AND field_name NOT IN (SELECT field_name FROM ##TPRE1##sys_dataview_field WHERE sys_dataview_id IN 
				(SELECT sys_dataview_id FROM ##TPRE1##sys_dataview xsc WHERE ##WHERE##)));
-- remove just the obsolete dv field rows
DELETE FROM ##TPRE2##sys_dataview_field WHERE sys_dataview_field_id IN (
	SELECT xsftg.sys_dataview_field_id FROM ##TPRE2##sys_dataview_field xsftg
	 WHERE sys_dataview_id IN (SELECT local_sys_dataview_id FROM ##TPRE1##sys_dataview xsc WHERE ##WHERE##)
	   AND field_name NOT IN (SELECT field_name FROM ##TPRE1##sys_dataview_field WHERE sys_dataview_id IN 
				(SELECT sys_dataview_id FROM ##TPRE1##sys_dataview xsc WHERE ##WHERE##)));
DELETE FROM ##TPRE2##sys_dataview_param WHERE sys_dataview_id IN (SELECT local_sys_dataview_id FROM ##TPRE1##sys_dataview xsc WHERE ##WHERE##);
DELETE FROM ##TPRE2##sys_dataview_sql WHERE sys_dataview_id IN (SELECT local_sys_dataview_id FROM ##TPRE1##sys_dataview xsc WHERE ##WHERE##);
";
            dataviewSql[0].update = dataviewSql[0].update.Replace("##TPRE1##", _fromPrefix).Replace("##TPRE2##", _toPrefix);

            initializeTemp(dataviewSql);

            _failCnt = 0; _succCnt = 0; _attemptCnt = 0;

            string loadType = loadWhichType("dataviews", dataviewSql);
            if (loadType == "cancel") return;
            if (loadType == "new") {
                //not a good idea
                _dr = showMessageBox(_worker,
                        getDisplayMember("databaseUpgrader{sureNewDv}", "Loading just new dataviews is not recommended, Are you sure you don't want to load all modified dataviews?"),
                        getDisplayMember("databaseUpgrader{sureNewDvtitle}", "Are you sure"),
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (_dr == DialogResult.No) {
                    loadType = "modified";
                }
            }

            updateSysTables(loadType, dataviewSql);


            // Now do the dataview child tables
            List<TableSql> tableSql = generateSql4Tables(false, new List<string> { "sys_dataview_lang", "sys_dataview_field", "sys_dataview_field_lang", "sys_dataview_param", "sys_dataview_sql" });

            // add custom name calculation
            tableSql[0].calc += String.Format("\r\n" + @"UPDATE xsc SET temp__name = x1sc.temp__name +' '+ x2sc.temp__name
    FROM {0}sys_dataview_lang xsc JOIN {0}sys_dataview x1sc ON x1sc.sys_dataview_id = xsc.sys_dataview_id
    JOIN {0}sys_lang x2sc ON x2sc.sys_lang_id = xsc.sys_lang_id;", _fromPrefix);
            tableSql[1].calc += String.Format("\r\n" + @"UPDATE xsc SET temp__name = x0sc.temp__name +' '+ field_name
    FROM {0}sys_dataview_field xsc JOIN {0}sys_dataview x0sc ON x0sc.sys_dataview_id = xsc.sys_dataview_id;", _fromPrefix);
            tableSql[2].calc += String.Format("\r\n" + @"UPDATE xsc SET temp__name = x1sc.temp__name +' '+ x2sc.temp__name
    FROM {0}sys_dataview_field_lang xsc JOIN {0}sys_dataview_field x1sc ON x1sc.sys_dataview_field_id = xsc.sys_dataview_field_id
    JOIN {0}sys_lang x2sc ON x2sc.sys_lang_id = xsc.sys_lang_id;", _fromPrefix);
            tableSql[3].calc += String.Format("\r\n" + @"UPDATE xsc SET temp__name = x0sc.temp__name +' '+ param_name
    FROM {0}sys_dataview_param xsc JOIN {0}sys_dataview x0sc ON x0sc.sys_dataview_id = xsc.sys_dataview_id;", _fromPrefix);
            tableSql[4].calc += String.Format("\r\n" + @"UPDATE xsc SET temp__name = x0sc.temp__name +' '+ database_engine_tag
    FROM {0}sys_dataview_sql xsc JOIN {0}sys_dataview x0sc ON x0sc.sys_dataview_id = xsc.sys_dataview_id;", _fromPrefix);

            // include a tracking field for touched dataviews
            tableSql[0].alter += String.Format("ALTER TABLE {0}{1} ADD sd_touched_by_updater VARCHAR(1) NULL;\r\n", _fromPrefix, tableSql[0].tableName);
            tableSql[1].alter += String.Format("ALTER TABLE {0}{1} ADD sd_touched_by_updater VARCHAR(1) NULL;\r\n", _fromPrefix, tableSql[1].tableName);
            tableSql[2].alter += String.Format("ALTER TABLE {0}{1} ADD sd_touched_by_updater VARCHAR(1) NULL;\r\n", _fromPrefix, tableSql[2].tableName);
            tableSql[3].alter += String.Format("ALTER TABLE {0}{1} ADD sd_touched_by_updater VARCHAR(1) NULL;\r\n", _fromPrefix, tableSql[3].tableName);
            tableSql[4].alter += String.Format("ALTER TABLE {0}{1} ADD sd_touched_by_updater VARCHAR(1) NULL;\r\n", _fromPrefix, tableSql[4].tableName);

            // add SQL to calculate touched field
            tableSql[0].calc += String.Format(@"
UPDATE xsc SET sd_touched_by_updater = xsdsc.sd_touched_by_updater 
  FROM {1}{0} xsc LEFT JOIN {1}sys_dataview xsdsc ON xsdsc.sys_dataview_id = xsc.sys_dataview_id;
", tableSql[0].tableName, _fromPrefix);
            tableSql[1].calc += String.Format(@"
UPDATE xsc SET sd_touched_by_updater = xsdsc.sd_touched_by_updater 
  FROM {1}{0} xsc LEFT JOIN {1}sys_dataview xsdsc ON xsdsc.sys_dataview_id = xsc.sys_dataview_id;
", tableSql[1].tableName, _fromPrefix);
            tableSql[2].calc += String.Format(@"
UPDATE xsc SET sd_touched_by_updater = xsdsc.sd_touched_by_updater 
  FROM {1}{0} xsc LEFT JOIN {1}sys_dataview_field xsdsc ON xsdsc.sys_dataview_field_id = xsc.sys_dataview_field_id;
", tableSql[2].tableName, _fromPrefix);
            tableSql[3].calc += String.Format(@"
UPDATE xsc SET sd_touched_by_updater = xsdsc.sd_touched_by_updater 
  FROM {1}{0} xsc LEFT JOIN {1}sys_dataview xsdsc ON xsdsc.sys_dataview_id = xsc.sys_dataview_id;
", tableSql[3].tableName, _fromPrefix);
            tableSql[4].calc += String.Format(@"
UPDATE xsc SET sd_touched_by_updater = xsdsc.sd_touched_by_updater 
  FROM {1}{0} xsc LEFT JOIN {1}sys_dataview xsdsc ON xsdsc.sys_dataview_id = xsc.sys_dataview_id;
", tableSql[4].tableName, _fromPrefix);

            // Change SQL to limit changes to touched dataviews
            tableSql[0].update   = tableSql[0].update.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[0].insertID = tableSql[0].insertID.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[0].insert   = tableSql[0].insert.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[1].update   = tableSql[1].update.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[1].insertID = tableSql[1].insertID.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[1].insert   = tableSql[1].insert.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[2].update   = tableSql[2].update.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[2].insertID = tableSql[2].insertID.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[2].insert   = tableSql[2].insert.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[3].update   = tableSql[3].update.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[3].insertID = tableSql[3].insertID.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[3].insert   = tableSql[3].insert.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[4].update   = tableSql[4].update.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[4].insertID = tableSql[4].insertID.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");
            tableSql[4].insert   = tableSql[4].insert.Replace("##WHERE##", "sd_touched_by_updater = 'Y' AND ##WHERE##");

            // do the work of updating the dataview child tables
            initializeTemp(tableSql);
            _failCnt = 0; _succCnt = 0; _attemptCnt = 1234567;
            updateSysTables("all", tableSql);
        }


        public void updateTrigger() {
            reportProgress(getDisplayMember("DatabaseUpgrader{readytoupgrade}", "Ready to upgrade {0}...", "datatriggers"));
            List<TableSql> tableSql = generateSql4Tables(false, new List<string> { "sys_datatrigger", "sys_datatrigger_lang" });

            // add custom name calculation
            tableSql[0].calc += String.Format("\r\nUPDATE {0}sys_datatrigger SET temp__name = fully_qualified_class_name;", _fromPrefix);
            tableSql[1].calc += String.Format("\r\n" + @"UPDATE xsc SET temp__name = x1sc.temp__name +' '+ x2sc.temp__name
    FROM {0}sys_datatrigger_lang xsc JOIN {0}sys_datatrigger x1sc ON x1sc.sys_datatrigger_id = xsc.sys_datatrigger_id
    JOIN {0}sys_lang x2sc ON x2sc.sys_lang_id = xsc.sys_lang_id;", _fromPrefix);

            initializeTemp(tableSql);

            _failCnt = 0; _succCnt = 0; _attemptCnt = 0;

            string loadType = loadWhichType("datatriggers", tableSql);
            if (loadType == "cancel") return;

            updateSysTables(loadType, tableSql);
        }


        public void updateCode() {
            reportProgress(getDisplayMember("DatabaseUpgrader{readytoupgrade}", "Ready to upgrade {0}...", "trigger"));
            List<TableSql> tableSql = generateSql4Tables(false, new List<string> { "code_value", "code_value_lang" });

            // add custom name calculation
            tableSql[0].calc += "\r\nUPDATE ##TPRE1##code_value SET temp__name = group_name +' '+value;";
            tableSql[0].calc = tableSql[0].calc.Replace("##TPRE1##", _fromPrefix);
            tableSql[1].calc += String.Format("\r\n" + @"UPDATE xsc SET temp__name = x1sc.temp__name +' '+ x2sc.temp__name
    FROM {0}code_value_lang xsc JOIN {0}code_value x1sc ON x1sc.code_value_id = xsc.code_value_id
    JOIN {0}sys_lang x2sc ON x2sc.sys_lang_id = xsc.sys_lang_id;", _fromPrefix);

            initializeTemp(tableSql);

            _failCnt = 0; _succCnt = 0; _attemptCnt = 0;

            string loadType = loadWhichType("codes (titles and descriptions used by the CT code picker)", tableSql);
            if (loadType == "cancel") return;

            updateSysTables(loadType, tableSql);
        }


        public void updateGeography() {
            reportProgress(getDisplayMember("DatabaseUpgrader{readytoupgrade}", "Ready to upgrade {0}...", "geography"));
            List<TableSql> tableSql = generateSql4Tables(true, new List<string> { "geography", "region", "geography_region_map", "site"});
            tableSql.Add(generateTableUpdateSql("cooperator", new List<string> { "secondary_geography_id", "web_cooperator_id" }, true));

            tableSql[0].alter = ""; //geography table already altered with local id fields
            // cooperator just needs a couple local ids skipped in initial thing
            tableSql[4].alter = String.Format("ALTER TABLE {0}cooperator ADD local_site_id INT NULL; ALTER TABLE {0}cooperator ADD local_sys_lang_id INT NULL;", _fromPrefix); 

            // add custom name calculation
            tableSql[0].calc += "\r\nUPDATE ##TPRE1##geography SET temp__name = country_code + COALESCE(' ' + adm1, '') + COALESCE(' ' + adm2, '') + COALESCE(' ' + adm3, '') + COALESCE(' ' + adm4, '');";
            tableSql[0].calc = tableSql[0].calc.Replace("##TPRE1##", _fromPrefix);
            tableSql[1].calc += "\r\nUPDATE ##TPRE1##region SET temp__name = continent + COALESCE(' ' + subcontinent, '');";
            tableSql[1].calc = tableSql[1].calc.Replace("##TPRE1##", _fromPrefix);
            tableSql[3].calc += "\r\nUPDATE ##TPRE1##site SET temp__name = site_short_name + ' ' + site_long_name + COALESCE(' '+organization_abbrev,'') +COALESCE(' '+fao_institute_number,'');";
            tableSql[3].calc = tableSql[3].calc.Replace("##TPRE1##", _fromPrefix);
            tableSql[4].calc += "\r\nUPDATE ##TPRE1##cooperator SET temp__name = COALESCE(first_name,'') +COALESCE(' '+last_name,'') +COALESCE(' '+organization,'');";
            tableSql[4].calc = tableSql[4].calc.Replace("##TPRE1##", _fromPrefix);

            initializeTemp(tableSql);

            _failCnt = 0; _succCnt = 0; _attemptCnt = 0;

            string loadType = loadWhichType("geography", tableSql);
            if (loadType == "cancel") return;

            updateSysTables(loadType, tableSql, true);
        }


        public void updateTaxonomy() {
            reportProgress(getDisplayMember("DatabaseUpgrader{readytoupgrade}", "Ready to upgrade {0}...", "taxonomy"));
            List<TableSql> tableSql = new List<TableSql> { };
            tableSql.Add(generateTableUpdateSql("taxonomy_family", new List<string> { "type_taxonomy_genus_id" }, true));
            tableSql.Add(generateTableUpdateSql("taxonomy_genus", null, true));
            tableSql.Add(generateTableUpdateSql("taxonomy_species", null, true));

            // add custom name calculation
            tableSql[0].calc += String.Format("\r\nUPDATE {0}taxonomy_family SET temp__name = family_name + COALESCE (' subfamily '+subfamily_name, ' tribe '+tribe_name, ' subtribe '+subtribe_name, '');", _fromPrefix);
            tableSql[1].calc += String.Format("\r\nUPDATE {0}taxonomy_genus SET temp__name = genus_name + COALESCE (' subseries '+subseries_name, ' series '+series_name, ' subsection '+subsection_name, ' section '+section_name, ' subgenus '+subgenus_name, '');", _fromPrefix);
            tableSql[2].calc += String.Format("\r\nUPDATE {0}taxonomy_species SET temp__name = name;", _fromPrefix);

            // extra species name match on all the name parts in case name formating changed
            tableSql[2].recalc = tableSql[2].recalc.Replace("--##PKBYNAME##", @"
UPDATE xsc SET local_taxonomy_species_id = xtg.taxonomy_species_id
    , blocked = CASE WHEN xsc.taxonomy_species_id = xtg.taxonomy_species_id THEN 'N' ELSE xsc.blocked END
  FROM ##TPRE1##taxonomy_species xsc
 INNER JOIN ##TPRE2##taxonomy_species xtg ON xtg.species_name = xsc.species_name
		AND COALESCE(xtg.subspecies_name, '')	= COALESCE(xsc.subspecies_name, '')
		AND COALESCE(xtg.variety_name, '')	= COALESCE(xsc.variety_name, '')
		AND COALESCE(xtg.subvariety_name, '')	= COALESCE(xsc.subvariety_name, '')
		AND COALESCE(xtg.forma_name, '')	= COALESCE(xsc.forma_name, '')
		AND COALESCE(xtg.forma_rank_type, '')	= COALESCE(xsc.forma_rank_type, '')
        AND CASE WHEN xsc.taxonomy_species_id != xsc.current_taxonomy_species_id THEN COALESCE(xtg.name_authority,'') ELSE '' END
          = CASE WHEN xsc.taxonomy_species_id != xsc.current_taxonomy_species_id THEN COALESCE(xsc.name_authority,'') ELSE '' END
 INNER JOIN ##TPRE1##taxonomy_genus rtg ON rtg.taxonomy_genus_id = xsc.taxonomy_genus_id
 INNER JOIN ##TPRE2##taxonomy_genus ltg ON ltg.taxonomy_genus_id = xtg.taxonomy_genus_id
		AND ltg.genus_name = rtg.genus_name
 LEFT JOIN ##TPRE1##taxonomy_species x1sc ON x1sc.taxonomy_species_id = xtg.taxonomy_species_id
		AND format(x1sc.created_date, 'yyyy-MM-dd HH:mm:ss') = format(xtg.created_date, 'yyyy-MM-dd HH:mm:ss')
 WHERE xsc.local_taxonomy_species_id IS NULL
    AND x1sc.taxonomy_species_id IS NULL -- check against rows being renamed away
").Replace("##TPRE1##", _fromPrefix).Replace("##TPRE2##", _toPrefix);

            initializeTemp(tableSql);

            _failCnt = 0; _succCnt = 0; _attemptCnt = 0;

            string loadType = loadWhichType("taxomony", tableSql);
            if (loadType == "cancel") return;

            updateSysTables(loadType, tableSql, true);

            // Fix the family type genus pointer after the genera are loaded
            string fixFamilyTypeGenusSql = @"UPDATE xtg SET xtg.type_taxonomy_genus_id = tg1.local_taxonomy_genus_id FROM ##TPRE2##taxonomy_family xtg
  JOIN ##TPRE1##taxonomy_family xsc ON xtg.taxonomy_family_id = xsc.local_taxonomy_family_id
  JOIN ##TPRE1##taxonomy_genus tg1 ON tg1.taxonomy_genus_id = xsc.type_taxonomy_genus_id
 WHERE xtg.type_taxonomy_genus_id IS NULL AND tg1.local_taxonomy_genus_id IS NOT NULL";
            fixFamilyTypeGenusSql = fixFamilyTypeGenusSql.Replace("##TPRE1##", _fromPrefix).Replace("##TPRE2##", _toPrefix);
            _dm.Write(fixFamilyTypeGenusSql);
        }


        public void updateTaxDetail() {
            reportProgress(getDisplayMember("DatabaseUpgrader{readytoupgrade}", "Ready to upgrade {0}...", "taxonomy detail"));
            List<TableSql> tableSql = generateSql4Tables(false, new List<string> { "taxonomy_alt_family_map", "taxonomy_author", "taxonomy_noxious" });
            tableSql.Add(generateTableUpdateSql("literature", null, true));
            tableSql.Add(generateTableUpdateSql("citation", new List<string> { "accession_id", "method_id", "accession_ipr_id", "accession_pedigree_id", "genetic_marker_id" }, true));
            tableSql.Add(generateTableUpdateSql("taxonomy_common_name", null, false));
            tableSql.Add(generateTableUpdateSql("taxonomy_geography_map", null, false));
            tableSql.Add(generateTableUpdateSql("taxonomy_use", null, false));

            // add custom name calculation
            tableSql[1].calc += String.Format("\r\nUPDATE {0}taxonomy_author SET temp__name = short_name;", _fromPrefix);
            tableSql[5].calc += String.Format(@"
UPDATE xsc SET temp__name = xssc.name + COALESCE(' '+language_description, '') +' '+ xsc.name + COALESCE(CAST(citation_id AS nvarchar(8)), '')
FROM {0}taxonomy_common_name xsc
JOIN {0}taxonomy_species xssc ON xssc.taxonomy_species_id = xsc.taxonomy_species_id;", _fromPrefix);

            //fix citation calc by removing refernece to tables not relevant
            tableSql[4].recalc = tableSql[4].recalc.Replace(" AND COALESCE(xtg.accession_id, 0) = COALESCE(xsc.local_accession_id, 0)", "")
                .Replace(" AND COALESCE(xtg.method_id, 0) = COALESCE(xsc.local_method_id, 0)", "")
                .Replace(" AND COALESCE(xtg.accession_ipr_id, 0) = COALESCE(xsc.local_accession_ipr_id, 0)", "")
                .Replace(" AND COALESCE(xtg.accession_pedigree_id, 0) = COALESCE(xsc.local_accession_pedigree_id, 0)", "")
                .Replace(" AND COALESCE(xtg.genetic_marker_id, 0) = COALESCE(xsc.local_genetic_marker_id, 0)", "");

            // extra common name name match to deal with old style accented characters
            tableSql[5].recalc = tableSql[5].recalc.Replace("--##PKBYNAME##", @"
UPDATE xsc SET local_taxonomy_common_name_id = xtg.taxonomy_common_name_id
    , blocked = CASE WHEN xsc.taxonomy_common_name_id = xtg.taxonomy_common_name_id THEN 'N' ELSE blocked END
FROM ##TPRE1##taxonomy_common_name xsc INNER JOIN ##TPRE2##taxonomy_common_name xtg ON COALESCE(xtg.taxonomy_genus_id, 0) = COALESCE(xsc.local_taxonomy_genus_id, 0) AND COALESCE(xtg.taxonomy_species_id, 0) = COALESCE(xsc.local_taxonomy_species_id, 0) 
AND COALESCE(xtg.language_description, '') = COALESCE(xsc.language_description, '') AND COALESCE(xtg.citation_id, 0) = COALESCE(xsc.local_citation_id, 0)
AND xtg.name LIKE '%&%;%' AND xtg.simplified_name COLLATE Latin1_general_CI_AI = xsc.simplified_name COLLATE Latin1_general_CI_AI
WHERE local_taxonomy_common_name_id IS NULL
").Replace("##TPRE1##", _fromPrefix).Replace("##TPRE2##", _toPrefix);

            /*
            reportProgress("\r\n\r\nTable: " + tableSql[5].tableName
    + "\r\nalter SQL: " + tableSql[5].alter
    + "\r\n\r\ncalc SQL: \r\n" + tableSql[5].recalc
    + "\r\n\r\ncalc2 SQL: \r\n" + tableSql[5].calc
    + "\r\n\r\nupdate SQL: \r\n" + tableSql[5].update
    + "\r\n\r\ninsert ID SQL: \r\n" + tableSql[5].insertID
    + "\r\n\r\ninsert SQL: \r\n" + tableSql[5].insert
    + "\r\n\r\nfixcurrent SQL: \r\n" + tableSql[5].fixcurrent); //KFEDEBUG
            */

            initializeTemp(tableSql);

            _failCnt = 0; _succCnt = 0; _attemptCnt = 0;

            string loadType = loadWhichType("taxonomy detail", tableSql);
            if (loadType == "cancel") return;

            //fix common name accent in language description
            if (loadType != "new") _dm.Write("UPDATE taxonomy_common_name SET language_description = 'Japanese Rōmaji' WHERE language_description = 'Japanese R&omacr;maji'");

            updateSysTables(loadType, tableSql, true);
        }


        private void askReid(TableSql x) {
            string templateMoveCntSql = "SELECT count(*) FROM {0}{1} WHERE {1}_id != local_{1}_id OR(blocked='Y' and local_{1}_id IS NULL)";
            int cnt = 0;
            cnt = Toolkit.ToInt32(_dm.ReadValue(String.Format(templateMoveCntSql, _fromPrefix, x.tableName)), 0);
            if (cnt > 0) {
                reportProgress(String.Format("{0} reid candidates in table {1}.", cnt.ToString(), x.tableName));
                _dr = DialogResult.Yes;
                if (_relocate == false) _dr = DialogResult.No;
                if (_ask && !_relocate.HasValue) _dr = showMessageBox(_worker, @"Is it alright to renumber existing local Primary Keys in order to make way for the new rows?

Answering no means rows added for the official list will need to be renumbered, which will make updating them later difficult."
                    , "Relocate", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (_dr == DialogResult.Yes) {
                    _relocate = true;
                    reidBlockers(x.tableName, x.recalc);
                } else _relocate = false;
            }
        }


        private List<TableSql> generateSql4Tables(bool matchOnDate, List<string> tableNames) {
            List<TableSql> result = new List<TableSql> { };
            foreach (string tableName in tableNames) {
                result.Add(generateTableUpdateSql(tableName, null, matchOnDate));
            }
            return result;
        }


        private void initializeTemp(List<TableSql> listSql) {
            string sql = "";
            foreach (TableSql x in listSql) {
                try {
                    sql = x.alter; if (!String.IsNullOrEmpty(sql)) _dm.Write(sql);
                    sql = x.calc + "\r\n\r\n" + x.recalc; if (!String.IsNullOrEmpty(sql)) _dm.Write(sql);
                } catch (Exception e) {
                    reportProgress(getDisplayMember("databaseUpgrade{det1486}"
                    , "Failed to add local fields or calculate local ids for temp {0} table", x.tableName));
                    reportProgress("Exception: " + e.Message);
                    reportProgress("SQL: \r\n" + sql);
                    return;
                }
            }
        }


        private string loadWhichType(string groupName, List<TableSql> listSql) {
            int totalCnt = 0;
            int newCnt = 0;
            int newerCnt = 0;

            foreach (TableSql x in listSql) {
                int[] cnt = countRowsByType(_dm, x.tableName);
                totalCnt += cnt[0];
                newCnt += cnt[1];
                newerCnt += cnt[2];
            }
            if (totalCnt < 1) return "cancel";

            var fu = new frmUpgrade();
            string loadType = "modified";
            if (_ask) loadType = fu.ShowDialog("Which update set would you like from the new release for " + groupName + "?", newCnt, newCnt + newerCnt, totalCnt);

            if (loadType == "all") {
                _attemptCnt = totalCnt;
            } else if (loadType == "new") {
                _attemptCnt = newCnt;
            } else if (loadType == "modified") {
                _attemptCnt = newCnt + newerCnt;
            } else {
                reportProgress(getDisplayMember("databaseUpgrader{skipupgrade}", "Skipping {0} upgrade...", groupName));
                return "cancel";
            }

            reportProgress(getDisplayMember("databaseUpgrader{upgradingcount}", "Upgrading {0} {1} records...", _attemptCnt.ToString(), groupName));

            return loadType;
        }


        private void updateSysTables(string loadType, List<TableSql> listSql, bool askRelocate = false) {
            int rpInt = 0;
            int oneAtATimeLinit = Toolkit.GetSetting("UpdateSysTableOneAtATimeLimit", 100);
            if (_attemptCnt <= oneAtATimeLinit) rpInt = 1;

            foreach (TableSql tSql in listSql) {
                if (askRelocate) askReid(tSql);
                // recalculate local ids in case owners loaded since first time
                updateTable(tSql.recalc + tSql.calc, "", tSql.tableName, "", "", 0);
                // Update existing local rows
                string where = "COALESCE(xtg.modified_date, xtg.created_date) < COALESCE(xsc.modified_date, xsc.created_date)";
                if (loadType == "all") where = "local_" + tSql.tableName + "_id IS NOT NULL";
                if (loadType != "new") updateTable(tSql.recalc, tSql.update, tSql.tableName, "updating", where, rpInt);
                // Insert new rows
                updateTable(tSql.recalc, tSql.insertID, tSql.tableName, "inserting new", "local_" + tSql.tableName + "_id IS NULL AND blocked IS NULL", rpInt);
                updateTable(tSql.recalc, tSql.insert, tSql.tableName, "inserting relocated new", "local_" + tSql.tableName + "_id IS NULL AND blocked = 'Y'", rpInt);
                // recalculate local ids now rows are loaded
                updateTable(tSql.recalc + tSql.calc, "", tSql.tableName, "", "", 0);
                // if necessary redo the current ids now all rows loaded
                if (!String.IsNullOrEmpty(tSql.fixcurrent)) _dm.Write(tSql.fixcurrent);
            }

            // final results
            if (_failCnt < 1) {
                reportProgress(getDisplayMember("upgradeDatabaseInstall{upgradeg1280}"
                    , "Successfully inserted or modified {0} rows!", _succCnt.ToString()));
            } else {
                reportProgress(getDisplayMember("upgradeDatabaseInstall{upgradeg1383}"
                    , @"Failed to insert or modify {0} rows!
                    {1} were succesfully done.", _failCnt.ToString(), _succCnt.ToString()));
            }
        }


        private TableSql generateTableUpdateSql(string tableName, List<string> ignoreFields, bool matchOnDate) {
            string alterSql = "ALTER TABLE " + _fromPrefix + tableName + " ADD temp__name VARCHAR(200) NULL;\r\n";
            alterSql += "ALTER TABLE " + _fromPrefix + tableName + " ADD blocked VARCHAR(1) NULL;\r\n";
            string calcSql = "--##CLEAR##\r\n--##PK##\r\n--##PKBYNAME##\r\n";
            string calc2Sql = "";
            string updateSql = "UPDATE xtg SET ";
            string insertSql = "";
            insertSql += "INSERT INTO " + _toPrefix + tableName + " ( --##PKID##";

            string insert2Sql = @")
SELECT
--##PKID##
";

            bool useCurrent = false;

            DataTable dtc = _dm.Read(@"SELECT stf.field_name, stf.is_primary_key, stf.is_foreign_key, stfk.field_name AS fk_name, table_name
  FROM sys_table_field stf
  LEFT JOIN sys_table_field stfk ON stfk.sys_table_field_id = stf.foreign_key_table_field_id
  LEFT JOIN sys_table st ON st.sys_table_id = stfk.sys_table_id
 WHERE stf.sys_table_id = (SELECT sys_table_id FROM sys_table WHERE table_name = :name)
ORDER BY stf.field_ordinal ASC;" , new DataParameters(":name", tableName));

            foreach (DataRow row in dtc.Rows) {
                string cName = row["field_name"].ToString();
                bool isPK = false;
                if (ignoreFields != null && ignoreFields.Contains(cName)) continue;
                if (row["is_primary_key"].ToString() == "Y") {
                    isPK = true;
                }
                bool isFK = false;
                if (row["is_foreign_key"].ToString() == "Y") {
                    isFK = true;
                    if (cName.StartsWith("current_")) useCurrent = true;
                }

                //ALTER
                if (isPK || isFK) alterSql += "ALTER TABLE " + _fromPrefix + tableName + " ADD local_" + cName + " INT NULL;\r\n";

                if (!isPK) {
                    if (isFK) {
                        string fkName = row["fk_name"].ToString();
                        string fkTable = row["table_name"].ToString();
                        updateSql += ", " + cName + " = xsc.local_" + cName + "\r\n";
                        insertSql += "\r\n  " + cName + ",";
                        insert2Sql += "\r\n  local_" + cName + ",";
                        if (cName.StartsWith("current_")) {
                            calcSql = calcSql.Replace("--##CURRENT##", "UPDATE xsc SET local_" + cName + " = x2sc." + cName
                                + "\r\nFROM " + _fromPrefix + tableName + " xsc"
                                + "\r\nLEFT JOIN " + _fromPrefix + tableName + " x2sc ON x2sc." + tableName + "_id = xsc." + cName
                                );

                        } else if (cName.EndsWith("_by")) {
                            calc2Sql += String.Format(@"
UPDATE xsc SET local_{2}	= COALESCE(c.local_cooperator_id, 1)
  FROM {1}{0} xsc LEFT JOIN {1}cooperator c ON c.cooperator_id = xsc.{2};
", tableName, _fromPrefix, cName);

                        } else { 
                            calc2Sql += "\r\nUPDATE xsc SET local_" + cName + " = x2sc.local_" + fkName + @" FROM " + _fromPrefix + tableName + " xsc "
                                + " LEFT JOIN " + _fromPrefix + fkTable + " x2sc ON	x2sc." + fkName + " = xsc." + cName + ";\r\n";
                        }
                    } else {
                        updateSql += ", " + cName + " = xsc." + cName + "\r\n";
                        insertSql += "\r\n  " + cName + ",";
                        insert2Sql += "\r\n  " + cName + ",";
                    }
                }
            }


            // finish calc

            string cas = "UPDATE " + _fromPrefix + tableName + " SET local_" + tableName + "_id = NULL, blocked = NULL";
            if (useCurrent) cas += ", local_current_" + tableName + "_id = NULL";
            cas += ";\r\n";
            calcSql = calcSql.Replace("--##CLEAR##", cas);

            string aspk = "";
            if (matchOnDate) {
                aspk = string.Format(@"UPDATE xsc SET local_{0}_id = CASE
		WHEN xsc.created_date = xtg.created_date THEN xtg.{0}_id
		WHEN format(xsc.created_date, 'yyyy-MM-dd HH:mm:ss') = format(xtg.created_date, 'yyyy-MM-dd HH:mm:ss') THEN xtg.{0}_id
		ELSE NULL END
  , blocked = CASE
		WHEN xsc.created_date = xtg.created_date THEN NULL
		WHEN format(xsc.created_date, 'yyyy-MM-dd HH:mm:ss') = format(xtg.created_date, 'yyyy-MM-dd HH:mm:ss') THEN NULL
		ELSE 'Y' END
  FROM {1}{0} xsc
 INNER JOIN {2}{0} xtg ON xsc.{0}_id = xtg.{0}_id;"
                    , tableName, _fromPrefix, _toPrefix);
            } else {
                aspk = string.Format(@"UPDATE xsc SET blocked = 'Y'
  FROM {1}{0} xsc
 INNER JOIN {2}{0} xtg ON xsc.{0}_id = xtg.{0}_id;"
                    , tableName, _fromPrefix, _toPrefix);
            }

            aspk += generatePkSql(tableName, matchOnDate);
            calcSql = calcSql.Replace("--##PK##", aspk);

            if (useCurrent) {
                calcSql += String.Format(@"
UPDATE xsc SET local_current_{0}_id = x2sc.local_{0}_id FROM {1}{0} xsc
LEFT JOIN {1}{0} x2sc ON  x2sc.{0}_id = xsc.current_{0}_id;
", tableName, _fromPrefix, _toPrefix);
            }


            // UPDATE
            updateSql = updateSql.Replace(" SET , ", " SET \r\n  ");
            updateSql += String.Format("\r\nFROM {1}{0} xsc\r\n INNER JOIN {2}{0} xtg ON xsc.local_{0}_id = xtg.{0}_id\r\n WHERE ##WHERE##"
                , tableName, _fromPrefix, _toPrefix);

            // INSERT
            insertSql += insert2Sql + "###FROM " + _fromPrefix + tableName + " xsc\r\nWHERE ##WHERE##;\r\n";
            insertSql = insertSql.Replace(",)", ")\r\n");
            insertSql = insertSql.Replace(",###FROM", "\r\nFROM");

            string insertIDSql = "SET IDENTITY_INSERT " + _toPrefix + tableName + " ON;\r\n"
                + insertSql
                + "SET IDENTITY_INSERT " + _toPrefix + tableName + " OFF;\r\n";
            insertIDSql = insertIDSql.Replace("--##PKID##", "\r\n " + tableName + "_id,");

            // FIX CURRENT
            string fixSql = "";
            if (useCurrent) fixSql = String.Format(@"UPDATE xtg SET current_{0}_id = xsc.local_current_{0}_id
  FROM {1}{0} xsc JOIN {2}{0} xtg ON xtg.{0}_id = xsc.local_{0}_id
 WHERE xtg.current_{0}_id IS NULL AND xsc.local_current_{0}_id IS NOT NULL;
", tableName, _fromPrefix, _toPrefix);

            TableSql returnSql = new TableSql(tableName, alterSql, calcSql, calc2Sql, updateSql, insertIDSql, insertSql, fixSql);
            return returnSql;
        }


        class TableSql {
            public string tableName { get; set; }
            public string alter { get; set; }
            public string recalc{ get; set; }
            public string calc{ get; set; }
            public string update { get; set; }
            public string insertID { get; set; }
            public string insert { get; set; }
            public string fixcurrent { get; set; }

            public TableSql(string t, string a, string r, string c, string u, string ii, string i, string f) {
                this.tableName = t;
                this.alter = a;
                this.recalc = r;
                this.calc = c;
                this.update = u;
                this.insertID = ii;
                this.insert = i;
                this.fixcurrent = f;
            }
        }


        public string generatePkSql(string tableName, bool matchOnDate) {
            string resultSQL = String.Format(@"
UPDATE xsc SET local_{0}_id = xtg.{0}_id
    , blocked = CASE WHEN xsc.{0}_id = xtg.{0}_id THEN 'N' ELSE blocked END
FROM {1}{0} xsc INNER JOIN {2}{0} xtg ON ", tableName, _fromPrefix, _toPrefix);

            DataTable dtc = _dm.Read(@"SELECT CASE WHEN stf.is_nullable = 'N' THEN 'xtg.'+stf.field_name + ' = ' + 'xsc.'
		+ CASE WHEN stf.is_foreign_key = 'Y' THEN 'local_' ELSE '' END +stf.field_name
    WHEN stf.field_type = 'string' THEN 'COALESCE(xtg.'+stf.field_name + ', '''') = COALESCE(xsc.'+stf.field_name + ', '''')'
	ELSE 'COALESCE(xtg.'+stf.field_name + ', 0) = COALESCE(xsc.'
		+ CASE WHEN stf.is_foreign_key = 'Y' THEN 'local_' ELSE '' END +stf.field_name + ', 0)'
	END AS criteria
FROM sys_table st
JOIN sys_index si ON si.sys_table_id = st.sys_table_id
JOIN sys_index_field sif ON sif.sys_index_id = si.sys_index_id
JOIN sys_table_field stf ON stf.sys_table_field_id = sif.sys_table_field_id
WHERE st.table_name = :tname AND si.is_unique = 'Y'
ORDER BY st.table_name, sif.sort_order", new DataParameters(":tname", tableName));

            bool first = true;
            foreach (DataRow row in dtc.Rows) {
                if (first) first = false;
                else resultSQL += " AND ";
                resultSQL += row["criteria"].ToString();
            }

            resultSQL += String.Format("\r\nWHERE local_{0}_id IS NULL", tableName);
            if (matchOnDate) resultSQL += String.Format(@"
   AND NOT EXISTS (SELECT * FROM {1}{0} WHERE {0}_id = xtg.{0}_id
		AND format(created_date, 'yyyy-MM-dd HH:mm:ss') = format(xtg.created_date, 'yyyy-MM-dd HH:mm:ss'))", tableName, _fromPrefix);

            return resultSQL;
        }


        private int[] countRowsByType(DataManager dm, string tableName) {

            string matchSql = string.Format(@"SELECT temp__name AS short_name
    , xsc.{0}_id AS from_id, xtg.{0}_id AS to_id, blocked
    , COALESCE(xsc.modified_date, xsc.created_date) AS from_date
    , COALESCE(xtg.modified_date, xtg.created_date) AS to_date
FROM {1}{0} xsc
LEFT JOIN {2}{0} xtg ON xtg.{0}_id = xsc.local_{0}_id"
                , tableName, _fromPrefix, _toPrefix);

            try {
                DataTable dt = dm.Read(matchSql);
                int totalCnt = dt.Rows.Count;
                int newCnt = dt.Select("to_date IS NULL").Length;
                int newerCnt = dt.Select("to_date < from_date").Length;
                reportProgress(getDisplayMember("upgradeDatabaseInstall{upgradegeo1882}"
                    , "Found {0} {3} with {1} new and {2} updated"
                    , totalCnt.ToString(), newCnt.ToString(), newerCnt.ToString(), tableName));
                return new int[] { totalCnt, newCnt, newerCnt };
            } catch (Exception e) {
                reportProgress("Failed to count row types for table " + tableName);
                reportProgress(e.Message + " Using  this SQL: " + matchSql);
            }

            return new int[3] { 0, 0, 0 };
        }


        private void updateTable(string calcLocalSql, string updateSql, string tableName, string operation, string where, int rpInterval) {

            // Recalculate the local ids for updating local table
            if (!String.IsNullOrEmpty(calcLocalSql)) {
                try {
                    _dm.Write(calcLocalSql);
                } catch {
                    reportProgress(getDisplayMember("upgradeDatabaseInstall{upgradeallorone1926}"
                    , "Failed to calculate local ids for table {0}", tableName));
                }
            }

            if (String.IsNullOrEmpty(updateSql)) return;

            if (rpInterval < 1) {   // modify all at once
                //reportProgress("Attempting " + operation + " on table " + tableName + " with WHERE:" + where );  //KFE DEBUG

                try {
                    if (!String.IsNullOrEmpty(where)) updateSql = updateSql.Replace("##WHERE##", where);
                    int writeCnt = _dm.Write(updateSql, new DataParameters(":fromid", 1));
                    if(writeCnt>0) reportProgress("Succeeded " + operation + " " + writeCnt.ToString() + " rows in table " + tableName);
                    _succCnt += writeCnt;
                } catch (Exception e) {
                    reportProgress("Failed " + operation + " on table " + tableName + @"
                            Exception: " + e.Message + @"
                            Using  this SQL: " + updateSql);
                }

            } else { // one at a time

                string pk = tableName + "_id";
                string idSql = "SELECT xsc." + pk + ", temp__name FROM " + _fromPrefix + tableName + " xsc"
                    + " LEFT JOIN " + tableName + " xtg" + " ON xtg." + pk + " = xsc.local_" + pk
                    + " WHERE " + where;
                DataTable dtId = _dm.Read(idSql);
                DataRow[] Rows = dtId.Select(pk + " IS NOT NULL");
                foreach (DataRow row in Rows) {
                    string singleSql = updateSql.Replace("##WHERE##", "xsc." + pk + " = " + row[pk].ToString());
                    try {
                        _dm.Write(singleSql);
                        reportProgress("Succeeded " + operation + " " + tableName + " " + row["temp__name"].ToString());
                        _succCnt += 1;
                    } catch (Exception e) {
                        reportProgress("Failed " + operation + " of " + tableName + " " + row["temp__name"].ToString());
                        reportProgress(e.Message + @" Using  this SQL: " + singleSql);
                    }
                }
            }
        }


        private void reidBlockers(string tableName, string calcLocalSql) {

            reidRows(tableName);

            int stillNeedMoveCnt = 0;
            // Recalculate locals than do a second pass to move relocated to high
            try {
                _dm.Write(calcLocalSql);

                // status
                string moveCntSql = String.Format("SELECT count(*) FROM {0}{1} WHERE {1}_id != local_{1}_id OR(blocked='Y' and local_{1}_id IS NULL)", _fromPrefix, tableName);

                stillNeedMoveCnt = Toolkit.ToInt32(_dm.ReadValue(moveCntSql), 0);
                if(stillNeedMoveCnt > 0) reportProgress("Found " + stillNeedMoveCnt.ToString() + " rows still needing to be relocated.");
                //dr = showMessageBox(worker, " Take a moment after recalculating?", "KFE Debug", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            } catch {
                reportProgress(getDisplayMember("upgradeDatabaseInstall{upgradegeo2051}"
                , "Failed to calculate local ids for temp {0} table", tableName));
            }

            // second try if necessary
            if (stillNeedMoveCnt > 0) reidRows(tableName);
        }


        private void reidRows(string tableName) {
            string findMoveSql = @"SELECT t1.##TABLE##_id AS id1
    ,t1.local_##TABLE##_id AS id2
    ,t3.##TABLE##_id AS id3
    ,t2.local_##TABLE##_id AS id4
    ,t1.blocked
FROM ##TPRE1####TABLE## t1
LEFT JOIN ##TPRE1####TABLE## t2 ON t1.local_##TABLE##_id = t2.##TABLE##_id
LEFT JOIN ##TPRE1####TABLE## t3 ON t1.##TABLE##_id = t3.local_##TABLE##_id
WHERE t1.blocked = 'Y' OR (t1.##TABLE##_id != t1.local_##TABLE##_id)
";
            findMoveSql = findMoveSql.Replace("##TPRE1##", _fromPrefix).Replace("##TPRE2##", _toPrefix).Replace("##TABLE##", tableName);

            string findUniqIdxSql = @"SELECT i.name FROM sys.indexes i INNER JOIN sys.tables t ON i.object_id = t.object_id WHERE is_unique = 1 AND i.type_desc = 'NONCLUSTERED' AND t.name = :tname";

            string findColumnsSql = @"SELECT c.name FROM sys.tables t inner join sys.columns c ON t.object_id = c.object_id WHERE is_identity = 0 AND t.name = '" + tableName + "'";

            string switchSql = @"SELECT 'UPDATE ' + t.name + ' SET ' + kcu.column_name + ' = :toid WHERE ' + kcu.column_name + ' = :fromid;' AS Statement
FROM sys.tables t
JOIN sys.objects c ON c.parent_object_id = t.object_id AND c.type IN ('C' ,'UQ' ,'F')
JOIN sys.foreign_keys fk ON fk.name = c.name
JOIN sys.tables t2 ON t2.object_id = fk.referenced_object_id
JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu ON kcu.CONSTRAINT_NAME = c.name
WHERE t2.name = :tname";

            int maxid = Toolkit.ToInt32(_dm.ReadValue("SELECT MAX (" + tableName + "_id) FROM " + _fromPrefix + tableName), 0);
            int high = Toolkit.ToInt32(_dm.ReadValue("SELECT MAX (" + tableName + "_id) FROM " + tableName), 0);
            if (high > maxid) maxid = high;

            // create insert sql for table
            string insertSql = "SET IDENTITY_INSERT " + tableName + @" ON;
INSERT INTO " + tableName + " (" + tableName + "_id";
            string selectSql = "\r\nSELECT :toid";
            DataTable dtCols = _dm.Read(findColumnsSql, new DataParameters(":tname", tableName));
            foreach (DataRow row in dtCols.Rows) {
                insertSql += ", " + row["name"];
                selectSql += ", " + row["name"];
            }
            insertSql += ") " + selectSql + " FROM " + tableName + " WHERE " + tableName + "_id = :fromid;";

            // create SQL to update FK ids
            string fkUpdateSql = "";
            DataTable dtSql = _dm.Read(switchSql, new DataParameters(":tname", tableName));
            foreach (DataRow row in dtSql.Rows) {
                fkUpdateSql += row["statement"].ToString() + "\r\n";
            }

            // find and disable any unique index(es) on the table
            DataTable dtUniqIdx = _dm.Read(findUniqIdxSql, new DataParameters(":tname", tableName));
            foreach (DataRow row in dtUniqIdx.Rows) {
                string disableSql = @"ALTER INDEX " + row["name"].ToString() + " ON " + tableName + " DISABLE;";
                try {
                    _dm.Write(disableSql);
                } catch (Exception e) {
                    reportProgress("Failed to disable index " + row["name"].ToString() + " on table " + tableName + " with exception:" + e.Message
                        + " using SQL: " + disableSql);
                }
            }

            // move blocking rows up out of the way
            DataTable dtId = _dm.Read(findMoveSql);
            foreach (DataRow row in dtId.Select("blocked = 'Y' AND id3 IS NULL")) {
                maxid += 1;
                //reportProgress("Moving local " + tableName + " row out of the way from " + row["id1"].ToString() + " to " + maxid.ToString()); //KFEDEBUG
                reidSingleRow(tableName, insertSql, fkUpdateSql, row["id1"].ToString(), maxid.ToString());
            }

            // move misplaced to official number metablocking first
            dtId = _dm.Read(findMoveSql);
            foreach (DataRow row in dtId.Select("id2 <> id1 AND id4 IS NOT NULL")) {
                int toid = Toolkit.ToInt32(row["id1"], 0);
                string checkSql = "SELECT COUNT(*) FROM " + tableName + " WHERE " + tableName + "_id = :toid";
                int checkCnt = Toolkit.ToInt32(_dm.ReadValue(checkSql, new DataParameters(":toid", toid)), 0);
                if (checkCnt > 0) toid = ++maxid; // movement blocked, go high

                //reportProgress("Relocating " + tableName + " row from " + row["id2"].ToString() + " to " + toid.ToString()); //KFEDEBUG
                reidSingleRow(tableName, insertSql, fkUpdateSql, row["id2"].ToString(), toid.ToString());
            }

            // move misplaced to official number non-metablocking second
            dtId = _dm.Read(findMoveSql);
            foreach (DataRow row in dtId.Select("id2 <> id1 AND id4 IS NULL")) {
                int toid = Toolkit.ToInt32(row["id1"], 0);
                string checkSql = "SELECT COUNT(*) FROM " + tableName + " WHERE " + tableName + "_id = :toid";
                int checkCnt = Toolkit.ToInt32(_dm.ReadValue(checkSql, new DataParameters(":toid", toid)), 0);
                if (checkCnt > 0) toid = ++maxid; // movement blocked, go high

                //reportProgress("Temporarily relocating " + tableName + " row from " + row["id2"].ToString() + " to " + toid.ToString()); //KFEDEBUG
                reidSingleRow(tableName, insertSql, fkUpdateSql, row["id2"].ToString(), toid.ToString());
            }


            // Rebuild the unique unique index(es) on the table
            foreach (DataRow row in dtUniqIdx.Rows) {
                string rebuildSql = @"ALTER INDEX " + row["name"].ToString() + " ON " + tableName + " REBUILD;";
                try {
                    _dm.Write(rebuildSql);
                } catch (Exception e) {
                    reportProgress("Failed to rebuild index " + row["name"].ToString() + " on table " + tableName + " with exception:" + e.Message
                        + " using SQL: " + rebuildSql);
                }
            }
        }


        private void reidSingleRow(string tableName, string copySql, string fkUpdateSql, string fromId, string toId) {
            string Sql = "";

            try {
                // create copy of row in new position
                Sql = copySql;
                int writeCnt = _dm.Write(copySql, new DataParameters(":fromid", Toolkit.ToInt32(fromId, -1), ":toid", Toolkit.ToInt32(toId, -1)));

                // update foreign keys, if any
                if (!String.IsNullOrEmpty(fkUpdateSql)) {
                    Sql = fkUpdateSql;
                    int fkCnt = _dm.Write(fkUpdateSql, new DataParameters(":fromid", Toolkit.ToInt32(fromId, -1), ":toid", Toolkit.ToInt32(toId, -1)));
                }

                // Now it should be safe to delete the original row
                Sql = "DELETE FROM " + tableName + " WHERE " + tableName + "_id = :fromid";
                int dCnt = _dm.Write(Sql, new DataParameters(":fromid", Toolkit.ToInt32(fromId, -1)));
            } catch (Exception e) {
                reportProgress("Failed moving " + tableName + " from row id " + fromId + " to " + toId
                    + " with exception:" + e.Message + " using SQL: " + Sql + "\r\n\r\ncopySql: " + copySql + "\r\n\r\nfkUpdateSql: " + fkUpdateSql);
            }
        }


        public void reportProgress(string message) {
            Thread.Sleep(20);
            _sb.AppendLine(message);
            _worker.ReportProgress(0, message);
            Thread.Sleep(20);
        }

    }

}

