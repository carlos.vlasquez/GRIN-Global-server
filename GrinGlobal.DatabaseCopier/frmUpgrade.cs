﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrinGlobal.DatabaseCopier {
    public partial class frmUpgrade : Form {
        public frmUpgrade() {
            InitializeComponent();
        }

        public string LoadType;

        public string ShowDialog(string header, int countNew, int countModified, int countAll) {
            lblHeader.MaximumSize = new Size(460, 0);
            lblHeader.AutoSize = true;
            lblHeader.Text = header;

            if (countNew == 0 & countModified == 0 & countAll == 0) return "cancel";

            lblNew.Text = String.Format("{0} New", countNew);
            lblModified.Text = String.Format("{0} New or Modified (have a more recent modified date)", countModified);
            if (countModified > countNew) {
                rdoModified.Checked = true;
                btnLoad.Text = "Load Modified";
            }
            lblAll.Text = String.Format("{0} Total (overwrite all)", countAll);
            this.ShowDialog(); //Display and activate this form
            return LoadType;
        }

        private void frmUpgrade_Load(object sender, EventArgs e) {
            LoadType = "";
        }

        private void rdaAll_CheckedChanged(object sender, EventArgs e) {
            btnLoad.Text = "Load All";
        }

        private void rdoNew_CheckedChanged(object sender, EventArgs e) {
            btnLoad.Text = "Load New";
        }

        private void rdoModified_CheckedChanged(object sender, EventArgs e) {
            btnLoad.Text = "Load Modified";
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            LoadType = "cancel";
            this.Close();
        }

        private void btnLoad_Click(object sender, EventArgs e) {
            if (rdoNew.Checked) LoadType = "new";
            if (rdoAll.Checked) LoadType = "all";
            if (rdoModified.Checked) LoadType = "modified";
            this.Close();
        }

        private void lblHeader_Click(object sender, EventArgs e) {

        }
    }
}
