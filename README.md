# GRIN-Global server software

This software was created by USDA/ARS, with Bioversity International coordinating testing and feedback from the international genebank community. Development was supported financially by USDA/ARS and by a major grant from the Global Crop Diversity Trust.&nbsp; This statement by USDA does not imply approval of these enterprises to the exclusion of others which might also be suitable.

## Terms of Use Disclaimer

USDA dedicates this software to the public, anyone may use, copy, modify, publish, distribute, perform publicly and display publicly this software. Notice of this access as well as the other paragraphs in this notice shall be included in all copies or modifications of this software.

This software application has not been tested or otherwise examined for suitability for implementation on, or compatibility with, any other computer systems. USDA does not warrant, either explicitly or implicitly, that this software program will not cause damage to the user�s computer or computer operating system, nor does USDA warrant, either explicitly or implicitly, the effectiveness of the software application.

The English text above shall take precedence in the event of any inconsistencies between the English text and any translation of this notice.

### Third Party Libraries

See LICENSE-3RD-PARTY.txt for third party library license information